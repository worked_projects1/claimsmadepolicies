{
  "treatment": {
    "index-name": "post-operative-avocational-activities",
    "label-name": "Post-operative Avocational Activities",
    "treatment-type": "surgery-add-on"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/17/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "surgery-add-on",
        "label-name": "Surgery Add-on"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation.\n\nThere is no recommendation for or against specific work, avocational activities, or sports post-operatively.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale:  There are three primary methods to assess appropriate work, avocational and sports activities for hip arthroplasty and hip fracture patients: epidemiological studies, biomechanical models, and experimental studies [1204]. The available studies from these different methods produce substantial conflicts that are not readily resolved. Because the evidence conflicts and the epidemiological studies are the gold standard for the development of quality guidance [1205-1207], this review emphasizes epidemiological studies.\n\nExercise recommendations are developed largely without epidemiological data. The following activities have been recommended: bicycling, ballroom dancing, croquet, golf, horseshoes, shooting, shuffleboard, swimming, doubles tennis, and walking [1204, 1208]. Activities recommended with appropriate experience included low-impact aerobics, road bicycling, bowling, canoeing, hiking, horseback riding and cross-country skiing. Activities some have advised against include baseball, basketball, football, jogging, singles tennis, and volleyball. There was no conclusion regarding square dancing, fencing, ice skating, speed walking, downhill skiing, or weight lifting [1204, 1208]. However, these recommendations do not necessarily conform with epidemiological evidence.\n\nIt has been argued that high-impact loading activities should be prohibited in hip arthroplasty patients [1182]; however, increased risk of loosening has been reported among patients who were not skiing compared with skiers [1021]. The same researchers also reported a longer term trend of accelerated wear in the more physically active group [1021]. Another study found an approximately 9-fold greater risk for loosening among patients engaged in no sporting activity compared with those engaged in sports (e.g., hiking, climbing, skiing, swimming, running, cycling, and tennis) [1209]. Uncemented prostheses have been reported to have less radiographic loosening in active golfers [1022]. Another study found no apparent deteriorating effect of intensive recreational activities [1020]. Higher rates of aseptic loosening are reported among men in registry studies and case series [443, 1210]; however, whether that is related to force is unknown. Currently, the balance of the epidemiological literature does not support the argument that activity results in loosening.\n\nStudies on prosthetic wear rates have been used to imply appropriate work limitations for the post-arthroplasty patient; however, no quality studies have been reported that address the appropriateness of work limitations. Additionally, the avocational studies reviewed above do not provide quality evidence in support of activity limitations. Thus, although reduced return-to-work status has been reported among patients with more physically demanding work [1211], there is not a strong rationale for work restrictions in the post-surgical hip population.\n\nQuality evidence does not sufficiently support evidence-based guidance and therefore there is no recommendation for or against specific work, avocational or sporting activities."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteoarthrosis%2Ftreatment-recommendations%2Frehabilitation",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "surgical-treatment",
    "label-name": "Surgical Treatment",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/17/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hip-fracture",
        "label-name": "Hip Fracture"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nSurgical intervention for hip fractures is recommended as soon as the patient is medically stable.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications:    Hip fractures\n\nBenefits:    Faster recovery and lower mortality risks than with non-operative management\n\nHarms:    Perioperative infections, venous thromboembolisms, pneumonia\n\nRationale:    There are many different surgical approaches and products used for fixation. There also are numerous biomechanical studies on these various approaches [1520-1525]; however, while yielding sometimes useful information, they are unable to definitively test efficacy or superiority in humans. Pins are sometimes hydroxyapatite-coated [1526], although quality evidence of efficacy or superiority of these products in these patients is lacking.\n\nFixation failures have been thought to be particularly due to either inadequate reduction or suboptimal fixation [1500, 1527, 1528]. In the elderly, additional factors influencing adverse outcomes include comorbid medical conditions and ability to bear weight [1500, 1529-1531]. These reports suggest technical issues as well as post-operative management are necessary to achieve optimal outcomes.\n\nTwo authors have published multiple Cochrane reviews [1532-1534]. One of these reviews concluded the sliding hip screw was superior to nails for extracapsular hip fractures, but that there is insufficient evidence to ascertain meaningful differences between different intramedullary nails [1533]. A sliding hip screw was also thought to be superior to fixed nail plates for extracapsular hip fractures [1534]. The sliding hip screw is thought to have a lower complication rate than intramedullary nails for treatment of trochanteric fractures [1532]. Another literature review concluded there was a preference for surgical fixation among intertrochanteric hip fracture patients if the patient was medically stable. Stable fractures were felt to be better treated with plate and screw implants and intramedullary devices. Unstable fractures were thought to be better treated with load-sharing intramedullary implants; however, the literature was not felt to have demonstrated this belief [1529].\n\nThere are two studies using minimally invasive techniques, but no clear conclusions in favor of these approaches [1535, 1536]. Osteonecrosis and nonunion rates are high in post-hip fracture patients, and with inadequacy of reduction reportedly a significant factor [1500], successful reduction becomes an important consideration. External fixation devices have been studied in one quality study and suggested external fixation was superior for operative time, blood loss and pain for treatment of pertrochanteric fractures [1526]. This study needs replication.\n\nThere are many quality RCTs evaluating various products, particularly including dynamic hip screws, dynamic condylar screws, compression hip screws, intramedullary hip screws, gamma nails, gliding nails, proximal femoral nails, Pugh nails, percutaneous compression plates, nail plates, and Medoff sliding plates (see hip fracture evidence table). A majority of the studies failed to find one approach superior to another [1502, 1503, 1512, 1517, 1537-1542] and some provide conflicting results. Additionally, the variability of the types of fractures provides additional uncertainty regarding optimal intervention(s). Thus, there is no recommendation for or against the use of a specific product.\n\nIt is also noteworthy that quality studies document fracture healing conservatively with traction [1501, 1507-1509], yet death rates are also reportedly higher for that method of treatment [1508]. A Cochrane review concluded that quality trials comparing conservative and surgical treatment for hip fractures are needed [1543]. However, as one quality study found longer hospital stays and deaths particularly in the elderly [1501], the current quality evidence suggests that surgical results are superior to traction for treatment of these fractures; thus, surgery is recommended, particularly in the elderly.\n\nThe speed with which treatment is considered early or delayed varies with estimates of 6 to 12 hours [1544-1548]. There are no quality studies, but a retrospective review of cases and a large case series suggest better outcomes for earlier intervention [1548] or shorter hospitalizations and fewer complications [1549]. Generally, early intervention is recommended once the patient is medically stable. Skin sterilization issues have been studied and are important considerations [1082, 1550-1553].\n\nThe type of surgical treatment (e.g., pin, screw, nail) or non-operative management is deferred to the treating surgeon."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-fractures%2Ftreatment-recommendations%2Fsurgical-considerations",
    "odg-link": ""
  }
}
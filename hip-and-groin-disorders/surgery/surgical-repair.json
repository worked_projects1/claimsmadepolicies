{
  "treatment": {
    "index-name": "surgical-repair",
    "label-name": "Surgical Repair",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/17/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "gluteus-medius-tear",
        "label-name": "Gluteus Medius Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have tear(s) of the gluteus medius tendon with accompanying pain and/or functional deficits felt amenable to surgical treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient had at least 3 weeks of non-operative treatment to ascertain whether the pain and/or function will sufficiently recover without need for surgery?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nSurgical repair is recommended for gluteus medius tears that are non-responsive to medical management.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:   Tears of the gluteus medius tendon with accompanying pain and/or functional deficits felt amenable to surgical treatment. Generally at least 3 weeks of non-operative treatment is advisable to ascertain whether the pain and/or function will sufficiently recover without need for surgery.\n\nBenefits:    Improved function and reduced pain.\n\nHarms:   Lack of resolution, infection, nerve injury, CRPS\n\nRationale:    Gluteus medius tendon tears have been treated with surgical repair, although there are no quality studies to ascertain efficacy. Surgical repair has adverse effects and is costly, but when there are functional deficits and/or unresolvable pain related to gluteus medius tendon tears, it is a recommended treatment."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hip-impingement",
        "label-name": "Hip Impingement"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have hip pain with suspicion of labral tear, intraarticular body, femoroacetabular impingement, or other subacute or chronic mechanical symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed conservative management and either failed arthroscopic repair and/or thought to be best treated with an open approach?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nOpen surgical repair is recommended for hip impingement or labral tear cases that fail conservative management and either fail arthroscopic repair and/or are thought to be best treated with an open approach.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:     Patients with hip pain with suspicion of labral tear, intraarticular body, femoroacetabular impingement, or other subacute or chronic mechanical symptoms. Generally, should either have failed arthroscopic repair and/or thought to be best treated with an open approach.\n\nBenefits:   Definitively address the diagnosis.\n\nHarms:    Somewhat higher complication rates with open procedures compared with arthroscopic procedures, including infection, nerve injuries, venous thrombosis, CRPS. Labral resections may increase instability [1750, 1751].\n\nFrequency/Dose/Duration:   Generally, only one procedure is needed.\n\nRationale:    Surgical repairs have been attempted with reportedly successful results in case series [496, 1744, 1762, 1763]. Arthroscopic surgery [83, 1714, 1720, 1727, 1731, 1747, 1752, 1755, 1758, 1764-1769] or open repair [1712, 1713, 1727, 1770, 1771] are recommended for cases that fail conservative management [42, 1747, 1772].\n\nThere are many different surgical procedures that have been utilized to attempt to address the hip pathology that is thought to be producing symptoms [1712], including debridement [1713, 1747] and/or osteoplasty of the femoral head [1712], acetabular osteoplasty [1712], resection or repair of labral tears [1707, 1714, 1727, 1749, 1759], labral debridement [1710], limbectomy [1757], trochanteric flip osteotomy, peri-acetabular osteotomy [1773], or triple osteotomy [1707, 1710, 1712-1714, 1727, 1747, 1749, 1757, 1759, 1773]. Surgical procedures for hip dysplasia have included shelf osteoplasty, femoral varus osteotomy, and acetabular osteotomy [43, 83, 1773, 1774]. There are no quality studies to address efficacy of either open or arthroscopic repairs, or comparative studies between these approaches [42]. There is controversy regarding which approach is preferred [42, 43, 1712]. A case series reported better results from arthroscopy among patients with mechanical symptoms and without osteoarthrosis [1755]. Arthroscopy has been used to diagnose and potentially plan subsequent mini or open surgical repair [43, 45, 1775]."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "labral-tear",
        "label-name": "Labral Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have hip pain with suspicion of labral tear, intraarticular body, femoroacetabular impingement, or other subacute or chronic mechanical symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed conservative management and either failed arthroscopic repair and/or thought to be best treated with an open approach?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nOpen surgical repair is recommended for hip impingement or labral tear cases that fail conservative management and either fail arthroscopic repair and/or are thought to be best treated with an open approach.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:     Patients with hip pain with suspicion of labral tear, intraarticular body, femoroacetabular impingement, or other subacute or chronic mechanical symptoms. Generally, should either have failed arthroscopic repair and/or thought to be best treated with an open approach.\n\nBenefits:   Definitively address the diagnosis.\n\nHarms:    Somewhat higher complication rates with open procedures compared with arthroscopic procedures, including infection, nerve injuries, venous thrombosis, CRPS. Labral resections may increase instability [1750, 1751].\n\nFrequency/Dose/Duration:   Generally, only one procedure is needed.\n\nRationale:    Surgical repairs have been attempted with reportedly successful results in case series [496, 1744, 1762, 1763]. Arthroscopic surgery [83, 1714, 1720, 1727, 1731, 1747, 1752, 1755, 1758, 1764-1769] or open repair [1712, 1713, 1727, 1770, 1771] are recommended for cases that fail conservative management [42, 1747, 1772].\n\nThere are many different surgical procedures that have been utilized to attempt to address the hip pathology that is thought to be producing symptoms [1712], including debridement [1713, 1747] and/or osteoplasty of the femoral head [1712], acetabular osteoplasty [1712], resection or repair of labral tears [1707, 1714, 1727, 1749, 1759], labral debridement [1710], limbectomy [1757], trochanteric flip osteotomy, peri-acetabular osteotomy [1773], or triple osteotomy [1707, 1710, 1712-1714, 1727, 1747, 1749, 1757, 1759, 1773]. Surgical procedures for hip dysplasia have included shelf osteoplasty, femoral varus osteotomy, and acetabular osteotomy [43, 83, 1773, 1774]. There are no quality studies to address efficacy of either open or arthroscopic repairs, or comparative studies between these approaches [42]. There is controversy regarding which approach is preferred [42, 43, 1712]. A case series reported better results from arthroscopy among patients with mechanical symptoms and without osteoarthrosis [1755]. Arthroscopy has been used to diagnose and potentially plan subsequent mini or open surgical repair [43, 45, 1775]."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hamstring-tear-and/or-strain",
        "label-name": "Hamstring Tear and/or Strain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have large or complete tears of the hamstrings or hip flexor strain(s) with functional deficits felt amenable to surgical treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nSurgical repair is recommended for treatment of large or complete hamstring or hip flexor strains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications:   Large or complete tears of the hamstrings or hip flexor strains with functional deficits felt amenable to surgical treatment. Generally, large or complete hamstrings tears require surgical repair to facilitate recovery.\n\nBenefits:    Improved functional recovery and reduced pain.\n\nHarms:     Lack of resolution, infection, nerve injury, CRPS\n\nRationale:   Hamstring tears and hip flexor tears that are large or complete have generally been treated with surgical repair, although there are no quality studies to ascertain efficacy. Surgical repair has adverse effects and is costly, but as these muscles are important for physical function, surgical repair is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hip-flexor-tear-and/or-strain",
        "label-name": "Hip Flexor Tear and/or Strain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have large or complete tears of the hamstrings or hip flexor strain(s) with functional deficits felt amenable to surgical treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nSurgical repair is recommended for treatment of large or complete hamstring or hip flexor strains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications:   Large or complete tears of the hamstrings or hip flexor strains with functional deficits felt amenable to surgical treatment. Generally, large or complete hamstrings tears require surgical repair to facilitate recovery.\n\nBenefits:    Improved functional recovery and reduced pain.\n\nHarms:     Lack of resolution, infection, nerve injury, CRPS\n\nRationale:   Hamstring tears and hip flexor tears that are large or complete have generally been treated with surgical repair, although there are no quality studies to ascertain efficacy. Surgical repair has adverse effects and is costly, but as these muscles are important for physical function, surgical repair is recommended."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fgluteus-medius-tendinosis%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-impingement-labral-tears%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhamstring-hip-flexor-strains%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
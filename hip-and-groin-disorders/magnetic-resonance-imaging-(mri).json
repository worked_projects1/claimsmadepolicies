{
  "treatment": {
    "index-name": "magnetic-resonance-imaging-(mri)",
    "label-name": "Magnetic Resonance Imaging (MRI)",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/17/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "routine-evaluation-of-hip-joint-pathology",
        "label-name": "Routine Evaluation of Hip Joint Pathology"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended.\n\nMRI is not recommended for routine evaluation of acute, subacute, or chronic hip joint pathology, including degenerative joint disease. There are other indications for MRI, such as gluteus medius tendinosis/tears, osteonecrosis, femoroacetabular impingement, and evaluation of masses. (See sections on other Hip Disorders.)\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\n Level of Confidence – Moderate\n\nRationale:   MRI has not been evaluated in quality studies for hip OA with subsequent improvements in clinical outcomes [38]. However, MRI findings consistent with OA are likely to be particularly helpful for soft tissue abnormalities. There is low-quality evidence that MRI may be less sensitive for the detection of subchondral fractures than helical CT or plain radiographs in patients with osteonecrosis [481]. MRI has been suggested for the evaluation of patients with symptoms lasting more than 3 months [496-498]. Because there are concerns that MRI is inferior to MR arthrography, particularly for evaluating the labrum [499], MRI without arthrography is recommended for evaluating the joint but not the labrum. There are reports of patient with negative MRIs in whom gluteus medius tendon tears were found at surgery; thus, MRIs may have similar limitations for imaging tendons in the hip joint as in the shoulder [500]. MRI is not invasive, has no adverse effects (aside from issues of claustrophobia or complications of medication), but it is costly. MRI is not recommended for routine hip imaging, but it is recommended for select hip joint pathology, particularly involving concerns regarding soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-osteonecrosis",
        "label-name": "Diagnosing Osteonecrosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have subacute or chronic hip pain thought to be due to osteonecrosis? (This is particularly recommended when the diagnosis is unclear or if additional diagnostic evaluation and/or staging is needed).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nMRI is recommended for diagnosing osteonecrosis.\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nLevel of Confidence – High\n\nIndications:  Subacute or chronic hip pain thought to be due to osteonecrosis, particularly when the diagnosis is unclear or if additional diagnostic evaluation and/or staging is needed.               \n\nBenefits:  Identify severity, collapse and to stage.\n\nHarms:  Negligible\n\nFrequency/Dose/Duration:   Generally one evaluation. A second is often needed if there is a significant clinical change, or need to evaluate progress/resolution.       \n\nRationale:   Multiple studies show utility of MRI for assessing ON [704, 1260]. Helical computerized tomography is considered superior to MRI for imaging bone collapse [481]. MRI is considered superior for imaging bone marrow edema, which is inversely correlated with prognosis. Thus, both tests have their advantages. MRI is not invasive (or minimally so with a contrast exam), has negligible adverse effects, is high cost, has utility for the diagnosis and staging of osteonecrosis and is thus recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hip-joint-pathology",
        "label-name": "Hip Joint Pathology"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have subacute or chronic hip pain and needs imaging surrounding soft tissues, including evaluating periarticular structures or masses (generally not indicated for acute hip pain as radiographs usually suffice)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nMRI is recommended for select hip fracture patients who also have subacute or chronic hip pain with consideration of accompanying soft tissue pathology or other diagnostic concerns.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\n Level of Confidence – Low\n\nIndications:   Patients with subacute or chronic hip pain who need imaging surrounding soft tissues, including evaluating periarticular structures or masses (generally not indicated for acute hip pain as radiographs usually suffice).\n\nBenefits:    Diagnosing an alternative condition\n\nHarms:   Negligible. Modest radiation exposure\n\nFrequency/Dose/Duration:  Generally only one examination should be required. A second evaluation is rarely needed.\n\nRationale:   There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis [481]. MRI has been suggested for evaluations of patients with symptoms over 3 months [496, 1386-1388]. There are reports of negative MRIs, yet finding gluteus medius tendon tears at surgery, thus MRIs appear to potentially have similar limitations imaging tendons in hip joint as in the shoulder [500]. MRI is not invasive, has no adverse effects aside from issues of claustrophobia or complications of medication, but is costly. MRI is not recommended for routine hip imaging, but is recommended for select hip joint pathology particularly involving concerns regarding soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-groin-strain",
        "label-name": "Diagnosing Groin Strain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nX-rays or MRI are selectively recommended to diagnose groin strains, sports hernias, or adductor-related groin pain in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:    Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:    There are no quality studies of MRI and x-rays for the assessment of these stains. X-rays aid avulsion fracture diagnosis and MRI aids the diagnosis of strain/tear severity. MRI and x-rays are not invasive, have low adverse effects, MRI is high cost, but these tests help assess degree of severity in more severe cases which helps define surgical eligibility."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-sports-hernia",
        "label-name": "Diagnosing Sports Hernia"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nX-rays or MRI are selectively recommended to diagnose groin strains, sports hernias, or adductor-related groin pain in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:    Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:    There are no quality studies of MRI and x-rays for the assessment of these stains. X-rays aid avulsion fracture diagnosis and MRI aids the diagnosis of strain/tear severity. MRI and x-rays are not invasive, have low adverse effects, MRI is high cost, but these tests help assess degree of severity in more severe cases which helps define surgical eligibility."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-adductor-related-groin-pain",
        "label-name": "Diagnosing Adductor-related Groin Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nX-rays or MRI are selectively recommended to diagnose groin strains, sports hernias, or adductor-related groin pain in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:    Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:    There are no quality studies of MRI and x-rays for the assessment of these stains. X-rays aid avulsion fracture diagnosis and MRI aids the diagnosis of strain/tear severity. MRI and x-rays are not invasive, have low adverse effects, MRI is high cost, but these tests help assess degree of severity in more severe cases which helps define surgical eligibility."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hamstring-strain",
        "label-name": "Diagnosing Hamstring Strain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a case of a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nMRI is recommended to diagnose hamstring or hip flexor strains in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:   Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:   There are no quality studies of MRI for the assessment of these stains. MRI is not invasive, has low adverse effects, is high cost, but helps to assess degree of severity in more severe cases which helps define surgical eligibility. Thus, MRI is selectively recommended to evaluate more severe strains that are potential surgical cases."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hamstring-tear",
        "label-name": "Diagnosing Hamstring Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a case of a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nMRI is recommended to diagnose hamstring or hip flexor strains in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:   Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:   There are no quality studies of MRI for the assessment of these stains. MRI is not invasive, has low adverse effects, is high cost, but helps to assess degree of severity in more severe cases which helps define surgical eligibility. Thus, MRI is selectively recommended to evaluate more severe strains that are potential surgical cases."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hip-flexor-strains",
        "label-name": "Diagnosing Hip Flexor Strains"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a case of a moderately-severe strain in which there is consideration for surgical repair?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nMRI is recommended to diagnose hamstring or hip flexor strains in more severe cases.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications:   Severe and select cases of moderately-severe strains in which there is consideration for surgical repair.\n\nBenefits:   Identification of need for surgery\n\nRationale:   There are no quality studies of MRI for the assessment of these stains. MRI is not invasive, has low adverse effects, is high cost, but helps to assess degree of severity in more severe cases which helps define surgical eligibility. Thus, MRI is selectively recommended to evaluate more severe strains that are potential surgical cases."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteoarthrosis%2Fdiagnostic-recommendations%2Fmri,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteonecrosis%2Fdiagnostic-recommendations%2Fmri,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-fractures%2Fdiagnostic-recommendations%2Fmri,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fgroin-strains-sprains%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhamstring-hip-flexor-strains%2Fdiagnostic-recommendations",
    "odg-link": ""
  }
}
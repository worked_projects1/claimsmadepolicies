{
  "treatment": {
    "index-name": "radiograph-(x-ray)",
    "label-name": "Radiograph (X-ray)",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/18/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-osteonecrosis",
        "label-name": "Diagnosing Osteonecrosis"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nX-rays are recommended for diagnosing osteonecrosis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications:   All patients thought to have osteonecrosis.\n\nBenefits:   Identify severity, collapse and for staging, although advanced imaging is better at staging.\n\nHarms:  Negligible\n\nFrequency/Dose/Duration:   Periodically obtaining x-rays to follow the course of the disease is customary.\n\nRationale:   Radiographs have long been used for diagnosing osteonecrosis, thus there are few quality studies to evaluate diagnostic efficacy. One study suggested MRI was modestly better than plain radiographs [1262], yet radiographs are much lower cost and more widely available. X-rays are helpful to evaluate most patients with hip pain, both to diagnose and to assist with the differential diagnostic possibilities. Early stage osteonecrosis x-rays may be normal, or show slight osteopenia. A high index of suspicion is necessary. X-rays are non-invasive, are low cost, have little risk of adverse effects, have clinical utility and thus are recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hip-osteoarthrosis",
        "label-name": "Diagnosing Hip Osteoarthrosis"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nRadiographs (x-rays) are recommended to assist in diagnosing hip osteoarthrosis.\n\nStrength of Evidence – Recommended, Evidence (C)\n\n Level of Confidence – High\n\nIndications:   Nearly all patients with hip pain thought to potentially have hip OA.\n\nBenefits:  Identification of hip degenerative joint disease/OA. Helps identify severity by radiographs.\n\nHarms:  Negligible\n\nFrequency/Dose/Duration:  Generally, only obtained at presentation. Occasionally x-rayed again at follow-up to assess later progression. X-rays may be obtained postoperatively to document success of arthroplasty.\n\nIndications for Discontinuation:   N/A\n\nRationale:   X-rays are helpful for the evaluation of hip OA and to diagnose hip OA [502-504]. Although there is some evidence that MRI may be superior for imaging cartilage [503], there is no quality evidence to justify the expense of MRI for routine OA diagnostic purposes. X-rays are non-invasive, are low to moderate cost, and have little risk of adverse effects. They are helpful in the diagnosis of hip OA and therefore are recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hip-fracture",
        "label-name": "Diagnosing Hip Fracture"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nX-rays are recommended for evaluating hip fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\n Level of Confidence – High\n\nIndications:   All patients with potential hip fractures. Also in the absence of red flags, moderate to severe hip pain lasting at least a few weeks, and/or limited range of motion. The threshold for additional x-rays of the lumbar spine and/or knee should be low if the hip x-rays do not readiliy explain the symptoms and signs.\n\nBenefits:   Diagnose fracture. Also may diagnose signs of potential metastases and polyostotic bone growth.\n\nHarms:   Negligible.\n\nFrequency/Dose/Duration:   Obtaining x-rays once is generally sufficient. For patients with chronic or progressive hip pain, it may be reasonable to obtain a second set of x-rays, particularly if symptoms change.\n\nRationale:   Radiographs are not invasive, have no adverse effects, are low cost, and highly effective at diagnosing fracture and are thus recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "other-hip-pain",
        "label-name": "Other Hip Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate to severe hip pain lasting at least a few weeks, and/or limited range of motion?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the patient have red flags for hip fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nX-rays are recommended for evaluating hip fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\n Level of Confidence – High\n\nIndications:   All patients with potential hip fractures. Also in the absence of red flags, moderate to severe hip pain lasting at least a few weeks, and/or limited range of motion. The threshold for additional x-rays of the lumbar spine and/or knee should be low if the hip x-rays do not readiliy explain the symptoms and signs.\n\nBenefits:   Diagnose fracture. Also may diagnose signs of potential metastases and polyostotic bone growth.\n\nHarms:   Negligible.\n\nFrequency/Dose/Duration:   Obtaining x-rays once is generally sufficient. For patients with chronic or progressive hip pain, it may be reasonable to obtain a second set of x-rays, particularly if symptoms change.\n\nRationale:   Radiographs are not invasive, have no adverse effects, are low cost, and highly effective at diagnosing fracture and are thus recommended."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteonecrosis%2Fdiagnostic-recommendations%2Fradiographs,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteoarthrosis%2Fdiagnostic-recommendations%2Fradiographs,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-fractures%2Fdiagnostic-recommendations%2Fradiographs",
    "odg-link": ""
  }
}
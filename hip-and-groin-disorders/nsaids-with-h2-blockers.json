{
  "treatment": {
    "index-name": "nsaids-with-h2-blockers",
    "label-name": "NSAIDs with H2 Blockers",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "hip-and-groin-disorders",
    "label-name": "Hip and Groin Disorders"
  },
  "version": {
    "mtus": "10/07/19",
    "odg": "",
    "policy-doc": "01/17/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patients-at-risk-for-gi-adverse-effects",
        "label-name": "Patients at Risk for GI Adverse Effects"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is the patient a a high-risk factor profile who also has indications for NSAIDs? (At-risk patients include those with a history of prior gastrointestinal bleeding, the elderly, diabetics, and cigarette smokers).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.\n\nConcomitant prescriptions of cytoprotective medications are recommended for patients at increased risk for gastrointestinal bleeding from NSAIDs.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – High\n\nIndications:   Patients with a high-risk factor profile who also have indications for NSAIDs, cytoprotective medications, particularly if longer term treatment is contemplated. At-risk patients include those with a history of prior gastrointestinal bleeding, the elderly, diabetics, and cigarette smokers. Physicians are cautioned that H2 blockers might not protect from gastric ulcers [703-705].\n\nBenefits:  Reduced risk of gastrointestinal bleeding\n\nHarms:   Generally minor adverse effects. PPIs have long term risks of fractures, hypomagnesemia, QT prolongation, kidney disease, vitamin B12 deficiency, iron deficiency.\n\nFrequency/Dose/Duration:  Dose and frequency for proton pump inhibitors, sucralfate, and H2 blockers are as recommended by manufacturer. Duration is the extent of the NSAID therapy; use is at times permanent for those with recurrent bleeds or other complications.\n\nIndications for Discontinuation:  Resolution of need for NSAID, discontinuation of NSAID, adverse effects, non-compliance.\n\nRationale:   Risks of gastrointestinal events are also recommended for assessment, particularly including prior history of gastrointestinal bleeding and source, length of treatment, age, smoking, diabetes mellitus and other medical factors. Those with greater risk should be considered for treatment with acetaminophen, NSAID plus misoprostol, proton pump inhibitors (see below), or a COX-2 selective agent (see NSAIDs/acetaminophen evidence table) [661, 666-668, 680, 681].\n\nGastrointestinal adverse events are generally considered the most significant of the risks of NSAIDs. A large volume of high- and moderate-quality evidence consistently shows proton pump inhibitors are effective for prevention and or treatment of gastric and duodenal ulcers and erosions [682-691]. There is only one quality head-to-head trial, and it found no difference in efficacy between pantoprazole and omeprazole [684]. Misoprostol has also been consistently shown to be effective compared with placebo [692-701]. Relatively fewer studies have shown sucralfate to be effective compared with placebo [702]; H2 blockers appear more effective for treatment of duodenal than gastric mucosa [703-705]. There are relatively few quality trials comparing efficacy of the different classes of agents. Pantoprazole but not lansoprazole has been found modestly superior to misoprostol [706, 707]. No difference was found between famotidine and lansoprazole [708]. Misoprostol has been reported superior to ranitidine [709, 710], cimetidine [697], and sucralfate [697, 712]. In short, while the evidence is not definitive, available quality evidence suggests proton pump inhibitors and misoprostol appear superior to H-2 blockers and sucralfate. While COX-2 selective agents have generally been recommended as either third- or fourth-line medications for routine use in osteoarthrosis patients, when there is a risk of gastrointestinal complications, they are often preferred. For patients at high risk of gastrointestinal bleeding, there is evidence that a combination of proton pump inhibitor plus COX-2 selective agent is efficacious."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fhip-and-groin-disorders%2Fhip-osteoarthrosis%2Ftreatment-recommendations%2Fmedications%2Fcytoprotective-agents",
    "odg-link": ""
  }
}
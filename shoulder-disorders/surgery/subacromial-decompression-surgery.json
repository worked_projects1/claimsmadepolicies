{
  "treatment": {
    "index-name": "subacromial-decompression-surgery",
    "label-name": "Subacromial Decompression Surgery",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "11/04/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "impingement-syndrome",
        "label-name": "Impingement Syndrome"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have adhesive capsulitis or shoulder stiffness (could be counter-indications for surgery)?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain (e.g., symptomatic with positive supraspinatus test, impingement signs)?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced active shoulder ROM or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Are there imaging findings by MRI or ultrasound of impingement or rotator cuff tendinoses consistent with symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Is there a temporary resolution or marked reduction in pain immediately after injection of a local anesthetic into the subacromial space?",
          "option-list": [
            {
              "option": "yes",
              "action": "6"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "6",
          "question": "Has the injured worker failed one or more glucocorticosteroid injections (see above)?",
          "option-list": [
            {
              "option": "yes",
              "action": "7"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "7",
          "question": "Has the injured worker failed at least one trial of a quality rehabilitation program that follows evidence-based guidelines (see above)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Counter-indications – Adhesive capsulitis or shoulder stiffness.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nRecommended. Subacromial decompression surgery is recommended for treatment of select patients with impingement syndrome/rotator cuff tendinoses.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: All of the following: 1) shoulder joint pain (e.g., symptomatic with positive supraspinatus test, impingement signs); 2) reduced active shoulder ROM or impaired function 3) imaging findings by MRI or ultrasound of rotator cuff tendinoses consistent with symptoms; and 4) temporary resolution or marked reduction in pain immediately after injection of a local anesthetic into the subacromial space. Patients should also have failed one or more glucocorticosteroid injections (see above) and at least one trial of a quality rehabilitation program that follows evidence-based guidelines (see above). (Haahr 05, 06; Brox 93, 99; Rahme 98; Sachs 94; Husby 03; Lindh 93; Michener 04)\n\nRationale: There are no sham-surgery controlled trials of surgical interventions for impingement syndrome. There are three moderate quality RCTs with four total reports that compared subacromial decompression plus physical therapy versus physical therapy exercises for treatment of impingement syndrome. (Brox 93, 99; Rahme 98; Haahr 05, 06; Constant 89) Importantly, one of these trials included a comparison with both exercise as well as sham-laser treatment. (Brox 93; Brox 99) That trial found surgery and rehabilitation superior to placebo laser and provides the primary basis for an evidence-based recommendation in favor of surgery. All of these trials comparing physical therapy/exercise with surgery appear to have considerable biases in favor of surgery over physiotherapy/exercise for at least two major reasons: 1) patients invariably appear to have been required to fail prior non-operative treatment that when described included considerable exercise components (thus a “more of the same” bias against physical therapy/ exercise); and 2) likely greater treatment contact time in the surgical groups which were combined with physical therapy/exercise. Except for Rahme’s 1998 study, these studies reported mostly failed prior rehabilitation and found surgery superior to physical therapy exercise. (Brox 93, 99; Haahr 05, 06) However, it also has been noted that there is a high rate of crossover to surgery over time. (Brox 99)\n\nThere is moderate-quality evidence that there are no long-term differences associated with arthroscopic compared to open decompression to treat impingement syndrome/rotator cuff tendinoses, (Husby 03; Lindh 93; Sachs 94) although there is some evidence of a modest short-term advantage of arthroscopy over open decompression for faster recovery. (Sachs 94) (A low-quality trial also reported similar evidence. (T’Jonck 97)) Open acromioplasty in patients with impingement syndrome appears not to prevent progression to rotator cuff tear in a nine-year followup study. (Hyvonen 98)   A case-control study found no evidence that calcium deposits in the rotator cuff seen on x-ray affected outcomes at 2 years after arthroscopic subacromial decompression. (Tillander 98) Experience of the surgeon and patient factors require judgment in selecting operative approaches. Long-term outcomes of up to 25 years have also reported excellent or good results in 77% of patients with various arthroscopic decompression techniques. (Odenbring 08; Ellman 91; Chin 07; Budoff 05)\n\nLimited motion may indicate adhesive capsulitis or capsular stiffness that would be a contraindication to surgery. Patients with rotator cuff syndromes or impingement typically do not have significant limitations of passive motion and if they do, then the diagnosis may be in doubt. Surgery is invasive, has adverse effects, and is costly. However, in carefully select patients with impingement syndrome/rotator cuff tendinoses who have failed quality non-operative treatments, benefits appear to outweigh risks and surgery is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "rotator-cuff-tendinoses",
        "label-name": "Rotator Cuff Tendinoses"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have adhesive capsulitis or shoulder stiffness (could be counter-indications for surgery)?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain (e.g., symptomatic with positive supraspinatus test, impingement signs)?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced active shoulder ROM or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Are there imaging findings by MRI or ultrasound of impingement or rotator cuff tendinoses consistent with symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Is there a temporary resolution or marked reduction in pain immediately after injection of a local anesthetic into the subacromial space?",
          "option-list": [
            {
              "option": "yes",
              "action": "6"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "6",
          "question": "Has the injured worker failed one or more glucocorticosteroid injections (see above)?",
          "option-list": [
            {
              "option": "yes",
              "action": "7"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "7",
          "question": "Has the injured worker failed at least one trial of a quality rehabilitation program that follows evidence-based guidelines (see above)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Counter-indications – Adhesive capsulitis or shoulder stiffness.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nRecommended. Subacromial decompression surgery is recommended for treatment of select patients with impingement syndrome/rotator cuff tendinoses.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: All of the following: 1) shoulder joint pain (e.g., symptomatic with positive supraspinatus test, impingement signs); 2) reduced active shoulder ROM or impaired function 3) imaging findings by MRI or ultrasound of rotator cuff tendinoses consistent with symptoms; and 4) temporary resolution or marked reduction in pain immediately after injection of a local anesthetic into the subacromial space. Patients should also have failed one or more glucocorticosteroid injections (see above) and at least one trial of a quality rehabilitation program that follows evidence-based guidelines (see above). (Haahr 05, 06; Brox 93, 99; Rahme 98; Sachs 94; Husby 03; Lindh 93; Michener 04)\n\nRationale: There are no sham-surgery controlled trials of surgical interventions for impingement syndrome. There are three moderate quality RCTs with four total reports that compared subacromial decompression plus physical therapy versus physical therapy exercises for treatment of impingement syndrome. (Brox 93, 99; Rahme 98; Haahr 05, 06; Constant 89) Importantly, one of these trials included a comparison with both exercise as well as sham-laser treatment. (Brox 93; Brox 99) That trial found surgery and rehabilitation superior to placebo laser and provides the primary basis for an evidence-based recommendation in favor of surgery. All of these trials comparing physical therapy/exercise with surgery appear to have considerable biases in favor of surgery over physiotherapy/exercise for at least two major reasons: 1) patients invariably appear to have been required to fail prior non-operative treatment that when described included considerable exercise components (thus a “more of the same” bias against physical therapy/ exercise); and 2) likely greater treatment contact time in the surgical groups which were combined with physical therapy/exercise. Except for Rahme’s 1998 study, these studies reported mostly failed prior rehabilitation and found surgery superior to physical therapy exercise. (Brox 93, 99; Haahr 05, 06) However, it also has been noted that there is a high rate of crossover to surgery over time. (Brox 99)\n\nThere is moderate-quality evidence that there are no long-term differences associated with arthroscopic compared to open decompression to treat impingement syndrome/rotator cuff tendinoses, (Husby 03; Lindh 93; Sachs 94) although there is some evidence of a modest short-term advantage of arthroscopy over open decompression for faster recovery. (Sachs 94) (A low-quality trial also reported similar evidence. (T’Jonck 97)) Open acromioplasty in patients with impingement syndrome appears not to prevent progression to rotator cuff tear in a nine-year followup study. (Hyvonen 98)   A case-control study found no evidence that calcium deposits in the rotator cuff seen on x-ray affected outcomes at 2 years after arthroscopic subacromial decompression. (Tillander 98) Experience of the surgeon and patient factors require judgment in selecting operative approaches. Long-term outcomes of up to 25 years have also reported excellent or good results in 77% of patients with various arthroscopic decompression techniques. (Odenbring 08; Ellman 91; Chin 07; Budoff 05)\n\nLimited motion may indicate adhesive capsulitis or capsular stiffness that would be a contraindication to surgery. Patients with rotator cuff syndromes or impingement typically do not have significant limitations of passive motion and if they do, then the diagnosis may be in doubt. Surgery is invasive, has adverse effects, and is costly. However, in carefully select patients with impingement syndrome/rotator cuff tendinoses who have failed quality non-operative treatments, benefits appear to outweigh risks and surgery is recommended."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Frotator-cuff%2Fsurgical-considerations",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "rotator-cuff-repair",
    "label-name": "Rotator Cuff Repair",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "11/04/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-massive-rotator-cuff-tear",
        "label-name": "Acute Massive Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is the tear acute and greater than 5cm?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced range of motion of the shoulder or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Does the injured worker have imaging findings by MRI, MR arthrography, or ultrasound of massive rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended.Rotator cuff repair is recommended for treatment of acute massive tears (>5cm).\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: All of the following: 1) shoulder joint pain; 2) reduced range of motion of the shoulder or impaired function; 3) imaging findings by MRI, MR arthrography, or ultrasound of massive rotator cuff tear. "
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-massive-rotator-cuff-tear",
        "label-name": "Chronic Massive Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is the tear greater than 5cm and chronic?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced range of motion of the shoulder or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Does the injured worker have imaging findings by MRI, MR arthrography, or ultrasound of massive rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Does the injured worker have poor function that is felt to necessitate surgical intervention?",
          "option-list": [
            {
              "option": "yes",
              "action": "6"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "6",
          "question": "Is there a likelihood for significant improvement with surgery?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Not Recommended. Rotator cuff repair is not generally recommended for treatment of chronic massive tears (>5cm).\n\nSurgical Considerations\n\nStrength of Evidence – Not Recommended, Evidence (C)\n\nIndications: While generally not recommended, if surgery is felt to be indicated for a particular patient, all of the following should be present: 1) shoulder joint pain; 2) reduced range of motion of the shoulder or impaired function; and 3) imaging findings by MRI, MR arthrography, or ultrasound of massive rotator cuff tear, 4) poor function that is felt to both necessitate surgical intervention and, 5) there is likelihood for significant improvement with surgery for that particular patient."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "small-rotator-cuff-tear",
        "label-name": "Small Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a tear less than 5cm?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced ROM of the shoulder or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Does the injured worker have imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Does the injured worker agree to participate fully in post operative active rehabilitation and understand there is a long recovery time? (Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Moderately Recommended. Rotator cuff repair is moderately recommended for treatment of small, medium, or large tears (<5cm).\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nIndications: All the following: 1) shoulder joint pain; 2) reduced ROM of the shoulder or impaired function; 3) imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear. Patient must agree to participate fully in post operative active rehabilitation and understand there is a long recovery time. Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery. (Moosmayer 10, 14; Kukkonen 14)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "medium-rotator-cuff-tear",
        "label-name": "Medium Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a tear less than 5cm?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced ROM of the shoulder or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Does the injured worker have imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Does the injured worker agree to participate fully in post operative active rehabilitation and understand there is a long recovery time? (Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Moderately Recommended. Rotator cuff repair is moderately recommended for treatment of small, medium, or large tears (<5cm).\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nIndications: All the following: 1) shoulder joint pain; 2) reduced ROM of the shoulder or impaired function; 3) imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear. Patient must agree to participate fully in post operative active rehabilitation and understand there is a long recovery time. Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery. (Moosmayer 10, 14; Kukkonen 14)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "large-rotator-cuff-tear",
        "label-name": "Large Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a tear less than 5cm?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder joint pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Does the injured worker have reduced ROM of the shoulder or impaired function?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Does the injured worker have imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Does the injured worker agree to participate fully in post operative active rehabilitation and understand there is a long recovery time? (Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Moderately Recommended. Rotator cuff repair is moderately recommended for treatment of small, medium, or large tears (<5cm).\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nIndications: All the following: 1) shoulder joint pain; 2) reduced ROM of the shoulder or impaired function; 3) imaging findings by MRI, MR arthrography, or ultrasound of rotator cuff tear. Patient must agree to participate fully in post operative active rehabilitation and understand there is a long recovery time. Pre-operative physical therapy is an option (but not a pre-operative requirement) as many patients sufficiently recover without surgery. (Moosmayer 10, 14; Kukkonen 14)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Frotator-cuff%2Fsurgical-considerations",
    "odg-link": ""
  }
}
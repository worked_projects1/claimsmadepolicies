{
  "treatment": {
    "index-name": "helical-computed-tomography/-helical-ct-scan",
    "label-name": "Helical Computed Tomography/ Helical CT Scan",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "10/25/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluating-osteonecrosis",
        "label-name": "Evaluating Osteonecrosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have shoulder pain from osteonecrosis with contraindications for MRI (e.g., implanted hardware)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have shoulder pain from osteonecrosis with increased polyostotic bone metabolism?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Helical CT is recommended for evaluation of patients with osteonecrosis who have contraindications for MRI.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with shoulder pain from osteonecrosis with contraindications for MRI (e.g., implanted hardware) or increased polyostotic bone metabolism.  "
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-shoulder-pain",
        "label-name": "Acute Shoulder Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a need for advanced bony structure imaging?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker need advanced imaging, but has contraindications for MRI (e.g., implanted hardware)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Helical CT is recommended for select patients with acute, subacute, or chronic shoulder pain in whom advanced imaging of bony structures is thought to potentially be helpful. It is also recommended for those who need advanced imaging, but have contraindications for MRI.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with acute, subacute or chronic shoulder pain with need for advanced bony structure imaging. Patients needing advanced imaging, but with contraindications for MRI (e.g., implanted hardware) are also candidates.                       \n\nRationale: Helical CT scanning has been largely replaced by MRI. However, there are patients who have contraindications for MRI (e.g., implanted ferrous metal) helical CT is recommended. Helical CT scan has been thought to be superior to MRI for evaluating subchondral fractures; however, a definitive study has not been reported. (Stevens 03) Helical CT has few if any adverse effects, but is costly. It is recommended for select use."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-shoulder-pain",
        "label-name": "Subacute Shoulder Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a need for advanced bony structure imaging?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker need advanced imaging, but has contraindications for MRI (e.g., implanted hardware)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Helical CT is recommended for select patients with acute, subacute, or chronic shoulder pain in whom advanced imaging of bony structures is thought to potentially be helpful. It is also recommended for those who need advanced imaging, but have contraindications for MRI.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with acute, subacute or chronic shoulder pain with need for advanced bony structure imaging. Patients needing advanced imaging, but with contraindications for MRI (e.g., implanted hardware) are also candidates.                       \n\nRationale: Helical CT scanning has been largely replaced by MRI. However, there are patients who have contraindications for MRI (e.g., implanted ferrous metal) helical CT is recommended. Helical CT scan has been thought to be superior to MRI for evaluating subchondral fractures; however, a definitive study has not been reported. (Stevens 03) Helical CT has few if any adverse effects, but is costly. It is recommended for select use."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-shoulder-pain",
        "label-name": "Chronic Shoulder Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have a need for advanced bony structure imaging?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker need advanced imaging, but has contraindications for MRI (e.g., implanted hardware)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Helical CT is recommended for select patients with acute, subacute, or chronic shoulder pain in whom advanced imaging of bony structures is thought to potentially be helpful. It is also recommended for those who need advanced imaging, but have contraindications for MRI.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with acute, subacute or chronic shoulder pain with need for advanced bony structure imaging. Patients needing advanced imaging, but with contraindications for MRI (e.g., implanted hardware) are also candidates.                       \n\nRationale: Helical CT scanning has been largely replaced by MRI. However, there are patients who have contraindications for MRI (e.g., implanted ferrous metal) helical CT is recommended. Helical CT scan has been thought to be superior to MRI for evaluating subchondral fractures; however, a definitive study has not been reported. (Stevens 03) Helical CT has few if any adverse effects, but is costly. It is recommended for select use."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Fdiagnostic-recommendations%2Fhelical-ct",
    "odg-link": ""
  }
}
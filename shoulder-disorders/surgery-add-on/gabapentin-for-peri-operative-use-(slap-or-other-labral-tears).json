{
  "treatment": {
    "index-name": "gabapentin-for-peri-operative-use-(slap-or-other-labral-tears)",
    "label-name": "Gabapentin for Peri-operative Use (SLAP or other Labral Tears)",
    "treatment-type": "surgery-add-on"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "10/27/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "surgery-add-on",
        "label-name": "Surgery Add-on"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Muscle relaxants, capsicum, tricyclic antidepressants or dual reuptake inhibiting anti-depressants for chronic pain (but not SSRI antidepressants which are not effective for nociceptive pain), or gabapentin for peri-operative use are recommended to control pain associated with superior labral anterior posterior (SLAP) or other labral tears.\n\nMedications (including topical creams)\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nRationale: There are no quality trials evaluating treatment of labral tears with medications. NSAIDs have been evaluated for the treatment of many musculoskeletal disorders and found uniformly effective (see Rotator Cuff Tendinopathies). NSAIDs and acetaminophen are not invasive and have low adverse effects profiles, particularly when used for short courses in occupational populations. Generic or over-the-counter formulations are low cost. NSAIDs and acetaminophen also may help avoid treatment with opioids which have far worse adverse effect profiles (see Chronic Pain Guideline). By analogy to treatment of other musculoskeletal conditions such as low back pain (see Low Back Disorders Guideline), acetaminophen is believed to be less efficacious, although it generally has a lower adverse effect profile.\n\nThere are no quality studies evaluating opioids for treatment of shoulder labral tear patients (see Rotator Cuff Tendinopathies and Chronic Pain Guidelines); thus quality evidence of long-term efficacy is lacking. Opioids have adverse effects with published evidence of high mortality risks. There are patients with severe pain, particularly select acute tear patients, for whom the brief use of opioids, especially to facilitate sleep, are recommended. Opioids are not invasive, have high adverse effects for a pharmaceutical (although tolerance may develop relatively rapidly), and are low cost when generic formulations are used.\n\nOther medications are rarely required for labral tear patients, as the associated pain is usually acute and not subacute or chronic. Norepinephrine reuptake inhibiting anti-depressants (e.g., amitriptyline, doxepin, imipramine, desipramine, nortriptyline, protriptyline, maprotiline, and clomipramine) and mixed norepinephrine and serotonin inhibitors (e.g., venlafaxine, bupropion, and duloxetine) have evidence of efficacy for treatment of chronic low back pain and some other chronic pain conditions (see Low Back Disorders Guideline). However, while there is no quality evidence evaluating these medications for treatment of shoulder pain, they appear likely to be mildly effective for some patients, especially in cases involving the shoulder girdle and myofascial pain.\n\nThere are no quality studies that address the use of anti-convulsant agents to treat patients with shoulder pain. By analogy, there is quality evidence that topiramate is weakly effective for treatment of low-back pain patients and gabapentin is unhelpful. However, there is quality evidence that gabapentin reduces the need for opioids when administered as part of peri-operative hip surgery patients’ pain management (see Hip and Groin Disorders Guideline).\n\nSkeletal muscle relaxants may be a reasonable alternative to spare opioid requirements in the acute recovery period and to facilitate sleep. However, daytime somnolence limits their use. Skeletal muscle relaxants are not recommended for continuous management of subacute or chronic shoulder pain, although they may be reasonable options for select acute pain exacerbations or for a limited trial as a 3rd- or 4th-line agent in more severely affected patients in whom NSAIDs and exercise have failed to control symptoms."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Fslap-tears%2Fmedications",
    "odg-link": ""
  }
}
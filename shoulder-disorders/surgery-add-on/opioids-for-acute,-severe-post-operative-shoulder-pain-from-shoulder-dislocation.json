{
  "treatment": {
    "index-name": "opioids-for-acute,-severe-post-operative-shoulder-pain-from-shoulder-dislocation",
    "label-name": "Opioids for Acute, Severe Post-operative Shoulder Pain from Shoulder Dislocation",
    "treatment-type": "surgery-add-on"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "11/09/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "surgery-add-on",
        "label-name": "Surgery Add-on"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have acute, severe post-operative pain due to shoulder dislocation?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have severe injury with a clear rationale for use (objective functional limitations due to pain resulting from the medical problem?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Has the injured worker had other more efficacious treatments,  and either: failed and/or have reasonable expectations of the immediate need for an opioid to obtain sleep the evening after the injury.",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Have prescription databases (usually referred to as Prescription Drug Monitoring Program (PDMP)) been checked and not shown evidence for conflicting opioid prescriptions from other providers or evidence of misreporting?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Have non-opioid prescriptions (e.g., NSAIDs, acetaminophen) absent contraindication(s) been used as the primary treatment and will accompany an opioid prescription?",
          "option-list": [
            {
              "option": "yes",
              "action": "6"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "6",
          "question": "Select option \"Yes\" if user has closely read and understood the following. \n1) Low-dose opioids may be needed in the elderly who have greater susceptibility to the adverse risks of opioids. Those of lower body weight may also require lower opioid doses.\n\n2)Dispensing quantities should be only what is needed to treat the pain. Short-acting opioids are recommended for treatment of acute pain. Long-acting opioids are not recommended.",
          "option-list": [
            {
              "option": "yes",
              "action": "7"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "7",
          "question": "Due to greater than 10-fold elevated risks of adverse effects and death, considerable caution is warranted among those using other sedating medications and substances including: i) benzodiazepines, ii) anti-histamines (H1-blockers), and/or iii) illicit substances.(105, 109, 167, 168) Patients should not receive opioids if they use illicit substances unless there is objective evidence of significant trauma or moderate to severe injuries. Considerable caution is also warranted among those who are unemployed as the reported risks of death are also greater than 10-fold.(109, 167) Due to elevated risk of death and adverse effects, caution is also warranted when considering prescribing an opioid for patients with any of the following characteristics: depression, anxiety, personality disorder, untreated sleep disorders, substance abuse history, current alcohol use or current tobacco use, attention deficit hyperactivity disorder (ADHD), post-traumatic stress disorder (PTSD), suicidal risk, impulse control problems, thought disorders, psychotropic medication use, chronic obstructive pulmonary disease (COPD), asthma, or recurrent pneumonia.(78, 102, 104, 108, 109, 169-186) Considerable caution is also warranted among those with other comorbidities such as chronic hepatitis and/or cirrhosis,(187) as well as coronary artery disease, dysrhythmias, cerebrovascular disease, orthostatic hypotension, asthma, recurrent pneumonia, thermoregulatory problems, advanced age (especially with mentation issues, fall risk, debility), osteopenia, osteoporosis, water retention, renal failure, severe obesity, testosterone deficiency, erectile dysfunction, abdominal pain, gastroparesis, constipation, prostatic hypertrophy, oligomenorrhea, pregnancy, human immunodeficiency virus (HIV), ineffective birth control, herpes, allodynia, dementia, cognitive dysfunction and impairment, gait problems, tremor, concentration problems, insomnia, coordination problems, and slow reaction time. There are considerable drug-drug interactions that have been reported (see Opioids Guideline, Appendices 2-3). Does the injured worker have contraindications for taking opioids as described above?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "approve"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Judicious short term use of opioids is recommended for treatment of acute, severe post-operative pain due to shoulder dislocation.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients should meet all of the following:\n\n1)         Severe injury with a clear rationale for use (objective functional limitations due to pain resulting from the medical problem.\n\n2)         Other more efficacious treatments should have been instituted,  and either:\n\n2a) failed and/or\n\n2b) have reasonable expectations of the immediate need for an opioid to obtain sleep the evening after the injury.\n\n3)         Where available, prescription databases (usually referred to as Prescription Drug Monitoring Program (PDMP)) should be checked and not show evidence for conflicting opioid prescriptions from other providers or evidence of misreporting.\n\n4)         Non-opioid prescriptions (e.g., NSAIDs, acetaminophen) absent contraindication(s) should nearly always be the primary treatment and accompany an opioid prescription.\n\n5)         Low-dose opioids may be needed in the elderly who have greater susceptibility to the adverse risks of opioids. Those of lower body weight may also require lower opioid doses.\n\n6)         Dispensing quantities should be only what is needed to treat the pain. Short-acting opioids are recommended for treatment of acute pain. Long-acting opioids are not recommended.\n\n7)         Due to greater than 10-fold elevated risks of adverse effects and death, considerable caution is warranted among those using other sedating medications and substances including: i) benzodiazepines, ii) anti-histamines (H1-blockers), and/or iii) illicit substances.(105, 109, 167, 168) Patients should not receive opioids if they use illicit substances unless there is objective evidence of significant trauma or moderate to severe injuries. Considerable caution is also warranted among those who are unemployed as the reported risks of death are also greater than 10-fold.(109, 167) Due to elevated risk of death and adverse effects, caution is also warranted when considering prescribing an opioid for patients with any of the following characteristics: depression, anxiety, personality disorder, untreated sleep disorders, substance abuse history, current alcohol use or current tobacco use, attention deficit hyperactivity disorder (ADHD), post-traumatic stress disorder (PTSD), suicidal risk, impulse control problems, thought disorders, psychotropic medication use, chronic obstructive pulmonary disease (COPD), asthma, or recurrent pneumonia.(78, 102, 104, 108, 109, 169-186) Considerable caution is also warranted among those with other comorbidities such as chronic hepatitis and/or cirrhosis,(187) as well as coronary artery disease, dysrhythmias, cerebrovascular disease, orthostatic hypotension, asthma, recurrent pneumonia, thermoregulatory problems, advanced age (especially with mentation issues, fall risk, debility), osteopenia, osteoporosis, water retention, renal failure, severe obesity, testosterone deficiency, erectile dysfunction, abdominal pain, gastroparesis, constipation, prostatic hypertrophy, oligomenorrhea, pregnancy, human immunodeficiency virus (HIV), ineffective birth control, herpes, allodynia, dementia, cognitive dysfunction and impairment, gait problems, tremor, concentration problems, insomnia, coordination problems, and slow reaction time. There are considerable drug-drug interactions that have been reported (see Opioids Guideline, Appendices 2-3).\n\nFrequency/Dose/Duration: Generally, opioids should be prescribed at night or while not working.(82) Lowest effective, short-acting opioid doses are preferable as they tend to have the better safety profiles, less risk of escalation,(188) less risk of lost time from work,(112) and faster return to work.(189) Short-acting opioids are recommended for treatment of acute pain and long-acting opioids are not recommended. Recommend opioid use as required by pain, rather than in regularly scheduled dosing.\n\nIf parenteral administration is required, ketorolac has demonstrated superior efficacy compared with opioids for acute severe pain,(190, 191) although ketorolac’s risk profile may limit use for some patients. Parenteral opioid administration outside of obvious acute trauma or surgical emergency conditions is almost never required, and requests for such treatment are clinically viewed as red flags for potential substance abuse. \n\nIndications for Discontinuation: Resolution of pain, sufficient improvement in pain, intolerance or adverse effects, non-compliance, surreptitious medication use, consumption of medications or substances advised to not take concomitantly (e.g., sedating medications, alcohol, benzodiazepines), or use beyond 2 weeks.\\\n\nHarms: Adverse effects are many (see section below on “Opioids Benefits and Harms”).\n\nBenefits: Improved short-term pain control."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Fshoulder-dislocation%2Fmedications",
    "odg-link": ""
  }
}
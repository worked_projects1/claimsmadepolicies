{
  "treatment": {
    "index-name": "mri-(immediate-imaging)",
    "label-name": "MRI (Immediate Imaging)",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "10/28/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosis-of-acute-rotator-cuff-tear",
        "label-name": "Diagnosis of Acute Rotator Cuff Tear"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is the injured worker suspected of having an acute, clinically significant rotator cuff tear?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the injured worker have significant rotator cuff weakness? (Exceptions include elderly patients or those who have substantial signs of pre-existing large/massive rotator cuff tear. It is also reasonable to wait for 1 or 2 weeks to ascertain whether the condition is likely to resolve with conservative care without obtaining an MRI.)",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for diagnosing osteonecrosis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with subacute or chronic shoulder pain thought to be related to osteonecrosis (AVN), particularly in whom the diagnosis is unclear or in whom additional diagnostic evaluation and staging is needed.              \n\nRationale: There is one moderate-quality study comparing MRI with arthrography, suggesting MRI is superior to arthrography; (Blanchard 99) however, arthrography alone has been largely replaced by other procedures. Otherwise, MRI has not been evaluated in high-quality studies for shoulder joint pathology. (Kassarjian 05; Leunig 04; Dinnes 03) MRI appears particularly helpful for soft tissue abnormalities. MRI has been suggested for evaluations of patients with symptoms over 3 months. (Kassarjian 05; Armfield 06; Bredella 05) MRI was compared with arthroscopy in 57 patients with shoulder pain of unclear cause. (Torstensen 99) MRI was found to be accurate in detecting 68% of rotator cuff tears and 62% accurate in detecting labral injuries. MRI sensitivity for RC tears was 96% and specificity 49% (for labral tears, 73% sensitive, 58% specific). The authors concluded that “MRI does not appears to be an accurate effective tool for assessing shoulder pathologic conditions in patients in whom the clinical picture is not clear and therefore may not be of assistance in surgical planning for patients with these difficult conditions.” MRI was compared with arthroscopic findings among 16 patients with trauma. (Kirkley 03) The authors found moderate correlation for superior labral lesions (k = 0.60), fair agreement for rotator cuff tear (k = 0.355), Hill-Sachs (k = 1.0), and moderate for size (k = 0.44). A consecutive case series of 104 patients with shoulder problems were evaluated and randomized to MRI first versus arthrography first. There were modestly fewer changes in diagnostic categories with MRI (30%) than arthrography (37%), p >0.5. MRI led to slightly more changes in planned therapy (36% vs. 25%, p >0.3). MRI was found to be 79% accurate, 81% sensitive and 78% specific for full-thickness rotator cuff tears. Arthrography was found to be 82% accurate, 50% sensitive and 96% specific. (Blanchard 99) A cross-sectional comparison of MRI (1.5T loop-gap resonator surface coil), double contrast arthrography, high resolution sonography and surgery among 38 patients with suspected rotator cuff tears did not include all patients receiving all tests or surgery (other than MRI and arthrography) and reported a sensitivity of MRI of 100%. (Burk 89). Ultrasound detected 9/15 (60%) of tears. However, the study population was small and biased in favor of overestimating the tests’ sensitivity.\n\nMRI has shown increased changes in the rotator cuff and tears with increased age, (Needell 96; Sher 95) as well as a high prevalence of bony and peritendinous shoulder abnormalities among those without symptoms. (Needell 96) MRI has reasonably good operant characteristics for full-thickness tears, although it does not have good sensitivity for partial thickness tears. (Dinnes 03) Fatty infiltration of the rotator cuff tendons is also found on MRI and thought to signify chronicity as well as portending a poorer surgical outcome. (Berhouet 09) A comparative assessment of T-2 weighted fast spin-echo technique with vs. without fat-suppression MRI for assessment of rotator cuff tears among 177 patients thought to have tears found no differences in assessments of complete tears, but differed in interpretations of partial tears. (Singson 96) Compared with surgery, sensitivity was 100% for full-thickness tears and specificity for intact tendons was 86%. Fat suppression was felt helpful for partial tears. MRI demonstrates acromial abnormalities and there is a higher prevalence of Type 3 acromion processes among those with either rotator cuff tear or impingement syndrome. (Epstein 93) It has been suggested increased T2 signal in the distal clavicle may be an indication for surgical resection.\n\nThere are no quality studies evaluating the use of MRI for osteonecrosis, although it appears helpful for staging osteonecrosis. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-ray in patients with osteonecrosis. (Stevens 03) There are concerns that MRI is inferior to MR arthrography for evaluating the labrum, (Schmerl 05) thus MRI is recommended for evaluation of the joint. MRI is suboptimal for the labrum. MRI is not invasive, has potential adverse effects from issues of claustrophobia or complications of medication, but is costly. MRI is not recommended for routine shoulder imaging, but is recommended for select shoulder joint pathology particularly involving concerns regarding soft tissue pathology. "
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Fdiagnostic-recommendations%2Fmri",
    "odg-link": ""
  }
}
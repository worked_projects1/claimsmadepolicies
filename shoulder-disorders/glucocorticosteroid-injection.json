{
  "treatment": {
    "index-name": "glucocorticosteroid-injection",
    "label-name": "Glucocorticosteroid Injection",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "10/27/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "adhesive-capsulitis",
        "label-name": "Adhesive Capsulitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have mild adhesive capsulitis with insufficient control or progress with NSAID?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is this a moderate to severe case of adhesive capsulitis?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Strongly Recommended. Glucocorticoid injections are strongly recommended for treatment of adhesive capsulitis.\n\nInjection Therapy\n\nStrength of Evidence – Strongly Recommended, Evidence (A)\n\nIndications: Adhesive capsulitis, mild cases with insufficient control or progress with NSAID, or moderate or severe cases. (Loew 05)\n\nFrequency/Dose/Duration: One injection recommended and results assessed. Injections have been performed in the glenohumeral joint, (Jacobs 09) subacromial space, (de Jong 98; Valtonen 74) 2 injection points, (Ryans 05) as well as targeting the shoulder capsule. (Dacre 89) One trial suggested no differences in outcomes between bursal injections and intra-articular injections. (Rizk 91) There are no quality trials comparing these different approaches. A second injection may be reasonable, particularly if the initial results are partial but insufficient. Subsequent injection(s) should generally be based on objective evidence of progress attributable to the injection(s), but with insufficient or incomplete results. If an initial injection is unsuccessful, a different approach is suggested. A third injection is not recommended if there is not objective response to the 2 prior injections. Injection combined with exercises is recommended. (Carette 03)\n\nQuality trials have utilized triamcinolone hexacetonide 40mg, (Carette 03) triamcinolone acetonide 10mg, (de Jong 98) and 40mg. (de Jong 98; van der Windt 98) Trials have both used fluoroscopy (Carette 03), ultrasound, (Lee 09) and have used no imaging for the injection(s). There is only one study suggesting better results with ultrasound than blind injections, (Lee 09) resulting in limited evidence on that question and need for further studies. One high-quality trial suggested triamcinolone acetonide 10mg was inferior to 40mg, thus, triamcinolone acetonide 40mg is the recommended dose for that glucocorticoid. (de Jong 98) Quality trials have suggested superior results with injection compared with oral steroids (Widiastuti-Samekto 04), but not NSAIDs where two studies conflict regarding superiority. (Dehghan 13; Shin 13)\n\nIndications for Discontinuation: Recovery, plateau in recovery, intolerance.\n\nRationale: There are multiple high- and moderate-quality trials that have evaluated glucocorticoid injections for treatment of adhesive capsulitis. (Carette 03; de Jong 98; Ryans 05; van der Windt 98; Jacobs 09; Dacre 89; Valtonen 74; Rizk 91; Gam 98; Widiastuti-Samekto 04) The highest quality trial found the injection group or the injection plus physiotherapy groups to be superior to the saline injection group or the saline group plus physiotherapy (see Figure 32). (Carette 03) The next highest quality trial suggests injections are more effective than physiotherapy. (Ryans 05) Other studies have found no differences in corticosteroid injections compared with NSAID (Dehghan 13) and hyaluranoic acid. (Park 13) Injections are invasive, have some adverse effects, and are moderate cost. However, they appear quite effective and thus are recommended for treatment of adhesive capsulitis."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acromioclavicular-dislocation",
        "label-name": "Acromioclavicular Dislocation"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the injured worker had ongoing pain of at least 6 to 12 months?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is a distal clavicle resection being considered?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. An injection is recommended prior to consideration of distal clavicle resection for patients with ongoing pain of at least 6 to 12 months to ascertain whether the injection will resolve the pain and, if the pain recurs, whether distal clavicle resection might be successful and should be recommended provided there is no acromioclavicular instability.\n\nInjection Therapy\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with ongoing pain of at least 6 to 12 months and in for whom surgery is considered.\n\nFrequency/Dose/Duration: Dose is unclear as there are no controlled trials evaluating dosage. Simultaneous administration of a local anesthetic with a glucocorticosteroid is recommended to ascertain whether there is immediate relief on injection."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Fadhesive-capsulitis%2Finjections,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Facromioclavicular-sprains-and-dislocations%2Finjections",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "low-intensity-pulsed-ultrasound",
    "label-name": "Low-intensity Pulsed Ultrasound",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "shoulder-disorders",
    "label-name": "Shoulder Disorders"
  },
  "version": {
    "mtus": "12/01/17",
    "odg": "",
    "policy-doc": "10/31/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "type-i-(mid-shaft)-clavicular-fracture",
        "label-name": "Type I (mid-shaft) Clavicular Fracture"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Moderately Not Recommended. Low-intensity pulsed ultrasound is moderately not recommended for treatment of Type I (mid-shaft) clavicular fractures.\n\nStrength of Evidence – Moderately Not Recommended, Evidence (B)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "clavicle-fracture-(non-type-1)",
        "label-name": "Clavicle Fracture (non-Type 1)"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the post-operative use of low-intensity pulsed ultrasound for treatment of all other (non-Type 1) clavicle fractures or non-unions.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: There is one trial comparing non-operative treatments and evidence in evaluating different outcome or complication factors is not uniformly in favor of one treatment approach. Thus, either a simple sling or a figure-of-8 brace is recommended. (Andersen 87) The majority of clavicular fractures are believed not to require surgery. (Khan 09; Kim 08; Jeray 07; Denard 05; Zlowodzki 05; Craig 90; Graves 05; Eiff 97; Miller 92; Quigley 50; Smekal 09; Preston 09) However, a sizeable minority of these fractures may be better treated with surgery which is often recommended for all open fractures, and for many patients with neurovascular compromise, (Barbier 97; Chen 00, 02; Connolly 89; Howard 65; Fujita 01; Miller 69; Kay 86; Bateman 68) multiple trauma, displaced fractures, (Smekal 09) floating shoulders, Type II distal fractures and proximal fractures associated with sternoclavicular dislocations, coracoclavicular ligament disruption, (Chen 02) malunions, and painful non-unions. (Jeray 07; Graves 05; Chen 02; Jones 00; Eiff 97) Treatment for simple displaced fractures is controversial. (Judd 09; Khan 09)\n\nFractures of the lateral end of the clavicle have a high rate of non-union (see Table 9). Type II fractures of the distal third of the clavicle (Neer 68) (coracoclavicular ligaments remain attached to distal fragment and proximal fragment displaced superiorly) are recommended for referral to an orthopedist for consideration of operative treatment due to high rates of non-union. (Anderson Clin Sports Med 03; Eiff 97; Heppenstall 75; Edwards 92; Eskola 87; Post 89; Katznelson 76; Hessman 96; Herscovici 95; Poigenfurst 92; Zenni 81) Fractures of the proximal 1/3 of the clavicle with either significant displacement or sternoclavicular dislocation are recommended for referral to an orthopedist. (Eiff 97) Surgical fixation of mid-shaft fractures most commonly involves intramedullary fixation (Grassi 01; Neviaser 75) and plate fixation. (Graves 05; Hill 97; Kabak 04; Kloen 09; Bradbury 96; Jupiter 87)\n\nThere are a few moderate-quality trials comparing operative treatment with non-operative treatment for mid-shaft fractures. The available evidence shows higher rates of union in those receiving surgery; however, the overall numbers of complications are not improved with surgery. While the quality evidence in favor of lower non-union and malunion rates moderately supports surgical approaches, the total numbers of complications is higher in the operative than non-operative group. Thus, careful consideration must be used with either approach. (Smekal 09; Judd 09; Canadian 07; Altamimi 08)\n\nAn analysis of 2,144 midshaft clavicle fractures found non-unions in 5.9% of non-operatively managed, 5% of plated, and 6% of intramedullary pinned clavicles. Infections occurred in 5.4 % of patients receiving surgery. Fixation failures occurred in 3.1% of the plated and 4.1% of the pinned groups. (Zlowodzki 05) However, these data are not randomized.\n\nOne high-quality RCT of Type I clavicle (diaphyseal) fractures has found no evidence of efficacy of ultrasound to accelerate healing, (Lubbert 08) thus this intervention is not recommended for those fractures. However, favorable results reported for healing disparate nonunion fracture types in uncontrolled studies (Nolte 01) and some evidence for other fractures (Busse 09) does suggest that there may be some role for low-intensity pulsed ultrasound for select clavicle fractures that has not yet been defined but, if successful, may involve more severe fracture types, risks for non-unions, or post-operative settings with risks of non-union."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "non-union",
        "label-name": "Non-union"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the post-operative use of low-intensity pulsed ultrasound for treatment of all other (non-Type 1) clavicle fractures or non-unions.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: There is one trial comparing non-operative treatments and evidence in evaluating different outcome or complication factors is not uniformly in favor of one treatment approach. Thus, either a simple sling or a figure-of-8 brace is recommended. (Andersen 87) The majority of clavicular fractures are believed not to require surgery. (Khan 09; Kim 08; Jeray 07; Denard 05; Zlowodzki 05; Craig 90; Graves 05; Eiff 97; Miller 92; Quigley 50; Smekal 09; Preston 09) However, a sizeable minority of these fractures may be better treated with surgery which is often recommended for all open fractures, and for many patients with neurovascular compromise, (Barbier 97; Chen 00, 02; Connolly 89; Howard 65; Fujita 01; Miller 69; Kay 86; Bateman 68) multiple trauma, displaced fractures, (Smekal 09) floating shoulders, Type II distal fractures and proximal fractures associated with sternoclavicular dislocations, coracoclavicular ligament disruption, (Chen 02) malunions, and painful non-unions. (Jeray 07; Graves 05; Chen 02; Jones 00; Eiff 97) Treatment for simple displaced fractures is controversial. (Judd 09; Khan 09)\n\nFractures of the lateral end of the clavicle have a high rate of non-union (see Table 9). Type II fractures of the distal third of the clavicle (Neer 68) (coracoclavicular ligaments remain attached to distal fragment and proximal fragment displaced superiorly) are recommended for referral to an orthopedist for consideration of operative treatment due to high rates of non-union. (Anderson Clin Sports Med 03; Eiff 97; Heppenstall 75; Edwards 92; Eskola 87; Post 89; Katznelson 76; Hessman 96; Herscovici 95; Poigenfurst 92; Zenni 81) Fractures of the proximal 1/3 of the clavicle with either significant displacement or sternoclavicular dislocation are recommended for referral to an orthopedist. (Eiff 97) Surgical fixation of mid-shaft fractures most commonly involves intramedullary fixation (Grassi 01; Neviaser 75) and plate fixation. (Graves 05; Hill 97; Kabak 04; Kloen 09; Bradbury 96; Jupiter 87)\n\nThere are a few moderate-quality trials comparing operative treatment with non-operative treatment for mid-shaft fractures. The available evidence shows higher rates of union in those receiving surgery; however, the overall numbers of complications are not improved with surgery. While the quality evidence in favor of lower non-union and malunion rates moderately supports surgical approaches, the total numbers of complications is higher in the operative than non-operative group. Thus, careful consideration must be used with either approach. (Smekal 09; Judd 09; Canadian 07; Altamimi 08)\n\nAn analysis of 2,144 midshaft clavicle fractures found non-unions in 5.9% of non-operatively managed, 5% of plated, and 6% of intramedullary pinned clavicles. Infections occurred in 5.4 % of patients receiving surgery. Fixation failures occurred in 3.1% of the plated and 4.1% of the pinned groups. (Zlowodzki 05) However, these data are not randomized.\n\nOne high-quality RCT of Type I clavicle (diaphyseal) fractures has found no evidence of efficacy of ultrasound to accelerate healing, (Lubbert 08) thus this intervention is not recommended for those fractures. However, favorable results reported for healing disparate nonunion fracture types in uncontrolled studies (Nolte 01) and some evidence for other fractures (Busse 09) does suggest that there may be some role for low-intensity pulsed ultrasound for select clavicle fractures that has not yet been defined but, if successful, may involve more severe fracture types, risks for non-unions, or post-operative settings with risks of non-union."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fshoulder-disorders%2Ftreatment-recommendations%2Fshoulder-fractures%2Fclavicular-fractures",
    "odg-link": ""
  }
}
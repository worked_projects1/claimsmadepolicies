{
  "treatment": {
    "index-name": "massage",
    "label-name": "Massage",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "low-back",
    "label-name": "Low Back"
  },
  "version": {
    "mtus": "11/23/21",
    "odg": "",
    "policy-doc": "09/09/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-low-back-pain",
        "label-name": "Subacute Low Back Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a serious pathology such as fracture, tumor, osteoporosis, or infection?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Will this treatment be used as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is the patient compliant with graded increases in activity levels?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended for select use in subacute or chronic low back pain as an adjunct to more efficacious treatments consisting primarily of a graded aerobic and strengthening exercise program.\n\nStrength of Evidence – Recommended, Evidence (C)\n\n Level of Confidence – Low\n\nIndication – For time-limited use in subacute and chronic LBP patients without underlying serious pathology such as fracture, tumor, osteoporosis, or infection as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises. Massage is recommended to assist in increasing the patient’s functional activity levels and comfort more rapidly although the primary treatment focus should remain on the conditioning program. In patients not involved in a conditioning program or who are non-compliant with graded increases in activity levels, this intervention is not recommended.\n\nFrequency/Duration – Six to 10 sessions of 30 to 35 minutes each, 1 or 2 times a week for 4 to 10 weeks. Objective improvements should be shown approximately half way through the regimen to continue this treatment course.\n\nIndications for Discontinuation – Resolution, intolerance, lack of benefit, or non-compliance with aerobic and strengthening exercises.\n\nBenefits – Modest reduction in pain.\n\nHarms – Short term discomfort during massage, and potentially longer term afterwards with more vigorous massage."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-low-back-pain",
        "label-name": "Chronic Low Back Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a serious pathology such as fracture, tumor, osteoporosis, or infection?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Will this treatment be used as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is the patient compliant with graded increases in activity levels?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended for select use in subacute or chronic low back pain as an adjunct to more efficacious treatments consisting primarily of a graded aerobic and strengthening exercise program.\n\nStrength of Evidence – Recommended, Evidence (C)\n\n Level of Confidence – Low\n\nIndication – For time-limited use in subacute and chronic LBP patients without underlying serious pathology such as fracture, tumor, osteoporosis, or infection as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises. Massage is recommended to assist in increasing the patient’s functional activity levels and comfort more rapidly although the primary treatment focus should remain on the conditioning program. In patients not involved in a conditioning program or who are non-compliant with graded increases in activity levels, this intervention is not recommended.\n\nFrequency/Duration – Six to 10 sessions of 30 to 35 minutes each, 1 or 2 times a week for 4 to 10 weeks. Objective improvements should be shown approximately half way through the regimen to continue this treatment course.\n\nIndications for Discontinuation – Resolution, intolerance, lack of benefit, or non-compliance with aerobic and strengthening exercises.\n\nBenefits – Modest reduction in pain.\n\nHarms – Short term discomfort during massage, and potentially longer term afterwards with more vigorous massage."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-low-back-pain",
        "label-name": "Acute Low Back Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient already had NSAIDs/acetaminophen, aerobic exercise, directional exercises, and cold/heat therapy with insufficient results?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is the patient involved in a conditioning program?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is the patient compliant with graded increases in activity levels?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended for select use in acute low back pain or chronic radicular pain syndromes in which low back pain is a substantial symptom component.\n\nIndications – Patients with acute LBP or chronic radicular pain syndromes. For acute LBP, patients should have already had NSAIDs/acetaminophen, aerobic exercise, directional exercises, cold/heat instituted with insufficient results as they typically resolve acute LBP. Massage is recommended as an adjunct to more efficacious treatments to assist in increasing functional activity levels more rapidly although it is recommended that the primary treatment focus remain on the conditioning program. In patients not involved in a conditioning program or who are non-compliant with graded increases in activity levels, this intervention is not recommended.\n\nFrequency/Duration – Objective benefit (functional improvement along with symptom reduction and opioid reduction) should be demonstrated after a trial of 5 sessions in order for further treatment to continue, for up to 10 visits during which a transition to a conditioning program is accomplished.\n\nIndications for Discontinuation – Resolution, intolerance, or lack of benefit.\n\nBenefits – Modest reduction in pain\n\nHarms – Short term discomfort during massage, and potentially longer term afterwards with more vigorous massage.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-radicular-pain-syndromes",
        "label-name": "Chronic Radicular Pain Syndromes"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient already had NSAIDs/acetaminophen, aerobic exercise, directional exercises, and cold/heat therapy with insufficient results?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is the patient involved in a conditioning program?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is the patient compliant with graded increases in activity levels?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended for select use in acute low back pain or chronic radicular pain syndromes in which low back pain is a substantial symptom component.\n\nIndications – Patients with acute LBP or chronic radicular pain syndromes. For acute LBP, patients should have already had NSAIDs/acetaminophen, aerobic exercise, directional exercises, cold/heat instituted with insufficient results as they typically resolve acute LBP. Massage is recommended as an adjunct to more efficacious treatments to assist in increasing functional activity levels more rapidly although it is recommended that the primary treatment focus remain on the conditioning program. In patients not involved in a conditioning program or who are non-compliant with graded increases in activity levels, this intervention is not recommended.\n\nFrequency/Duration – Objective benefit (functional improvement along with symptom reduction and opioid reduction) should be demonstrated after a trial of 5 sessions in order for further treatment to continue, for up to 10 visits during which a transition to a conditioning program is accomplished.\n\nIndications for Discontinuation – Resolution, intolerance, or lack of benefit.\n\nBenefits – Modest reduction in pain\n\nHarms – Short term discomfort during massage, and potentially longer term afterwards with more vigorous massage.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Flow-back-disorders-november-2021%2Frecommendations%2Flow-back-pain-radicular-pain%2Ftreatment-recommendations%2Fallied-health",
    "odg-link": ""
  }
}
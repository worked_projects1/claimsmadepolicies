{
  "treatment": {
    "index-name": "muscle-relaxants",
    "label-name": "Muscle Relaxants",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/08/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-radicular-pain",
        "label-name": "Acute Radicular Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Muscle relaxants are recommended as second- or third-line agents for cases of acute severe radicular pain syndromes or in acute post-surgical patients.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Moderate to severe radicular pain syndromes or post-surgical pain. In radiculopathy pain relief from “muscle relaxants” would presumably be from an analgesic effect and not from a “muscle relaxant” effect, since radicular pain by definition is neuropathic pain and not muscular pain. Generally, muscle relaxants should be prescribed nocturnally initially and not during workdays or when patients plan on operating motor vehicles. However, other agents may be more efficacious for relieving radicular pain, e.g., NSAIDs.\n\n \n\nFrequency/Dose/Duration: Initial dose to be administered in evening. Daytime use is acceptable in circumstances where there are minimal CNS-sedating effects. If significant daytime somnolence interferes with patients work activities, aerobic exercises, or other rehabilitation activities, then the medication may need to be discontinued.\n\nIndications for Discontinuation: Resolution of pain, non-tolerance, significant sedating effects that carry over into the daytime, or other adverse effects.\n\nRationale: Skeletal muscle relaxants have been evaluated in quality studies, although the quality of studies comparing these agents to placebo are likely overstated due to the unblinding that would be inherent in taking a drug with substantial CNS-sedating effects. Nevertheless, there is quality evidence that skeletal muscle relaxants improve acute cervicothoracic pain, particularly for the first 4 to 7 days.(672, 741, 743, 745, 746) However, a concerning adverse event is the significant potential for CNS sedation which has typically affected between 25 to 50% of patients.(744, 745) Thus, it is recommended that the prescription of skeletal muscle relaxants for daytime use be carefully weighed against the need to drive vehicles, operate machinery, or otherwise engage in occupations where mistakes in judgment may have serious consequences. Skeletal muscle relaxants also have a modest, but significant, potential for abuse(747) and caution should be used when prescribing them for patients with a history of substance abuse or dependence.\n\nAlthough the mechanism of action is unclear, skeletal muscle relaxants have demonstrated efficacy in acute cervicothoracic pain,(672, 740, 743, 744) have significant adverse effects, and are low cost, especially if generic medications are prescribed. Thus, skeletal muscle relaxants are recommended for select management of moderate to severe acute cervicothoracic pain. There is little evidence of muscle relaxant efficacy for treatment of chronic cervicothoracic pain. They are not recommended for continuous management of subacute or chronic cervicothoracic pain, although they may be recommended for brief management of acute exacerbations in the setting of chronic cervicothoracic pain. (748)\n\nDiazepam appears inferior to skeletal muscle relaxants, (740, 742) has a higher incidence rate of adverse effects, and is addictive. Diazepam is not recommended for use as a skeletal muscle relaxant. Cyclobenzaprine has advantages of lower abuse potential and some chemical analogy to tricyclic anti-depressants.(749) "
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "mild-to-moderate-acute-cervicothoracic-pain",
        "label-name": "Mild to Moderate Acute Cervicothoracic Pain"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Muscle relaxants are not recommended for mild to moderate acute cervicothoracic pain due to problems with adverse effects.\n\nStrength of Evidence – Not Recommended, Insuffcient Evidence (I)\n\nLevel of Confidence – Moderate\n\nRationale: Skeletal muscle relaxants have been evaluated in quality studies, although the quality of studies comparing these agents to placebo are likely overstated due to the unblinding that would be inherent in taking a drug with substantial CNS-sedating effects. Nevertheless, there is quality evidence that skeletal muscle relaxants improve acute cervicothoracic pain, particularly for the first 4 to 7 days.(672, 741, 743, 745, 746) However, a concerning adverse event is the significant potential for CNS sedation which has typically affected between 25 to 50% of patients.(744, 745) Thus, it is recommended that the prescription of skeletal muscle relaxants for daytime use be carefully weighed against the need to drive vehicles, operate machinery, or otherwise engage in occupations where mistakes in judgment may have serious consequences. Skeletal muscle relaxants also have a modest, but significant, potential for abuse(747) and caution should be used when prescribing them for patients with a history of substance abuse or dependence.\n\nAlthough the mechanism of action is unclear, skeletal muscle relaxants have demonstrated efficacy in acute cervicothoracic pain,(672, 740, 743, 744) have significant adverse effects, and are low cost, especially if generic medications are prescribed. Thus, skeletal muscle relaxants are recommended for select management of moderate to severe acute cervicothoracic pain. There is little evidence of muscle relaxant efficacy for treatment of chronic cervicothoracic pain. They are not recommended for continuous management of subacute or chronic cervicothoracic pain, although they may be recommended for brief management of acute exacerbations in the setting of chronic cervicothoracic pain. (748)\n\nDiazepam appears inferior to skeletal muscle relaxants, (740, 742) has a higher incidence rate of adverse effects, and is addictive. Diazepam is not recommended for use as a skeletal muscle relaxant. Cyclobenzaprine has advantages of lower abuse potential and some chemical analogy to tricyclic anti-depressants.(749)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "moderate-to-severe-acute-cervicothoracic-pain",
        "label-name": "Moderate to Severe Acute Cervicothoracic Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is this treatment being used as a second-line treatment in cases of moderate to severe acute cervicothoracic pain that has not been adequately controlled by NSAIDs?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Muscle relaxants are recommended as a second-line treatment in cases of moderate to severe acute cervicothoracic pain that has not been adequately controlled by NSAIDs.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Moderate\n\nIndications: Moderate to severe acute cervicothoracic pain; best in patients with clinically palpable muscle spasm, limited ROM, limitation of activities of daily living, and tenderness on palpation with symptoms less than 14 days.(672, 740-743) Caution should be used in prescribing skeletal muscle relaxants for those with a history of depression, personality disorder, and/or substance addiction/abuse (including alcohol or tobacco) as most of RCTs exclude participants with these co-morbidities.(672, 742-744)\n\n \n\nBenefits: Modest reduction in acute cervicothoracic pain compared with placebo.\n\nHarms: Sedation, daytime fatigue. Modest potential for abuse. Risk for safety including motor vehicle crash and other injuries.\n\nFrequency/Dose/Duration: Initial dose recommended nocturnally and not during workdays or when patients plan to operate motor vehicles. Daytime use is acceptable in circumstances where there are minimal CNS-sedating effects and little concern about sedation compromising function or safety. If significant daytime somnolence results, the medication may need to be discontinued, particularly if it interferes with performance of work, aerobic exercises, or other components of the rehabilitation plan. It is not recommended that the first dose be taken prior to starting a work shift or operating a motor vehicle or machinery. No significant improvement reported in symptoms between the 5mg and 10mg doses of cyclobenzaprine, but found increased somnolence with 10mg dose; patients taking 10mg dose had the highest incidence of premature discontinuation due to adverse effects.(744) If a muscle relaxant is felt to be necessary in patients with psychological issues noted above, cyclobenzaprine is recommend, as its chemical structure resembles a tricyclic anti-depressant, and addiction and abuse are less likely.\n\nIndications for Discontinuation: Resolution of pain, non-tolerance, significant sedating effects that carry over into the daytime, or other adverse effects.\n\nRationale: Skeletal muscle relaxants have been evaluated in quality studies, although the quality of studies comparing these agents to placebo are likely overstated due to the unblinding that would be inherent in taking a drug with substantial CNS-sedating effects. Nevertheless, there is quality evidence that skeletal muscle relaxants improve acute cervicothoracic pain, particularly for the first 4 to 7 days.(672, 741, 743, 745, 746) However, a concerning adverse event is the significant potential for CNS sedation which has typically affected between 25 to 50% of patients.(744, 745) Thus, it is recommended that the prescription of skeletal muscle relaxants for daytime use be carefully weighed against the need to drive vehicles, operate machinery, or otherwise engage in occupations where mistakes in judgment may have serious consequences. Skeletal muscle relaxants also have a modest, but significant, potential for abuse(747) and caution should be used when prescribing them for patients with a history of substance abuse or dependence.\n\nAlthough the mechanism of action is unclear, skeletal muscle relaxants have demonstrated efficacy in acute cervicothoracic pain,(672, 740, 743, 744) have significant adverse effects, and are low cost, especially if generic medications are prescribed. Thus, skeletal muscle relaxants are recommended for select management of moderate to severe acute cervicothoracic pain. There is little evidence of muscle relaxant efficacy for treatment of chronic cervicothoracic pain. They are not recommended for continuous management of subacute or chronic cervicothoracic pain, although they may be recommended for brief management of acute exacerbations in the setting of chronic cervicothoracic pain. (748)\n\nDiazepam appears inferior to skeletal muscle relaxants, (740, 742) has a higher incidence rate of adverse effects, and is addictive. Diazepam is not recommended for use as a skeletal muscle relaxant. Cyclobenzaprine has advantages of lower abuse potential and some chemical analogy to tricyclic anti-depressants.(749)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-subacute",
        "label-name": "Cervicothoracic Pain - Subacute"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Muscle relaxants are not recommended for subacute or chronic cervicothoracic pain as there is no evidence to support their use. Additionally, there are relatively high adverse effect profiles and possible abuse potential.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: Skeletal muscle relaxants have been evaluated in quality studies, although the quality of studies comparing these agents to placebo are likely overstated due to the unblinding that would be inherent in taking a drug with substantial CNS-sedating effects. Nevertheless, there is quality evidence that skeletal muscle relaxants improve acute cervicothoracic pain, particularly for the first 4 to 7 days.(672, 741, 743, 745, 746) However, a concerning adverse event is the significant potential for CNS sedation which has typically affected between 25 to 50% of patients.(744, 745) Thus, it is recommended that the prescription of skeletal muscle relaxants for daytime use be carefully weighed against the need to drive vehicles, operate machinery, or otherwise engage in occupations where mistakes in judgment may have serious consequences. Skeletal muscle relaxants also have a modest, but significant, potential for abuse(747) and caution should be used when prescribing them for patients with a history of substance abuse or dependence.\n\nAlthough the mechanism of action is unclear, skeletal muscle relaxants have demonstrated efficacy in acute cervicothoracic pain,(672, 740, 743, 744) have significant adverse effects, and are low cost, especially if generic medications are prescribed. Thus, skeletal muscle relaxants are recommended for select management of moderate to severe acute cervicothoracic pain. There is little evidence of muscle relaxant efficacy for treatment of chronic cervicothoracic pain. They are not recommended for continuous management of subacute or chronic cervicothoracic pain, although they may be recommended for brief management of acute exacerbations in the setting of chronic cervicothoracic pain. (748)\n\nDiazepam appears inferior to skeletal muscle relaxants, (740, 742) has a higher incidence rate of adverse effects, and is addictive. Diazepam is not recommended for use as a skeletal muscle relaxant. Cyclobenzaprine has advantages of lower abuse potential and some chemical analogy to tricyclic anti-depressants.(749)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-chronic",
        "label-name": "Cervicothoracic Pain-Chronic"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Muscle relaxants are not recommended for subacute or chronic cervicothoracic pain as there is no evidence to support their use. Additionally, there are relatively high adverse effect profiles and possible abuse potential.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: Skeletal muscle relaxants have been evaluated in quality studies, although the quality of studies comparing these agents to placebo are likely overstated due to the unblinding that would be inherent in taking a drug with substantial CNS-sedating effects. Nevertheless, there is quality evidence that skeletal muscle relaxants improve acute cervicothoracic pain, particularly for the first 4 to 7 days.(672, 741, 743, 745, 746) However, a concerning adverse event is the significant potential for CNS sedation which has typically affected between 25 to 50% of patients.(744, 745) Thus, it is recommended that the prescription of skeletal muscle relaxants for daytime use be carefully weighed against the need to drive vehicles, operate machinery, or otherwise engage in occupations where mistakes in judgment may have serious consequences. Skeletal muscle relaxants also have a modest, but significant, potential for abuse(747) and caution should be used when prescribing them for patients with a history of substance abuse or dependence.\n\nAlthough the mechanism of action is unclear, skeletal muscle relaxants have demonstrated efficacy in acute cervicothoracic pain,(672, 740, 743, 744) have significant adverse effects, and are low cost, especially if generic medications are prescribed. Thus, skeletal muscle relaxants are recommended for select management of moderate to severe acute cervicothoracic pain. There is little evidence of muscle relaxant efficacy for treatment of chronic cervicothoracic pain. They are not recommended for continuous management of subacute or chronic cervicothoracic pain, although they may be recommended for brief management of acute exacerbations in the setting of chronic cervicothoracic pain. (748)\n\nDiazepam appears inferior to skeletal muscle relaxants, (740, 742) has a higher incidence rate of adverse effects, and is addictive. Diazepam is not recommended for use as a skeletal muscle relaxant. Cyclobenzaprine has advantages of lower abuse potential and some chemical analogy to tricyclic anti-depressants.(749)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fradicular-pain%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fcervicothoracic-pain%2Ftreatment-recommendations%2Fmedications",
    "odg-link": ""
  }
}
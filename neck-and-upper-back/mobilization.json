{
  "treatment": {
    "index-name": "mobilization",
    "label-name": "Mobilization",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/07/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-acute",
        "label-name": "Cervicothoracic Pain - Acute"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is this treatment for short-term relief of cervical pain? ",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is this treatment a component of an active treatment program focusing on active exercises for acute cervicothoracic pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Manipulation/mobilization of the cervical and/or thoracic spine is recommended for short-term relief of cervical pain or as a component of an active treatment program focusing on active exercises for acute cervicothoracic pain.  However, high amplitude, high velocity manipulation is not recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nBenefits: Potential for faster resolution of pain and improved function.\n\nHarms: Worsening of neck pain, especially immediately after manipulation.\n\nFrequency/Dose/Duration:  Dependent on severity. Most patients with more severe spine conditions may receive up to 12 visits over 6 to 8 weeks, typically one to 3 times a week;(958-960) total treatments dependent on response to therapy. Substantial progression (e.g., return to work or activities, increasing ability to tolerate exercise, reduced medication use) should be documented at each follow-up visit. Treatment plan should be reassessed after each 2-week interval. Most guidelines suggest that if there is significant response in the above outcomes, it is worth considering another 2 weeks of treatment. If no response to 2 weeks of application of a particular manipulation treatment, it should be discontinued and 2 weeks of a different method of manipulation/mobilization or other treatment should be considered. If there is no response after 4 weeks and two 2-week trials of different manipulation/mobilization techniques, it is unlikely that further manipulation/mobilization will be helpful.\n\nIndications for Discontinuation: Lack of demonstrated continued functional response after 6 manipulation/mobilization sessions (2 trials of 2 or more different methods), resolution of symptoms, or failure to participate in an active rehabilitation program.\n\nRationale: Multiple studies evaluate thoracic and cervical spine manipulation, (537, 932) whereas other studies evaluated one or the other.(949, 959, 961-964) Other studies do not delineate between the two different types of therapies.(578, 579, 675, 679, 965, 966)\n\nThere are no quality trials comparing mobilization to sham or placebo for treatment of acute cervical pain. The closest study appears to be that of Cleland et al (2007), but it was impaired by methodological limitations. Most studies compare mobilization to manipulation, or use mobilization as a component of other interventions, significantly weakening the ability to infer efficacy of manipulation.(581) Most studies had small samples sizes with most <70.(959, 960, 967, 968) A moderate-quality trial evaluating mobilization suggested greater benefit compared with directed exercise and continued care by a general practitioner. However, this study included acute, subacute, and chronic pain without delineation between duration in the results, and the general practitioner care appeared to fail to include treatments thought to be efficacious.(565) A moderate-quality trial comparing cervical manipulation to mobilization suggested improvement in pain and range of motion in both groups after a single treatment, but manipulation was reportedly associated with overall better pain improvement on the NRS-101 and larger gains in range of motion.(6) Thus, the available quality evidence conflicts on treatment of cervicothoracic pain.(969) Hoving suggested mobilization is a favorable treatment option for patients with cervical pain compared with directed exercise or continued care by a general practitioner, although the general medical care may have been suboptimal.(565)\n\nThere are no sham-controlled trials of manipulation. Only a few RCTs evaluated subacute cervicothoracic pain and did so in combination with chronic cervicothoracic pain without reporting findings based on duration of symptoms. (960) A moderate-quality study comparing a single episode of cervical manipulation versus mobilization in subacute and chronic patients reported manipulation to have greater improvement in cervicothoracic pain at rest and active range of motion.(961) A moderate-quality study that did not describe well the duration of symptoms found an increase in range of motion after a single thoracic spine manipulation compared to no intervention.(970) (Krauss 08) Where another study compared manipulation and exercises alone and in combination and reported no significant clinical differences at 12-month follow up in chronic pain patients.(537)\n\nA moderate-quality study of patients with chronic pain examined manipulation, manipulation and exercise and an exercise only group. They found that the manipulation alone group had less improvement compared to manipulation with exercise and exercises alone at 16 months after 11 weeks of treatment.(537) One study of 119 patients with cervicothoracic pain greater than 3 months duration reported improvement in all groups, but did not find any difference in the manipulation group when compared to physiotherapy and intensive training of cervical musculature for 6 weeks.(548) A moderate-quality study suggested acupuncture was more effective than manipulation or medications in treating chronic cervical pain.(675) Another moderate-quality study compared manipulation with sham ultrasound to sham ultrasound alone and suggested an improvement in pain in the manipulation group at 12 weeks.(971) While the RCTs show that other interventions are equally beneficial, the manipulation groups also experienced significant improvement in pain control and range of motion. Manipulation in subacute and chronic cervicothoracic pain is recommended and is best utilized in combination with an active exercise program.(537, 972) It was not possible to determine which technique was beneficial for which patient populations. There was also insufficient evidence for cervicothoracic pain with radicular findings.\n\nA study evaluated a Clinical Prediction Rule for cervicothoracic pain using thoracic manipulation that is somewhat analogous to those for the lumbar spine (see Low Back Disorders guideline). They reported predictors for increasing the likelihood of a positive outcome with thoracic manipulation.(973, 974) These 6 variables were symptoms <30 days, no symptoms distal to the shoulder, neck extension does not aggravate pain, FABQPA score <12, diminished upper thoracic spine kyphosis, and cervical extension ROM <30 degrees. Once this information has been reproduced and validated there may be a group of patients identified where thoracic manipulation may be recommended with greater specificity. However, a recent RCT reported that the above CPR was not able to be validated.(975) Another group assessed a clinical prediction rule and noted better response to treatment if: initial Neck Disability Index <11.5, bilateral involvement pattern, no sedentary work >5 hours a day, feeling better while moving the neck, not worse while extending the neck, and a diagnosis of spondylosis without radiculopathy.(976)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-subacute",
        "label-name": "Cervicothoracic Pain - Subacute"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is this treatment for short-term relief of cervical pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is this treatment a component of an active treatment program focusing on active exercises for acute cervicothoracic pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Manipulation/mobilization of the cervical and/or thoracic spine is recommended for short-term relief of cervical pain or as a component of an active treatment program focusing on active exercises for acute cervicothoracic pain.  However, high amplitude, high velocity manipulation is not recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nBenefits: Potential for faster resolution of pain and improved function.\n\nHarms: Worsening of neck pain, especially immediately after manipulation.\n\nFrequency/Dose/Duration:  Dependent on severity. Most patients with more severe spine conditions may receive up to 12 visits over 6 to 8 weeks, typically one to 3 times a week;(958-960) total treatments dependent on response to therapy. Substantial progression (e.g., return to work or activities, increasing ability to tolerate exercise, reduced medication use) should be documented at each follow-up visit. Treatment plan should be reassessed after each 2-week interval. Most guidelines suggest that if there is significant response in the above outcomes, it is worth considering another 2 weeks of treatment. If no response to 2 weeks of application of a particular manipulation treatment, it should be discontinued and 2 weeks of a different method of manipulation/mobilization or other treatment should be considered. If there is no response after 4 weeks and two 2-week trials of different manipulation/mobilization techniques, it is unlikely that further manipulation/mobilization will be helpful.\n\nIndications for Discontinuation: Lack of demonstrated continued functional response after 6 manipulation/mobilization sessions (2 trials of 2 or more different methods), resolution of symptoms, or failure to participate in an active rehabilitation program.\n\nRationale: Multiple studies evaluate thoracic and cervical spine manipulation, (537, 932) whereas other studies evaluated one or the other.(949, 959, 961-964) Other studies do not delineate between the two different types of therapies.(578, 579, 675, 679, 965, 966)\n\nThere are no quality trials comparing mobilization to sham or placebo for treatment of acute cervical pain. The closest study appears to be that of Cleland et al (2007), but it was impaired by methodological limitations. Most studies compare mobilization to manipulation, or use mobilization as a component of other interventions, significantly weakening the ability to infer efficacy of manipulation.(581) Most studies had small samples sizes with most <70.(959, 960, 967, 968) A moderate-quality trial evaluating mobilization suggested greater benefit compared with directed exercise and continued care by a general practitioner. However, this study included acute, subacute, and chronic pain without delineation between duration in the results, and the general practitioner care appeared to fail to include treatments thought to be efficacious.(565) A moderate-quality trial comparing cervical manipulation to mobilization suggested improvement in pain and range of motion in both groups after a single treatment, but manipulation was reportedly associated with overall better pain improvement on the NRS-101 and larger gains in range of motion.(6) Thus, the available quality evidence conflicts on treatment of cervicothoracic pain.(969) Hoving suggested mobilization is a favorable treatment option for patients with cervical pain compared with directed exercise or continued care by a general practitioner, although the general medical care may have been suboptimal.(565)\n\nThere are no sham-controlled trials of manipulation. Only a few RCTs evaluated subacute cervicothoracic pain and did so in combination with chronic cervicothoracic pain without reporting findings based on duration of symptoms. (960) A moderate-quality study comparing a single episode of cervical manipulation versus mobilization in subacute and chronic patients reported manipulation to have greater improvement in cervicothoracic pain at rest and active range of motion.(961) A moderate-quality study that did not describe well the duration of symptoms found an increase in range of motion after a single thoracic spine manipulation compared to no intervention.(970) (Krauss 08) Where another study compared manipulation and exercises alone and in combination and reported no significant clinical differences at 12-month follow up in chronic pain patients.(537)\n\nA moderate-quality study of patients with chronic pain examined manipulation, manipulation and exercise and an exercise only group. They found that the manipulation alone group had less improvement compared to manipulation with exercise and exercises alone at 16 months after 11 weeks of treatment.(537) One study of 119 patients with cervicothoracic pain greater than 3 months duration reported improvement in all groups, but did not find any difference in the manipulation group when compared to physiotherapy and intensive training of cervical musculature for 6 weeks.(548) A moderate-quality study suggested acupuncture was more effective than manipulation or medications in treating chronic cervical pain.(675) Another moderate-quality study compared manipulation with sham ultrasound to sham ultrasound alone and suggested an improvement in pain in the manipulation group at 12 weeks.(971) While the RCTs show that other interventions are equally beneficial, the manipulation groups also experienced significant improvement in pain control and range of motion. Manipulation in subacute and chronic cervicothoracic pain is recommended and is best utilized in combination with an active exercise program.(537, 972) It was not possible to determine which technique was beneficial for which patient populations. There was also insufficient evidence for cervicothoracic pain with radicular findings.\n\nA study evaluated a Clinical Prediction Rule for cervicothoracic pain using thoracic manipulation that is somewhat analogous to those for the lumbar spine (see Low Back Disorders guideline). They reported predictors for increasing the likelihood of a positive outcome with thoracic manipulation.(973, 974) These 6 variables were symptoms <30 days, no symptoms distal to the shoulder, neck extension does not aggravate pain, FABQPA score <12, diminished upper thoracic spine kyphosis, and cervical extension ROM <30 degrees. Once this information has been reproduced and validated there may be a group of patients identified where thoracic manipulation may be recommended with greater specificity. However, a recent RCT reported that the above CPR was not able to be validated.(975) Another group assessed a clinical prediction rule and noted better response to treatment if: initial Neck Disability Index <11.5, bilateral involvement pattern, no sedentary work >5 hours a day, feeling better while moving the neck, not worse while extending the neck, and a diagnosis of spondylosis without radiculopathy.(976)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-chronic",
        "label-name": "Cervicothoracic Pain - Chronic"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is this treatment for short-term relief of cervical pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is this treatment a component of an active treatment program focusing on active exercises for acute cervicothoracic pain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Manipulation/mobilization of the cervical and/or thoracic spine is recommended for short-term relief of cervical pain or as a component of an active treatment program focusing on active exercises for acute cervicothoracic pain.  However, high amplitude, high velocity manipulation is not recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nBenefits: Potential for faster resolution of pain and improved function.\n\nHarms: Worsening of neck pain, especially immediately after manipulation.\n\nFrequency/Dose/Duration:  Dependent on severity. Most patients with more severe spine conditions may receive up to 12 visits over 6 to 8 weeks, typically one to 3 times a week;(958-960) total treatments dependent on response to therapy. Substantial progression (e.g., return to work or activities, increasing ability to tolerate exercise, reduced medication use) should be documented at each follow-up visit. Treatment plan should be reassessed after each 2-week interval. Most guidelines suggest that if there is significant response in the above outcomes, it is worth considering another 2 weeks of treatment. If no response to 2 weeks of application of a particular manipulation treatment, it should be discontinued and 2 weeks of a different method of manipulation/mobilization or other treatment should be considered. If there is no response after 4 weeks and two 2-week trials of different manipulation/mobilization techniques, it is unlikely that further manipulation/mobilization will be helpful.\n\nIndications for Discontinuation: Lack of demonstrated continued functional response after 6 manipulation/mobilization sessions (2 trials of 2 or more different methods), resolution of symptoms, or failure to participate in an active rehabilitation program.\n\nRationale: Multiple studies evaluate thoracic and cervical spine manipulation, (537, 932) whereas other studies evaluated one or the other.(949, 959, 961-964) Other studies do not delineate between the two different types of therapies.(578, 579, 675, 679, 965, 966)\n\nThere are no quality trials comparing mobilization to sham or placebo for treatment of acute cervical pain. The closest study appears to be that of Cleland et al (2007), but it was impaired by methodological limitations. Most studies compare mobilization to manipulation, or use mobilization as a component of other interventions, significantly weakening the ability to infer efficacy of manipulation.(581) Most studies had small samples sizes with most <70.(959, 960, 967, 968) A moderate-quality trial evaluating mobilization suggested greater benefit compared with directed exercise and continued care by a general practitioner. However, this study included acute, subacute, and chronic pain without delineation between duration in the results, and the general practitioner care appeared to fail to include treatments thought to be efficacious.(565) A moderate-quality trial comparing cervical manipulation to mobilization suggested improvement in pain and range of motion in both groups after a single treatment, but manipulation was reportedly associated with overall better pain improvement on the NRS-101 and larger gains in range of motion.(6) Thus, the available quality evidence conflicts on treatment of cervicothoracic pain.(969) Hoving suggested mobilization is a favorable treatment option for patients with cervical pain compared with directed exercise or continued care by a general practitioner, although the general medical care may have been suboptimal.(565)\n\nThere are no sham-controlled trials of manipulation. Only a few RCTs evaluated subacute cervicothoracic pain and did so in combination with chronic cervicothoracic pain without reporting findings based on duration of symptoms. (960) A moderate-quality study comparing a single episode of cervical manipulation versus mobilization in subacute and chronic patients reported manipulation to have greater improvement in cervicothoracic pain at rest and active range of motion.(961) A moderate-quality study that did not describe well the duration of symptoms found an increase in range of motion after a single thoracic spine manipulation compared to no intervention.(970) (Krauss 08) Where another study compared manipulation and exercises alone and in combination and reported no significant clinical differences at 12-month follow up in chronic pain patients.(537)\n\nA moderate-quality study of patients with chronic pain examined manipulation, manipulation and exercise and an exercise only group. They found that the manipulation alone group had less improvement compared to manipulation with exercise and exercises alone at 16 months after 11 weeks of treatment.(537) One study of 119 patients with cervicothoracic pain greater than 3 months duration reported improvement in all groups, but did not find any difference in the manipulation group when compared to physiotherapy and intensive training of cervical musculature for 6 weeks.(548) A moderate-quality study suggested acupuncture was more effective than manipulation or medications in treating chronic cervical pain.(675) Another moderate-quality study compared manipulation with sham ultrasound to sham ultrasound alone and suggested an improvement in pain in the manipulation group at 12 weeks.(971) While the RCTs show that other interventions are equally beneficial, the manipulation groups also experienced significant improvement in pain control and range of motion. Manipulation in subacute and chronic cervicothoracic pain is recommended and is best utilized in combination with an active exercise program.(537, 972) It was not possible to determine which technique was beneficial for which patient populations. There was also insufficient evidence for cervicothoracic pain with radicular findings.\n\nA study evaluated a Clinical Prediction Rule for cervicothoracic pain using thoracic manipulation that is somewhat analogous to those for the lumbar spine (see Low Back Disorders guideline). They reported predictors for increasing the likelihood of a positive outcome with thoracic manipulation.(973, 974) These 6 variables were symptoms <30 days, no symptoms distal to the shoulder, neck extension does not aggravate pain, FABQPA score <12, diminished upper thoracic spine kyphosis, and cervical extension ROM <30 degrees. Once this information has been reproduced and validated there may be a group of patients identified where thoracic manipulation may be recommended with greater specificity. However, a recent RCT reported that the above CPR was not able to be validated.(975) Another group assessed a clinical prediction rule and noted better response to treatment if: initial Neck Disability Index <11.5, bilateral involvement pattern, no sedentary work >5 hours a day, feeling better while moving the neck, not worse while extending the neck, and a diagnosis of spondylosis without radiculopathy.(976)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fcervicothoracic-pain%2Ftreatment-recommendations%2Fallied-health",
    "odg-link": ""
  }
}
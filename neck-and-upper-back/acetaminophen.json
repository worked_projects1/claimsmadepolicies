{
  "treatment": {
    "index-name": "acetaminophen",
    "label-name": "Acetaminophen",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/08/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-with-radicular-symptoms",
        "label-name": "Cervicothoracic Pain with Radicular Symptoms"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Acetaminophen is recommended for treatment of cervicothoracic pain with or without radicular symptoms, particularly for those with contraindications for NSAIDs.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nBenefits: Addresses spine pain without increased risk of cardiovascular event.\n\nHarms: Less effective than NSAID. Aspirin also more prone towards gastrointestinal bleeding and other hemorrhage.\n\nRationale: There is less quality evidence for use of NSAIDs and acetaminophen in cervicothoracic pain compared to low back pain and arthroses (see Low Back Disorders and Hip and Groin Disorders guidelines). A review found only 5 RCTs with a total of 270 people.(670) There are no randomized placebo controlled trials evaluating NSAIDs and cervicothoracic pain. There is evidence that NSAIDs decrease pain in lumbosacral spine pain (see Low Back Disorders guideline) as well as other joint pain.\n\nThere is quality evidence that NSAIDs reduce pain and improve functional status among acute, subacute, and chronic cervicothoracic pain patients.(671-674) These RCTs compared NSAIDs to other interventions such as manipulation in acute and subacute cervicothoracic pain,(675, 676) acupuncture(675, 677) and documented improvement with NSAIDs, but did not find a statistically significant improvement compared to the other interventions. Less clear, primarily due to in part to diagnostic uncertainties, are the beneficial effects that appear to be present for the treatment of radicular pain syndromes.(678)\n\nResults are positive whether considering COX-1 (non-selective) or COX-2 (selective) NSAIDs,(673, 675, 679) although the magnitude of benefit is not generally large for any given medication. There is a dearth of head-to-head comparative trials of NSAIDs. Evidence that one medication is superior to another is lacking for cervicothoracic pain. There also is no strong evidence that any specific dosing pattern is superior.\n\nThere are no quality studies of acetaminophen as a single agent in the adult working population. There is one moderate-quality RCT evaluating single dose acetaminophen compared to ibuprofen and codeine in ages 6 to 17 in acute musculoskeletal pain, showing ibuprofen to have more significant pain relief.(674) However, paracetamol, a close analog, has been studied more extensively in subacute/chronic cervicothoracic pain and has some evidence of efficacy.(673, 675) There has not been any evidence that paracetamol is superior or equivalent to NSAIDs.(673)\n\nNSAIDs are not invasive, have low side effect profiles in a healthy working age patient population, and when generic medications are used are low cost. The potential for some NSAIDs to increase the risk of cardiovascular events should be considered and requires additional quality studies to fully address. A recent review should be consulted before prescribing for high cardiovascular risk individuals.(669)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-without-radicular-symptoms",
        "label-name": "Cervicothoracic Pain without Radicular Symptoms"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Acetaminophen is recommended for treatment of cervicothoracic pain with or without radicular symptoms, particularly for those with contraindications for NSAIDs.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nBenefits: Addresses spine pain without increased risk of cardiovascular event.\n\nHarms: Less effective than NSAID. Aspirin also more prone towards gastrointestinal bleeding and other hemorrhage.\n\nRationale: There is less quality evidence for use of NSAIDs and acetaminophen in cervicothoracic pain compared to low back pain and arthroses (see Low Back Disorders and Hip and Groin Disorders guidelines). A review found only 5 RCTs with a total of 270 people.(670) There are no randomized placebo controlled trials evaluating NSAIDs and cervicothoracic pain. There is evidence that NSAIDs decrease pain in lumbosacral spine pain (see Low Back Disorders guideline) as well as other joint pain.\n\nThere is quality evidence that NSAIDs reduce pain and improve functional status among acute, subacute, and chronic cervicothoracic pain patients.(671-674) These RCTs compared NSAIDs to other interventions such as manipulation in acute and subacute cervicothoracic pain,(675, 676) acupuncture(675, 677) and documented improvement with NSAIDs, but did not find a statistically significant improvement compared to the other interventions. Less clear, primarily due to in part to diagnostic uncertainties, are the beneficial effects that appear to be present for the treatment of radicular pain syndromes.(678)\n\nResults are positive whether considering COX-1 (non-selective) or COX-2 (selective) NSAIDs,(673, 675, 679) although the magnitude of benefit is not generally large for any given medication. There is a dearth of head-to-head comparative trials of NSAIDs. Evidence that one medication is superior to another is lacking for cervicothoracic pain. There also is no strong evidence that any specific dosing pattern is superior.\n\nThere are no quality studies of acetaminophen as a single agent in the adult working population. There is one moderate-quality RCT evaluating single dose acetaminophen compared to ibuprofen and codeine in ages 6 to 17 in acute musculoskeletal pain, showing ibuprofen to have more significant pain relief.(674) However, paracetamol, a close analog, has been studied more extensively in subacute/chronic cervicothoracic pain and has some evidence of efficacy.(673, 675) There has not been any evidence that paracetamol is superior or equivalent to NSAIDs.(673)\n\nNSAIDs are not invasive, have low side effect profiles in a healthy working age patient population, and when generic medications are used are low cost. The potential for some NSAIDs to increase the risk of cardiovascular events should be considered and requires additional quality studies to fully address. A recent review should be consulted before prescribing for high cardiovascular risk individuals.(669)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fcervicothoracic-pain%2Ftreatment-recommendations%2Fmedications",
    "odg-link": ""
  }
}
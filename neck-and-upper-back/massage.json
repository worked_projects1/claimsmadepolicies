{
  "treatment": {
    "index-name": "massage",
    "label-name": "Massage",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/07/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-acute",
        "label-name": "Cervicothoracic Pain - Acute"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is cervicothoracic pain a substantial symptom component?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended as a treatment for acute and subacute cervicothoracic pain and chronic radicular syndromes in which cervicothoracic pain is a substantial symptom component.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Patients with subacute and chronic cervicothoracic pain without underlying serious pathology, such as fracture, tumor, or infection.               \n\nFrequency/Dose/Duration: Objective benefit (functional improvement along with symptom reduction) may be demonstrated after a trial of 2 sessions in order for further treatment to continue, for up to 10 sessions during which a transition to a conditioning program is accomplished.\n\nIndications for Discontinuation: Resolution, intolerance, or lack of benefit.\n"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-subacute",
        "label-name": "Cervicothoracic Pain - Subacute"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is cervicothoracic pain a substantial symptom component?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the patient have a serious underlying pathology such as fracture, tumor, or infection?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "approve"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended as a treatment for acute and subacute cervicothoracic pain and chronic radicular syndromes in which cervicothoracic pain is a substantial symptom component.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Patients with subacute and chronic cervicothoracic pain without underlying serious pathology, such as fracture, tumor, or infection.               \n\nFrequency/Dose/Duration: Objective benefit (functional improvement along with symptom reduction) may be demonstrated after a trial of 2 sessions in order for further treatment to continue, for up to 10 sessions during which a transition to a conditioning program is accomplished.\n\nIndications for Discontinuation: Resolution, intolerance, or lack of benefit."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-radicular-syndromes",
        "label-name": "Chronic Radicular Syndromes"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is cervicothoracic pain a substantial symptom component?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the patient have a serious underlying pathology such as fracture, tumor, or infection?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "approve"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended as a treatment for acute and subacute cervicothoracic pain and chronic radicular syndromes in which cervicothoracic pain is a substantial symptom component.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Patients with subacute and chronic cervicothoracic pain without underlying serious pathology, such as fracture, tumor, or infection.               \n\nFrequency/Dose/Duration: Objective benefit (functional improvement along with symptom reduction) may be demonstrated after a trial of 2 sessions in order for further treatment to continue, for up to 10 sessions during which a transition to a conditioning program is accomplished.\n\nIndications for Discontinuation: Resolution, intolerance, or lack of benefit.\n"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-chronic",
        "label-name": "Cervicothoracic Pain - Chronic"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a serious underlying pathology such as fracture, tumor, or infection?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Is this treatment for time-limited use?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is the patient involved in a conditioning program?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "4",
          "question": "Is the patient compliant with graded increases in activity level?",
          "option-list": [
            {
              "option": "yes",
              "action": "5"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "5",
          "question": "Is this treatment being used as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Massage is recommended for select use in chronic cervicothoracic pain as an adjunct to more efficacious treatments consisting primarily of a graded aerobic and strengthening exercise program.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications:  For time-limited use in chronic cervicothoracic pain patients without underlying serious pathology as an adjunct to a conditioning program that has both graded aerobic exercise and strengthening exercises. The intervention is only recommended to assist in increasing functional activity levels more rapidly and the primary attention should remain on the conditioning program. In patients not involved in a conditioning program or who are non-compliant with graded increases in activity levels, this intervention is not recommended.\n\nBenefits: Modest reduction in pain.\n\nHarms: Short term discomfort during massage, and potentially longer term afterwards with more vigorous massage.\n\nFrequency/Dose/Duration: Six to 10 sessions of 30 to 35 minutes each, 1 or 2 times a week for 4 to 6 weeks.(1061) Objective improvements should be shown approximately half way through the regimen to continue this treatment course.\n\nIndications for Discontinuation: Resolution, intolerance, lack of benefit, or non-compliance with aerobic and strengthening exercises."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fcervicothoracic-pain%2Ftreatment-recommendations%2Fallied-health",
    "odg-link": ""
  }
}
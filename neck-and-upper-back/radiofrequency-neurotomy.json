{
  "treatment": {
    "index-name": "radiofrequency-neurotomy",
    "label-name": "Radiofrequency Neurotomy",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/08/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicogenic-headache",
        "label-name": "Cervicogenic Headache"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Moderately Not Recommended. Radiofrequency neurotomy is moderately not recommended for the treatment of cervicogenic headache.\n\nStrength of Evidence – Moderately Not Recommended, Evidence (B)\n\nLevel of Confidence – Low \n\nRationale: A moderate-quality, sham controlled trial evaluating patients with cervical zygapophyseal-joint pain diagnosed with anesthetic blocks, but without any radicular symptoms, showed improvement in pain over a sham procedure at 12 months. However, there were statistically more patients in the sham group involved in litigation over the accident that caused their pain (p = 0.04) than in the intervention group.(1184) Thus, even though the study’s methodology scores were good, it has a potential fatal flaw or bias. Another moderate-quality study assessing radiofrequency denervation of facet joints C2-C6 for cervicogenic headache (CH) compared to a sham procedure did not have any significant improvements at 12 or 24 months.(1177) A study evaluating radiofrequency versus occipital nerve block did not find any benefit of radiofrequency lesions over nerve block in cervicogenic headache patients.(1185) Studies in the lumbar spine are increasingly suggesting lack of efficacy (1544–1548), including the largest-sized trial that found neurotomy to be ineffective compared with an exercise program for treatment of LBP, SI joint pain, or intervertebral  disc pain (1549). The initial study for the cervical spine (1550) suggesting efficacy was small-sized, is now more than 20 years old, and has not been reproduced in a quality study, which is concerning.\n\nAs results can be permanent, there should be good evidence of long-term benefit prior to recommending this procedure. Radiofrequency lesioning is invasive, has adverse effects, and is costly. There is evidence of a lack of efficacy for treatment of lumbar pain, thus there is an unreconciled dispute in the literature (ineffective in the lumbar spine, but perhaps some efficacy in the cervical spine). This is not recommended as a first or second line procedure and is recommended only in the setting of participation in an active rehabilitation program in a patient who is motivated to increase his/her daily functioning."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-chronic",
        "label-name": "Cervicothoracic Pain - Chronic"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have radiculopathy?",
          "option-list": [
            {
              "option": "yes",
              "action": "deny"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed conservative treatments?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Has the patient had a confirmed diagnosis by medial branch blocks?",
          "option-list": [
            {
              "option": "yes",
              "action": "no_recommendation"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of radiofrequency neurotomy, neurotomy, or facet rhizotomy for the treatment of chronic cervicothoracic pain confirmed with diagnostic blocks, but who do not have radiculopathy and who have failed conservative treatment. (64% panel agreement; 36% of panel agreed with limited indications as indicated below.)\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Chronic cervicothoracic pain patients without radiculopathy who failed conservative treatments and who have had a confirmed diagnosis by medial branch blocks.(69)          \n\nFrequency/Dose/Duration: One procedure might be tried after failure of non-invasive treatments including NSAIDs and a quality exercise program or as a means to help with participation in an active rehabilitation program. There is no recommendation for repeated procedures. It is reasonable to attempt a second lesion after 26 weeks in patients who had greater than 50% improvement in pain from first procedure for the first 8 weeks with a late return of pain.(1184) There is no recommendation for a third or for additional procedures. There is logically a limit as to how many times it is possible to permanently destroy the same nerve.\n\nIndications for Discontinuation:        Resolution of symptoms. If there is no response to the first procedure, there is no evidence that a second lesion will be beneficial.\n\nRationale for Recommendations: A moderate-quality, sham controlled trial evaluating patients with cervical zygapophyseal-joint pain diagnosed with anesthetic blocks, but without any radicular symptoms, showed improvement in pain over a sham procedure at 12 months. However, there were statistically more patients in the sham group involved in litigation over the accident that caused their pain (p = 0.04) than in the intervention group.(1184) Thus, even though the study’s methodology scores were good, it has a potential fatal flaw or bias. Another moderate-quality study assessing radiofrequency denervation of facet joints C2-C6 for cervicogenic headache (CH) compared to a sham procedure did not have any significant improvements at 12 or 24 months.(1177) A study evaluating radiofrequency versus occipital nerve block did not find any benefit of radiofrequency lesions over nerve block in cervicogenic headache patients.(1185) Studies in the lumbar spine are increasingly suggesting lack of efficacy (1544–1548), including the largest-sized trial that found neurotomy to be ineffective compared with an exercise program for treatment of LBP, SI joint pain, or intervertebral  disc pain (1549). The initial study for the cervical spine (1550) suggesting efficacy was small-sized, is now more than 20 years old, and has not been reproduced in a quality study, which is concerning.\n\n \n\n                                                     As results can be permanent, there should be good evidence of long-term benefit prior to recommending this procedure. Radiofrequency lesioning is invasive, has adverse effects, and is costly. There is evidence of a lack of efficacy for treatment of lumbar pain, thus there is an unreconciled dispute in the literature (ineffective in the lumbar spine, but perhaps some efficacy in the cervical spine). This is not recommended as a first or second line procedure and is recommended only in the setting of participation in an active rehabilitation program in a patient who is motivated to increase his/her daily functioning."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fheadache-pain%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
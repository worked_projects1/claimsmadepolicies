{
  "treatment": {
    "index-name": "thoracic-discectomy",
    "label-name": "Thoracic Discectomy",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "08/19/22",
    "created-by": "Bill",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "thoracic-subacute-radiculopathy",
        "label-name": "Thoracic Subacute Radiculopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is there a radicular pain syndrome with current dermatomal pain and/or numbness consistent with a herniated disc?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Do imaging findings by MRI, or CT with or without myelography that confirm persisting nerve root compression at the level and on the side predicted by the history and clinical examination?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is there continued significant pain and functional limitation after at least 3 months of time and appropriate non-operative treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Thoracic discectomy is recommended for treatment of patients with ongoing nerve root compression who continue to have significant pain and functional limitation after at least 3 months of time and appropriate non-operative therapy. The decision as to which type of discectomy procedure to perform should be left to the surgeon and the patient until quality evidence becomes available to provide evidence-based guidance.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications:                                           All of the following present: 1) radicular pain syndrome with current dermatomal pain and/or numbness consistent with a herniated disc; 2) imaging findings by MRI, or CT with or without myelography that confirm persisting nerve root compression at the level and on the side predicted by the history and clinical examination; and 3) continued significant pain and functional limitation after at least 3 months of time and appropriate non-operative treatment.                    \n\nRationale:                                              There are no quality studies on treatment of symptomatic herniated thoracic discs. However, the same indications are believed to be necessary for treatment of patients with these relatively less common issues. There is no significant muscle weakness problem with thoracic disc herniations. The issues are pain, and potentially spinal cord compression with leg spasticity and ataxia, and bowel or bladder control impairment. The current literature does not permit a conclusion that open discectomy, microdiscectomy, or endoscopic discectomy should be the preferred procedure as there are no quality comparative trials for treatment of the cervical or thoracic spine. There is no quality evidence that automated percutaneous discectomy, laser discectomy, or coblation therapy is an effective treatment for any cervical or thoracic spine problem. There are no quality studies for this issue, which is relative uncommon. Patients who are candidates for discectomy should be informed that (other than likely for progressive neurological deficits and the rare progressive major neurologic deficit), there is evidence that there is no need to rush surgical decisions. The decision as to which type of discectomy procedure to perform should be left to the surgeon and the patient until quality evidence becomes available to provide evidence-based guidance.             "
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "thoracic-chronic-radiculopathy",
        "label-name": "Thoracic Chronic Radiculopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is there a radicular pain syndrome with current dermatomal pain and/or numbness consistent with a herniated disc?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Do imaging findings by MRI, or CT with or without myelography that confirm persisting nerve root compression at the level and on the side predicted by the history and clinical examination?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Is there continued significant pain and functional limitation after at least 3 months of time and appropriate non-operative treatment?  ",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Thoracic discectomy is recommended for treatment of patients with ongoing nerve root compression who continue to have significant pain and functional limitation after at least 3 months of time and appropriate non-operative therapy. The decision as to which type of discectomy procedure to perform should be left to the surgeon and the patient until quality evidence becomes available to provide evidence-based guidance.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications:                                           All of the following present: 1) radicular pain syndrome with current dermatomal pain and/or numbness consistent with a herniated disc; 2) imaging findings by MRI, or CT with or without myelography that confirm persisting nerve root compression at the level and on the side predicted by the history and clinical examination; and 3) continued significant pain and functional limitation after at least 3 months of time and appropriate non-operative treatment.                    \n\nRationale:                                              There are no quality studies on treatment of symptomatic herniated thoracic discs. However, the same indications are believed to be necessary for treatment of patients with these relatively less common issues. There is no significant muscle weakness problem with thoracic disc herniations. The issues are pain, and potentially spinal cord compression with leg spasticity and ataxia, and bowel or bladder control impairment. The current literature does not permit a conclusion that open discectomy, microdiscectomy, or endoscopic discectomy should be the preferred procedure as there are no quality comparative trials for treatment of the cervical or thoracic spine. There is no quality evidence that automated percutaneous discectomy, laser discectomy, or coblation therapy is an effective treatment for any cervical or thoracic spine problem. There are no quality studies for this issue, which is relative uncommon. Patients who are candidates for discectomy should be informed that (other than likely for progressive neurological deficits and the rare progressive major neurologic deficit), there is evidence that there is no need to rush surgical decisions. The decision as to which type of discectomy procedure to perform should be left to the surgeon and the patient until quality evidence becomes available to provide evidence-based guidance.             "
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fradicular-pain%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
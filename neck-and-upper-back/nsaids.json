{
  "treatment": {
    "index-name": "nsaids",
    "label-name": "NSAIDs",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "neck-and-upper-back",
    "label-name": "Neck and Upper Back"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "09/08/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-acute",
        "label-name": "Cervicothoracic Pain - Acute"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative cervicothoracic pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, chronic, or post-operative cervicothoracic pain; over-the-counter (OTC) agents may suffice and be tried first.\n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: Scheduled dosage rather than as-needed preferable; as-needed prescriptions may be reasonable for mild or moderate chronic cervicothoracic pain.\n\nIndications for Discontinuation: Resolution of cervicothoracic pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-subacute",
        "label-name": "Cervicothoracic Pain - Subacute"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative cervicothoracic pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, chronic, or post-operative cervicothoracic pain; over-the-counter (OTC) agents may suffice and be tried first.\n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: Scheduled dosage rather than as-needed preferable; as-needed prescriptions may be reasonable for mild or moderate chronic cervicothoracic pain.\n\nIndications for Discontinuation: Resolution of cervicothoracic pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cervicothoracic-pain-chronic",
        "label-name": "Cervicothoracic Pain - Chronic"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative cervicothoracic pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, chronic, or post-operative cervicothoracic pain; over-the-counter (OTC) agents may suffice and be tried first.\n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: Scheduled dosage rather than as-needed preferable; as-needed prescriptions may be reasonable for mild or moderate chronic cervicothoracic pain.\n\nIndications for Discontinuation: Resolution of cervicothoracic pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-cervicothoracic-radicular-pain-syndromes",
        "label-name": "Acute Cervicothoracic Radicular Pain Syndromes"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of cervicothoracic radicular pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Radicular pain syndromes.                          \n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: In acute radicular pain syndromes, scheduled dosage rather than as needed is preferable; as-needed prescriptions may be reasonable for mild or moderate chronic radicular pain.\n\nIndications for Discontinuation: Resolution of symptoms, lack of efficacy, or development of adverse effects that necessitate discontinuation. It should be noted that resolution of radicular symptoms generally takes significantly longer than resolution of acute cervicothoracic pain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-cervicothoracic-radicular-pain-syndromes",
        "label-name": "Subacute Cervicothoracic Radicular Pain Syndromes"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of cervicothoracic radicular pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Radicular pain syndromes.                          \n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: In acute radicular pain syndromes, scheduled dosage rather than as needed is preferable; as-needed prescriptions may be reasonable for mild or moderate chronic radicular pain.\n\nIndications for Discontinuation: Resolution of symptoms, lack of efficacy, or development of adverse effects that necessitate discontinuation. It should be noted that resolution of radicular symptoms generally takes significantly longer than resolution of acute cervicothoracic pain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-cervicothoracic-radicular-pain-syndromes",
        "label-name": "Chronic Cervicothoracic Radicular Pain Syndromes"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. NSAIDs are recommended for treatment of cervicothoracic radicular pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Radicular pain syndromes.                          \n\nBenefits: Modest reduction in spine pain and earlier recovery.\n\nHarms: Gastrointestinal bleeding, other bleeding, and possible delayed fracture healing. Possible elevated cardiovascular risks including myocardial infarction, especially for high-dose COX-2 inhibitors. Renal failure may occur particularly in the elderly or those with otherwise compromised function.\n\nFrequency/Dose/Duration: In acute radicular pain syndromes, scheduled dosage rather than as needed is preferable; as-needed prescriptions may be reasonable for mild or moderate chronic radicular pain.\n\nIndications for Discontinuation: Resolution of symptoms, lack of efficacy, or development of adverse effects that necessitate discontinuation. It should be noted that resolution of radicular symptoms generally takes significantly longer than resolution of acute cervicothoracic pain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patients-at-risk-for-cardiovascular-adverse-effects",
        "label-name": "Patients at Risk for Cardiovascular Adverse Effects"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. It is recommended that patients with known cardiovascular disease or multiple risk factors for cardiovascular disease should know the risks and benefits of NSAID therapy for pain discussed.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nBenefits: Counter risk of adverse event.\n\nHarms: None."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fcervical-and-thoracic-spine%2Fdiagnostic-and-treatment-recommendations%2Fcervicothoracic-pain%2Ftreatment-recommendations%2Fmedications",
    "odg-link": ""
  }
}
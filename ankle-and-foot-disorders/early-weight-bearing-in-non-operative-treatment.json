{
  "treatment": {
    "index-name": "early-weight-bearing-in-non-operative-treatment",
    "label-name": "Early Weight Bearing in Non-operative Treatment",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/28/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "achilles-tendon-rupture",
        "label-name": "Achilles Tendon Rupture"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against early weight bearing for non-operatively managed Achilles tendon ruptures.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are five moderate-quality trials comparing non-operative management with surgical repair for ruptured Achilles tendons.(89-91, 115, 116, 118, 119) One trial suggested surgical management was superior to non-operative management for reducing risk of re-rupture,(89, 118) but did not have an important aspect of care (timing of casting and mobilization) held constant. In the other trials, there appeared to be a non-statistically significant trend towards higher re-rupture rates among the non-operative groups (there were no trials suggesting higher risk of re-rupture in the surgical groups).(90, 115, 116, 119)\n\nKhan pooled data from three studies into a summary odds ratio and 95% confidence limit derived from the meta-analysis, which showed that non-surgical treatment was likely to result in 3.7 times more reruptures than surgical treatment; however, overall rerupture rates are low enough in surgical and non-surgical reapproximation methods to make 11 operations necessary to avoid one rerupture.(117) Additionally, simple arithmetic summing of their data allowed calculation of an overall rerupture and infection rates, which are described above. The evidence indicates surgery reduces risk of re-rupture compared to non-operative treatment, but given a low overall rerupture rate, the effect is not dramatic.\n\nOne trial found no difference in lost time,(90) and two reported less lost time with the surgical group.(89, 91, 115, 118) A low-quality RCT also documented less lost time in the surgically repaired group.(87) One noted time-to-return-to-work favored the subset of population performing light work that received surgery (35.7 days versus 67.2 days), but the advantage was equivocal in sedentary and heavy job classifications. Overall, the studies suggest that persons in jobs that require mobility may benefit from surgical repair.(89, 91, 118) One author suggested early mobilization is the most important factor in treating ruptured Achilles tendons.(116) Moeller investigated differences in tendon healing based with MRI and ultrasound studies(91) and found no differences of partial defect, tendon thickness, homogenicity, tendinous edema, peritendinous reaction, or pattern of motion and the type of treatment received.\n\nHowever, non-operative management appears to be effective in most patients.(89-91, 116, 118) There are few trials on casting and splinting or bracing. One moderate-quality study compared functional splinting with casting and reported higher satisfaction in the bracing compared with casting.(120) No differences in re-rupture rates or complications were found. There was a significant difference in dorsiflexion range of motion favoring the splinting group, although the clinical significance of this finding is unknown. The bracing group also self-reported shorter time required to be able to walk comfortably indoors and outdoors. However, this was a small study and was not a randomized crossover trial, which limits the utility to make a recommendation for one method over another. Thus, both methods are recommended as they are non-invasive, have similar long-term efficacy, and are reported as an effective treatment arm in other studies. Use of splinting is now becoming more common, with the primary advantage being patient preference.\n\nOne high-quality trial evaluated early weight bearing comparing non-operative immediate weight bearing using an orthosis to the use of a non-weight bearing rigid cast over a 12-week treatment period.(121) Both groups were placed in the equinus position for 6 weeks followed by reduction of 1.5 inches every 2 weeks until the ankle was in a neutral position at 12 weeks. Evaluations at 3, 6, and 12 months did not demonstrate any significant differences in walking, stair climbing, return to work, return to sport, quality of life scores, or deficits in range of motion or torque. From this single study, it appears early weight bearing using the protocol described did not result in a significant benefit or adverse effect. Therefore, there is no recommendation for immediate weight bearing over rigid immobilization. Early weight bearing was found to provide functional improvement over rigid immobilization after surgical repair (see Post-Operative Care), but further evidence is needed to make a similar recommendation for non-operative care."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendon-rupture%2Ftreatment-recommendations%2Fnonoperative-management",
    "odg-link": ""
  }
}
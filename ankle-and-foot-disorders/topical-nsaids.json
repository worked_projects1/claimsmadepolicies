{
  "treatment": {
    "index-name": "topical-nsaids",
    "label-name": "Topical NSAIDs",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/22/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-sprain",
        "label-name": "Acute Ankle Sprain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have contraindications for oral treatment or does not prefer to take oral medications?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Moderately Recommended. Topical NSAIDs are moderately recommended for the treatment of acute ankle sprain.\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nLevel of Confidence – Moderate\n\nIndications: Acute ankle sprain or patients with contraindications for oral treatment or who prefer not to take oral medications. No evidence of comparative superiority of one topical NSAID over another.\n\nFrequency/Dose/Duration: Frequency per manufacturer’s recommendation. Topical NSAID use has been reported for 1 to 3 weeks.(39)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-sprain",
        "label-name": "Subacute Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of topical NSAIDs for the treatment of subacute, chronic, or post-operative ankle sprain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are 3 high- and 3 moderate-quality placebo-controlled randomized trials for the use of topical NSAIDs in the treatment of ankle sprain injuries. All 3 high-quality RCTs demonstrated treatment benefit in analgesia and swelling reduction using daily ketoprofen patch,(479) diclofenac gel, and ketorolac or etofenamate gel used over a 2-week period.(480) A moderate-quality trial demonstrated no benefit from Piroxicam gel over placebo for ankle sprains,(39) while a second moderate-quality study found niflumic acid gel to provide benefit in pain and functional improvement in the acute phase of injury.(481) A third moderate-quality study found no treatment effect with flurbiprofen patch until 7 days post injury compared with placebo.(482) There are no trials that demonstrate efficacy of one NSAID over another. One high-quality trial demonstrated increased efficacy in diclofenac gel formulated with lecithin compared to non-lecithin gel, although no placebo arm was included.(483) One moderate-quality trial demonstrated equivalent efficacy of NSAID gel with therapeutic ultrasound, although other studies found no benefit from ultrasound (see Physical Methods – Ultrasound).(484) Topical NSAIDs are not invasive, have low adverse effect rates, but may cumulatively be moderate to high cost. They are recommended for treatment of acute ankle sprain, particularly in patients who do not tolerate or are poor candidates for oral treatment. Post-operative patients may be reasonable candidates after the incision is well healed."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-sprain",
        "label-name": "Chronic Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of topical NSAIDs for the treatment of subacute, chronic, or post-operative ankle sprain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are 3 high- and 3 moderate-quality placebo-controlled randomized trials for the use of topical NSAIDs in the treatment of ankle sprain injuries. All 3 high-quality RCTs demonstrated treatment benefit in analgesia and swelling reduction using daily ketoprofen patch,(479) diclofenac gel, and ketorolac or etofenamate gel used over a 2-week period.(480) A moderate-quality trial demonstrated no benefit from Piroxicam gel over placebo for ankle sprains,(39) while a second moderate-quality study found niflumic acid gel to provide benefit in pain and functional improvement in the acute phase of injury.(481) A third moderate-quality study found no treatment effect with flurbiprofen patch until 7 days post injury compared with placebo.(482) There are no trials that demonstrate efficacy of one NSAID over another. One high-quality trial demonstrated increased efficacy in diclofenac gel formulated with lecithin compared to non-lecithin gel, although no placebo arm was included.(483) One moderate-quality trial demonstrated equivalent efficacy of NSAID gel with therapeutic ultrasound, although other studies found no benefit from ultrasound (see Physical Methods – Ultrasound).(484) Topical NSAIDs are not invasive, have low adverse effect rates, but may cumulatively be moderate to high cost. They are recommended for treatment of acute ankle sprain, particularly in patients who do not tolerate or are poor candidates for oral treatment. Post-operative patients may be reasonable candidates after the incision is well healed."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-plantar-fasciitis-pain",
        "label-name": "Acute Plantar Fasciitis Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe plantar fasciitis and/or contraindication(s) for oral treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic plantar fascial pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Mild, moderate, or severe plantar fasciitis or in patients with contraindications for oral treatment. There is no evidence of comparative superiority of one topical NSAID versus another.\n\nFrequency/Dose/Duration: Frequency according to manufacturer’s recommendation. Topical NSAIDs have been used for 1 to 3 weeks.(39)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-plantar-fasciitis-pain",
        "label-name": "Subacute Plantar Fasciitis Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe plantar fasciitis and/or contraindication(s) for oral treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic plantar fascial pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Mild, moderate, or severe plantar fasciitis or in patients with contraindications for oral treatment. There is no evidence of comparative superiority of one topical NSAID versus another.\n\nFrequency/Dose/Duration: Frequency according to manufacturer’s recommendation. Topical NSAIDs have been used for 1 to 3 weeks.(39)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-plantar-fasciitis-pain",
        "label-name": "Chronic Plantar Fasciitis Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe plantar fasciitis and/or contraindication(s) for oral treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic plantar fascial pain syndromes.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Mild, moderate, or severe plantar fasciitis or in patients with contraindications for oral treatment. There is no evidence of comparative superiority of one topical NSAID versus another.\n\nFrequency/Dose/Duration: Frequency according to manufacturer’s recommendation. Topical NSAIDs have been used for 1 to 3 weeks.(39)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-achilles-tendinopathy",
        "label-name": "Acute Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe achilles tendinopathy?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic Achilles tendinosis.\n\nStrength of Evidence (Acute, subacute) – Recommended, Evidence (C)\nStrength of Evidence (Chronic) – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Mild, moderate, or severe Achilles tendinopathy. Only niflumic acid as a topical NSAID treatment for Achilles tendon disorders has been studied(41); (Auclair 89) thus, there is no evidence of comparative superiority of any other topical NSAID.\n\nFrequency/Dose/Duration: Frequency per manufacturer’s recommendation. Niflumic acid was used for 1 week(41) (Auclair 89) and piroxicam for 1 to 3 weeks (study of mixed acute disorders, 3% were Achilles tendonitis).(39) (Russell 91)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-achilles-tendinopathy",
        "label-name": "Subacute Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe achilles tendinopathy?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic Achilles tendinosis.\n\nStrength of Evidence (Acute, subacute) – Recommended, Evidence (C)\nStrength of Evidence (Chronic) – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Mild, moderate, or severe Achilles tendinopathy. Only niflumic acid as a topical NSAID treatment for Achilles tendon disorders has been studied(41); (Auclair 89) thus, there is no evidence of comparative superiority of any other topical NSAID.\n\nFrequency/Dose/Duration: Frequency per manufacturer’s recommendation. Niflumic acid was used for 1 week(41) (Auclair 89) and piroxicam for 1 to 3 weeks (study of mixed acute disorders, 3% were Achilles tendonitis).(39) (Russell 91)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-achilles-tendinopathy",
        "label-name": "Chronic Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe achilles tendinopathy?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Topical NSAIDs are recommended for treatment of acute, subacute, or chronic Achilles tendinosis.\n\nStrength of Evidence (Acute, subacute) – Recommended, Evidence (C)\nStrength of Evidence (Chronic) – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Mild, moderate, or severe Achilles tendinopathy. Only niflumic acid as a topical NSAID treatment for Achilles tendon disorders has been studied(41); (Auclair 89) thus, there is no evidence of comparative superiority of any other topical NSAID.\n\nFrequency/Dose/Duration: Frequency per manufacturer’s recommendation. Niflumic acid was used for 1 week(41) (Auclair 89) and piroxicam for 1 to 3 weeks (study of mixed acute disorders, 3% were Achilles tendonitis).(39) (Russell 91)\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-sprain%2Ftreatment-recommendations%2Fmedications,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Fmedications,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendinopathy%2Ftreatment-recommendations%2Fmedications",
    "odg-link": ""
  }
}
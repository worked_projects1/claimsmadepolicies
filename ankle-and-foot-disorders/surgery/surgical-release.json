{
  "treatment": {
    "index-name": "surgical-release",
    "label-name": "Surgical Release",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/28/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "space-occupying-lesion-in-tarsal-tunnel-syndrome-(tts)",
        "label-name": "Space-Occupying Lesion in Tarsal Tunnel Syndrome (TTS)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Surgical release of posterior tibial nerve impingement at the tarsal tunnel is recommended upon failure of conservative treatment and in the presence of space occupying lesion. Surgical release for cases with nonspecific causes are otherwise expected to have mixed results and patients should be counseled regarding potential lack of benefit before consideration of surgery. There is no recommendation for any specific technique as there is a lack of quality evidence.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nRationale: Surgical intervention is controversial as there are no quality trials comparing surgery with conservative care methods, or any quality studies evaluating the overall efficacy of surgical intervention. Further, although surgical techniques have changed over time, there are no comparison studies of techniques. The majority of TTS cases described in the literature ultimately resulted in surgical release. Case reports and series generally report space occupying lesions are responsible for symptoms in many cases, although there is no data to indicate what percentage of overall TTS. There are few data reported on complications, efficacy of symptom relief, or correction of neurosensory deficits post surgery. Results of a case series (n = 32 feet) of patients undergoing surgical release and followed longitudinally 24 to 118 months found only 44% had good or excellent results with 48% dissatisfied with the results.(374) Somewhat similar to CTS, no correlation was found between pre-operative electrodiagnostic results and clinical results (negative versus positive versus not done). The only reliable predictor of favorable result was identification of an anatomic lesion. Another case series (n = 34) comparing patients who had surgery with those who did not, report 50% efficacy of conservative treatment, whereas surgical decompression effectively relieved some symptoms in 79% of cases, although varied by diagnosis. The authors concluded aggressive treatment is warranted, although the prognosis overall is mixed, and should be preceded by a trial of conservative therapy prior to surgical release.(327) Finally, the author in a case series of 108 ankles (72 patients) surgically released after failure of conservative treatment patients concluded that patients operated on sooner than 12 months after onset of symptoms had significantly higher postoperative scores on the Maryland Foot Score tool at 12 month follow-up than those with longer duration of symptoms, although both groups showed significant improvement in scores compared with preoperative measurements.(375)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-tenosynovitis",
        "label-name": "Subacute Ankle Tenosynovitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have ankle tenosynovitis that has failed to respond to non-operative interventions generally including at least 2 glucocorticosteroid injections? (May be indicated without prior injection(s) if there is a clear contraindication for injections).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of surgical release for patients with subacute or chronic ankle tenosynovitis who fail to respond to injection. (Lapidus 72)\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Ankle tenosynovitis that fails to respond to non-operative interventions generally including at least 2 glucocorticosteroid injections. May be indicated without prior injection(s) if there is a clear contraindication for injections. Tendinous ruptures are often surgically treated.\n\nRationale: There are no quality studies evaluating the use of surgical release for ankle tenosynovitis. It may be a last resort for patients who have failed glucocorticosteroid injection(s) and other non-invasive treatments, but no recommendation is offered. A non-randomized study of 27 patients who underwent arthroscopic release for flexor hallucis longus tenosynovitis found 81% to have returned to the same level of activity prior to the injury. (Corte-Real 12) In another study, 13 female ballet dances underwent operative release of the flexor hallucis longus tendon due to stenosing tenosynovitis. After a mean follow-up time of six years and six months, the authors found the treatment to be effective. All patients returned to dancing within 5 months, and 11 reached full participation. (Kolettis 96)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-tenosynovitis",
        "label-name": "Chronic Ankle Tenosynovitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have ankle tenosynovitis that has failed to respond to non-operative interventions generally including at least 2 glucocorticosteroid injections? (May be indicated without prior injection(s) if there is a clear contraindication for injections).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of surgical release for patients with subacute or chronic ankle tenosynovitis who fail to respond to injection. (Lapidus 72)\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Ankle tenosynovitis that fails to respond to non-operative interventions generally including at least 2 glucocorticosteroid injections. May be indicated without prior injection(s) if there is a clear contraindication for injections. Tendinous ruptures are often surgically treated.\n\nRationale: There are no quality studies evaluating the use of surgical release for ankle tenosynovitis. It may be a last resort for patients who have failed glucocorticosteroid injection(s) and other non-invasive treatments, but no recommendation is offered. A non-randomized study of 27 patients who underwent arthroscopic release for flexor hallucis longus tenosynovitis found 81% to have returned to the same level of activity prior to the injury. (Corte-Real 12) In another study, 13 female ballet dances underwent operative release of the flexor hallucis longus tendon due to stenosing tenosynovitis. After a mean follow-up time of six years and six months, the authors found the treatment to be effective. All patients returned to dancing within 5 months, and 11 reached full participation. (Kolettis 96)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ftts%2Ftreatment-recommendations%2Fsurgical-considerations , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ftenosynovitis%2Fsurgery",
    "odg-link": ""
  }
}
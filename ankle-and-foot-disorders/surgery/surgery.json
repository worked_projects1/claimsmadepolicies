{
  "treatment": {
    "index-name": "surgery",
    "label-name": "Surgery",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/28/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-plantar-fasciitis",
        "label-name": "Acute Plantar Fasciitis"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgical release is not recommended for treatment of acute or subacute plantar fasciitis.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nRationale: There are no quality randomized trials that compare sham surgery with surgical release, none that include surgery as a treatment arm for chronic plantar fasciitis, and none that compare efficacy of open versus endoscopic or other procedures. There is a dearth of case series reports of surgical plantar fascia release. Plantar fasciotomy is reported to have a complete pain relief success rate of 44%,(313) (Faraj 02) 50%,(199) (Davies 99) 61%,(314) (Conflitti 04) 68%,(315) (Hogan 04) and 69%.(316) (Jarde 03) Complete satisfaction is also reported between 48%(199) (Davies 99) and 85%.(315) (Hogan 04) Average return to work or daily activities can range from 1.5(314) (Conflitti 04) to 7.85 months.(199) (Davies 99) Patients in the workers’ compensation system have reportedly faired worse in satisfaction and lost time than those in non-workers’ compensation systems.(317) (Bazaz 07) Fascial release is also associated with many adverse effects, including acute plantar fasciitis, forefoot stress fractures, and calcaneal and cuboid fractures.(318) (Cheung 06) Fascial release greater than 50% of the thickness may result in instability of the plantar arch(319) (Jerosch 04) and result in lateral column pain symptoms.(320) (Brugh 02) There is no quality evidence on the added inclusion of spur excision or release of the abductor digiti quinti nerve with plantar release surgery. Thus, while surgery appears to provide complete relief to about half of patients, it is not without significant risk of complication, expense, and lack of comparison data to other non-surgical interventions.\n\nTherefore, surgery is recommended as an intervention after at least 6 months of other non-operative treatments have been attempted and the patient’s symptoms are sufficient to warrant the risks of surgical intervention. Patient education regarding suboptimal expected outcomes is recommended. There is no recommendation for or against procedure type (i.e. open vs. endoscopic) or the adjunct procedures (i.e. spur excision, neurolysis or release of abductor digiti quinti nerve)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-plantar-fasciitis",
        "label-name": "Subacute Plantar Fasciitis"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgical release is not recommended for treatment of acute or subacute plantar fasciitis.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nRationale: There are no quality randomized trials that compare sham surgery with surgical release, none that include surgery as a treatment arm for chronic plantar fasciitis, and none that compare efficacy of open versus endoscopic or other procedures. There is a dearth of case series reports of surgical plantar fascia release. Plantar fasciotomy is reported to have a complete pain relief success rate of 44%,(313) (Faraj 02) 50%,(199) (Davies 99) 61%,(314) (Conflitti 04) 68%,(315) (Hogan 04) and 69%.(316) (Jarde 03) Complete satisfaction is also reported between 48%(199) (Davies 99) and 85%.(315) (Hogan 04) Average return to work or daily activities can range from 1.5(314) (Conflitti 04) to 7.85 months.(199) (Davies 99) Patients in the workers’ compensation system have reportedly faired worse in satisfaction and lost time than those in non-workers’ compensation systems.(317) (Bazaz 07) Fascial release is also associated with many adverse effects, including acute plantar fasciitis, forefoot stress fractures, and calcaneal and cuboid fractures.(318) (Cheung 06) Fascial release greater than 50% of the thickness may result in instability of the plantar arch(319) (Jerosch 04) and result in lateral column pain symptoms.(320) (Brugh 02) There is no quality evidence on the added inclusion of spur excision or release of the abductor digiti quinti nerve with plantar release surgery. Thus, while surgery appears to provide complete relief to about half of patients, it is not without significant risk of complication, expense, and lack of comparison data to other non-surgical interventions.\n\nTherefore, surgery is recommended as an intervention after at least 6 months of other non-operative treatments have been attempted and the patient’s symptoms are sufficient to warrant the risks of surgical intervention. Patient education regarding suboptimal expected outcomes is recommended. There is no recommendation for or against procedure type (i.e. open vs. endoscopic) or the adjunct procedures (i.e. spur excision, neurolysis or release of abductor digiti quinti nerve)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-recalcitrant-plantar-fasciitis",
        "label-name": "Chronic Recalcitrant Plantar Fasciitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate to severe chronic plantar fasciitis?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed multiple non-surgical treatments, and has their condition lasted at least 6 to 12 months?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Has the patient failed NSAID(s), plantar fascia stretching, injection(s) and failed or refused other more conservative treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Surgical release is recommended for select chronic recalcitrant plantar fasciitis. There is no recommendation for any particular procedure or method over another.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Moderate to severe chronic plantar fasciitis patients who have failed multiple non-surgical treatments and whose condition has lasted at least 6 to 12 months. Patients should generally have failed NSAID(s), plantar fascia stretching, injection(s) and failed or refused other more conservative treatment. Patients should receive pre-operative education regarding expected outcomes."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hallux-valgus-(bunions)",
        "label-name": "Hallux Valgus (Bunions)"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate to severe hallux valgus where pain and/or debility are significant and changing shoe wear and orthotics fail to sufficiently control symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the patient have a milder case but pain is clearly localized to the bunion prominence while also demonstrating inadequate relief with shoe wear adjustments and other conservative measures?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Surgery is recommended for Hallux Valgus.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications – Select cases of mostly moderate hallux valgus where pain and/or debility are significant and changing shoe wear and orthotics fail to sufficiently control symptoms. However, some evidence suggests better outcomes with milder cases and those cases should have pain clearly localized to the bunion prominence while also demonstrating inadequate relief with shoe wear adjustments. (Deenik 08)\n\nRationale – There is one moderate quality trial comparing surgery (chevron procedure) with orthosis with no treatment and found the best results with surgery. (Torkki 01) There are no other quality sham-controlled trials or comparative trials with non-operative treatments. There are many comparative trials comparing operative approaches. (Pentikainen 15, 12; Glazebrook 14; Matricali 14; Park 13a,b; Faber 13; Windhagen 13; Giannini 13;  Radwan 12; Klos 11; Deenik 08, 07; Saro 07; Easley 96; Resch 94; Klosok 93) The moderate quality comparative study reported osteotomy was superior to either orthotics or no treatment. Thus, surgery is recommended where non-operative approaches are insufficient."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hammer-toe",
        "label-name": "Hammer Toe"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "There are various surgical procedures used (arthroplasty, flexor tendon transfer, flexor tenotomy, extensor tendon lengthening and metatarsophalangeal joint capsulotomy, fusion, and diaphysectomy)  (Schuberth 99, Thomas 09; Shirzad 11; Witt 12; Klammer 12; Veljkovic 14; Errichiello 12). Yet, there are no quality studies compared with no treatment or non-surgical options. There is one comparative study of operative treatments. (Klammer 12) Thus, surgery is Recommended, Insuffiicent Evidence (I), Level of Confidence – Low for hammer toes with insufficient results from non-operative treatment."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "mid-tarsus-sprain",
        "label-name": "Mid-tarsus Sprain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a severe case, unstable injury, and significant diastasis (e.g., >2mm)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Surgery is Recommended, Insufficient Evidence (I), Level of Confidence – Moderate, for all severe cases, unstable injuries, and those with significant diastasis [e.g., >2mm (Myerson 08)]. Other than one quality study suggesting inferiority of ORIF, (Henning 09) there are no other quality comparative trials for the more common operative techniques (screw fixation vs. arthrodesis vs. ligament reconstruction). There is not quality evidence to preferentially support immediate (24-48 hour surgery post-injury), however some surgeons prefer this often with percutaneous fixation techniques, while others opt to wait approximately one week for swelling to subside.  It is helpful to coordinate with the surgeon regarding these preferences. Post-operative, non-weight bearing status is usually maintained for 8-12 weeks."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-achilles-tendinopathy-without-rupture",
        "label-name": "Acute Achilles Tendinopathy without Rupture"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgery is not recommended for acute or subacute Achilles tendinopathy without rupture.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nRationale: There are no quality trials comparing surgical intervention(s) with continued non-operative interventions for patients with Achilles tendinopathies. Further, there are no trials comparing different surgical techniques. There are several studies that indicate surgical success as measured by satisfied or very satisfied scores is up to 85%.(30) Success rates at 7 months in a prospective study were higher for paratenonitis (88%) versus only 54% for those with intratendinous lesions, with complication rates of 6% versus 27% respectively.(79) Thus, while surgery appears to provide relief to the majority of patients, it is not without significant risk of complication, expense, and lack of comparison data to other non-surgical interventions. Therefore, surgery is not recommended until a course of at least 6 months of other non-operative treatments with demonstrated efficacy has been attempted and the patient’s symptoms are sufficient to warrant the risks of surgical intervention."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-achilles-tendinopathy-without-rupture",
        "label-name": "Subacute Achilles Tendinopathy without Rupture"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgery is not recommended for acute or subacute Achilles tendinopathy without rupture.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nRationale: There are no quality trials comparing surgical intervention(s) with continued non-operative interventions for patients with Achilles tendinopathies. Further, there are no trials comparing different surgical techniques. There are several studies that indicate surgical success as measured by satisfied or very satisfied scores is up to 85%.(30) Success rates at 7 months in a prospective study were higher for paratenonitis (88%) versus only 54% for those with intratendinous lesions, with complication rates of 6% versus 27% respectively.(79) Thus, while surgery appears to provide relief to the majority of patients, it is not without significant risk of complication, expense, and lack of comparison data to other non-surgical interventions. Therefore, surgery is not recommended until a course of at least 6 months of other non-operative treatments with demonstrated efficacy has been attempted and the patient’s symptoms are sufficient to warrant the risks of surgical intervention."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-achilles-tendinopathy-without-rupture",
        "label-name": "Chronic Achilles Tendinopathy without Rupture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a moderate to severe, chronic achilles tendinopathy who has failed multiple non-surgical treatments and whose condition has lasted at least 6 months?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed NSAID(s), eccentric exercises, iontophoresis, injection(s) and low level laser therapy?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Surgery is recommended for select cases of chronic Achilles tendinopathy without rupture. There is no recommendation for any particular procedure over another.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Patients with moderate to severe chronic Achilles tendinopathies who have failed multiple non-surgical treatments and whose condition has lasted at least 6 months. Patients should generally have failed NSAID(s), eccentric exercises, iontophoresis, injection(s) and low level laser therapy."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "achilles-tendon-rupture",
        "label-name": "Achilles Tendon Rupture"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Surgical repair is recommended for treatment of ruptured Achilles tendon. (The mixed results of the data supporting operative and non-operative care should be discussed with patients when covering treatment options. Discussion should include the numbers needed to treat or harm or likelihood that they will benefit from surgical care versus non-surgical care – 1 in 11, or be harmed by surgical care – 1 in 3), and the equivocal superiority of surgical compared to non-operative treatment.)\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Moderate"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-ligament-tear",
        "label-name": "Acute Ankle Ligament Tear"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgical repair is not recommended for routine lateral ligament tear associated with acute or subacute ankle sprain.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-ligament-tear",
        "label-name": "Subacute Ankle Ligament Tear"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Surgical repair is not recommended for routine lateral ligament tear associated with acute or subacute ankle sprain.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-instability-(cai)",
        "label-name": "Chronic Ankle Instability (CAI)"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient had chronic ankle instability of at least 6-months duration, lateral ankle ligament laxity, and failure of non-operative therapies including therapy and use of ankle orthosis?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ligament reconstruction is recommended for select cases of chronic ankle instability.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Chronic ankle instability of at least 6-months duration, lateral ankle ligament laxity, and failure of non-operative therapies including therapy and use of ankle orthosis.\n\nRationale: There are six moderate-quality trials that compared operative repair with non-operative management of acute rupture of the lateral ligaments. No quality clinically important evidence has been demonstrated to recommend initial surgical repair over non-operative care. Three of the quality-trials compared surgery to functional treatment.(614-617) Seven low-quality trials compared surgical repair to functional treatment with all finding no clinically significant long-term differences. Two studies suggest limited benefit of operative intervention(511) (Gronmark 80) as measured by percentage of subjects symptomless at long-term follow-up, and number with fear of giving way.(500) Four trials suggest limited benefit of functional treatment as measured by higher percentage of subjects symptomless,(412) increased range-of-motion short-term(499, 618) and faster return to sport.(619) One trial found no differences in any outcome measure.(510) There were no differences in objective long-term functional outcomes demonstrated between the treatment groups. Subjectively, one study found functional treatment to result in patients becoming symptomless sooner than the surgical group,(616, 617) but another study has reported the functional group had a higher incidence of feeling ankle instability,(614) although no differences in sprain recurrence were demonstrated. One study found less reinjury in the surgical repair group, but more osteoarthrosis after surgery. Patients in a functional group using an ankle orthosis (Aircast) returned to work significantly faster than the operative group (1.6 weeks versus 7.0 weeks, p <0.001).(615)\n\nThere are two quality trials that compared cast immobilization with operative repair,(616, 617, 620) and seven low-quality trials that compared surgical repair to cast immobilization. Five studies demonstrated no differences between the two treatment types.(412, 499, 510, 605, 621) Two studies suggest limited benefit of operative intervention(511) as measured by percentage of subjects symptomless at long-term follow-up, and number with fear of giving way.(500) There were no long-term differences between the groups demonstrated in either study. Cast mobilization resulted in fewer reports of residual instability than operative repair.(620) Surgical repair is invasive, high cost, results in more lost-time from work, and has higher risk of adverse events than non-operative treatment. There is insufficient evidence that operative repair of ankle ligament ruptures provides significant long-term clinical benefit compared with non-operative care, and is therefore not recommended as an initial treatment for acute lateral ligament rupture of the ankle. Persistent functional instability of a chronic nature may be considered for ligament reconstruction. There are no quality-trials for repair of chronic ankle instability."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Fsurgical-considerations , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fbunions%2Ftreatment-recommendations%2Fsurgical-considerations , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhammer-toe , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmid-tarsus-pain , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendinopathy%2Ftreatment-recommendations%2Fsurgery , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-sprain%2Ftreatment-recommendations%2Fsurgical-considerations",
    "odg-link": ""
  }
}
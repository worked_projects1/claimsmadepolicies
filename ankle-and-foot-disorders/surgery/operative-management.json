{
  "treatment": {
    "index-name": "operative-management",
    "label-name": "Operative Management",
    "treatment-type": "surgery"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/28/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "displaced-metatarsal-shaft-fracture",
        "label-name": "Displaced Metatarsal Shaft Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a displaced metatarsal shaft fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for displaced metatarsal shaft fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Multiple metatarsals fractured if displaced; shaft fracture near metatarsal head.(678)\n\nRationale: There are no quality studies comparing non-operative treatment, fixation, bone screws, or plates for metatarsal fractures. There also are no quality studies defining acceptable limits of displacement for non-operative management, determining the ideal splint time or duration of internal or external fixation, making comparisons of fixation techniques or defining ideal post-operative rehabilitation protocols. Immobilization or fixation technique is therefore dictated by the physical and radiographic findings."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "displaced-tarsal-metatarsal-injury-(lisfranc)",
        "label-name": "Displaced Tarsal-Metatarsal Injury (Lisfranc)"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a fracture joint displacement with joint dislocation >2 mm?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for an unstable tarsal-metatarsal injury (Lisfranc).\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Fracture joint displacement with joint dislocation >2 mm.(676)\n\nComments: Operative fixation with K-wire or screws, or primary arthrodesis; non-weight bearing for 6 to 12 weeks and edema management (see Ankle and Calcaneus Fractures).(630, 676, 678, 823) Toe-touch weight-bearing in walking boot for 8 weeks. May take 4 to 5 months to heal; therapy may be started. Removal of hardware prior to full activities.(676)\n\nRationale: There are no quality trials comparing non-operative treatment to fixation, bone screws, or plates for tarsal-metatarsal joint injuries. There is one moderate-quality trial comparing primary arthrodesis to primary ORIF that demonstrated no functional differences, but the study was discontinued secondary to higher number of secondary surgeries for hardware removal.(823) It is unclear if removal was due to protocol or from symptomatic concerns. Twenty-two percent of eligible patients were enrolled in the study. Therefore, there is insufficient evidence to recommend primary arthrodesis as the initial procedure over primary ORIF. There are no quality studies defining acceptable limits of displacement for non-operative management, determining the ideal splint time or duration of internal or external fixation, making comparisons of fixation techniques or defining ideal post-operative rehabilitation protocols. Immobilization or fixation technique is therefore dictated by the physical and radiographic findings."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "distal-phalanx-fracture",
        "label-name": "Distal Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a displaced fracture of their great toe with poor reduction and unable to hold reduction with tape splinting?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Displaced fractures of great toe with poor reduction, unable to hold reduction with tape splinting.\n\nRationale: There are no quality studies for non-operative treatment, percutaneous fixation, bone screws, or plates for phalangeal fractures. There also are no quality studies defining acceptable limits of displacement for non-operative management, determining the ideal splint time or duration of internal or external fixation, making comparisons of fixation techniques or defining ideal post-operative rehabilitation impractical. Immobilization or fixation technique is therefore dictated by the physical and radiographic findings. It is generally limited to displaced fractures of the great toe or multiple toe fractures (see Phalangeal Fractures in Hand, Wrist, and Forearm Disorders guideline for analogous injury management)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "middle-phalanx-fracture",
        "label-name": "Middle Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a displaced fracture of their great toe with poor reduction and unable to hold reduction with tape splinting?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Displaced fractures of great toe with poor reduction, unable to hold reduction with tape splinting.\n\nRationale: There are no quality studies for non-operative treatment, percutaneous fixation, bone screws, or plates for phalangeal fractures. There also are no quality studies defining acceptable limits of displacement for non-operative management, determining the ideal splint time or duration of internal or external fixation, making comparisons of fixation techniques or defining ideal post-operative rehabilitation impractical. Immobilization or fixation technique is therefore dictated by the physical and radiographic findings. It is generally limited to displaced fractures of the great toe or multiple toe fractures (see Phalangeal Fractures in Hand, Wrist, and Forearm Disorders guideline for analogous injury management)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "proximal-phalanx-fracture",
        "label-name": "Proximal Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a displaced fracture of their great toe with poor reduction and unable to hold reduction with tape splinting?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Displaced fractures of great toe with poor reduction, unable to hold reduction with tape splinting.\n\nRationale: There are no quality studies for non-operative treatment, percutaneous fixation, bone screws, or plates for phalangeal fractures. There also are no quality studies defining acceptable limits of displacement for non-operative management, determining the ideal splint time or duration of internal or external fixation, making comparisons of fixation techniques or defining ideal post-operative rehabilitation impractical. Immobilization or fixation technique is therefore dictated by the physical and radiographic findings. It is generally limited to displaced fractures of the great toe or multiple toe fractures (see Phalangeal Fractures in Hand, Wrist, and Forearm Disorders guideline for analogous injury management)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "lower-extremity-stress-fracture",
        "label-name": "Lower Extremity Stress Fracture"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of operative management of lower extremity stress fractures in select patients.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality trials available for lower extremity stress fractures. Stress fractures are reported to respond well to activity restriction in most instances. Activity restriction is therefore recommended. Stress fractures that do not respond or that are displaced are treated operatively with fixation with and without graft. Athletes or persons that desire quicker return to activity often go straight to surgical intervention for stress fractures that are high-risk for non-union. Some high-risk fractures for non-union include talus, navicular, and fifth metatarsal.(681-685, 828) There is insufficient evidence for recommendation of operative management."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "calcaneus-fracture",
        "label-name": "Calcaneus Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a non-reduced, displaced intra-articular calcaneus fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "2"
            }
          ]
        },
        {
          "#": "2",
          "question": "Does the patient have a displaced, non-reducible extra-articular fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for select calcaneus fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Displaced, non-reducible extra-articular fractures, displaced intra-articular fractures.\n\nRationale: There are two moderate-quality trials that compared operative management (ORIF) to non-operative cast immobilization of non-reduced displaced intra-articular calcaneus fractures.(792, 802) A low-quality prospective trial with 15-year follow-up demonstrated no significant benefit from surgery for non-displaced calcaneus fractures over non-operative care.(668, 805) There are two additional reports from the original trial and are considered as one trial in this analysis.(674, 818) The study, which included 424 fractures with intraarticular displacement greater than 2mm, demonstrated no differences in functional and pain assessment scores between groups. Upon stratification, females, patients not receiving workers’ compensation, younger males, patients with a higher Bohler angle, patients with a lighter workload, and those with a single simple displaced intra-articular calcaneus fracture were demonstrated to have better results after operative treatment than after non-operative treatment. A second report demonstrated ORIF patients are more likely to develop complications, but workers’ compensation patients developed a high incidence of complications regardless of the management strategy chosen.(674) The third report demonstrated similar effect on subjective gait scores as the original report, with no differences between the non-stratified groups, but improved scores after stratification in the classifications of young males, non-workers’ compensation, moderate workload before injury, and restoration of the Bohler angle to above 0 degrees.(818) The second moderate-quality trial of 30 patients demonstrated surgical fixation results in lower pain scores and higher functional scores at 12 months. Thus, there is conflicting evidence for management recommendations, and both are recommended as treatment alternatives. Additional quality studies are needed.\n\nThere is evidence for consideration of non-operative management in workers’ compensation patients.(802) Both operative and non-operative management have considerable potential for adverse effects, including secondary late fusion, compartment syndrome and fasciotomy, DVT and pulmonary embolism, and late-term arthrodesis. Surgical intervention also may result in superficial and deep infection, malposition of fixation, and hardware removal,(674, 805, 819) and this should be taken into consideration when determining management technique. There is no quality evidence for costs of operative compared with non-operative care, although an economic study evaluated both direct and indirect costs, reporting operative management costs $19,000 CAN less per patient than non-operative management because of reduced lost time from work.(820) There is no quality evidence for one operative technique over another."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "displaced-talar-fracture",
        "label-name": "Displaced Talar Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a non-displaced, non-reducible fracture or a displaced talar fracture (head, neck, body, lateral process)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management is recommended for all displaced talar fractures (head, neck, body, lateral process).\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: All non-displaced, non-reducible fractures. Referral to specialist is indicated for all injuries due to the high potential for poor outcomes of these injuries. Emergent referral for talar neck fractures.(791)\n\nRationale: There are no quality trials for talar head, neck, body, or lateral process fractures. Because of the key role the talus plays in locomotion, and the risk for significant disability and complication with these fractures, most are managed aggressively with open reduction and internal fixation.(664, 791, 808) Referral to specialists for most, if not all, talus fractures is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "tibial-plafond-and-pilon-fracture",
        "label-name": "Tibial Plafond and Pilon Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a displaced, comminuted, or an inability to obtain acceptable fracture alignment with closed reduction?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Operative management for tibial plafond fractures is recommended in select patients.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Displaced, comminuted, or inability to obtain acceptable fracture alignment with closed reduction.\n\nRationale: Distal lower leg fractures that impinge on the articular surface with the talus are known as plafond fractures. As these fractures are often caused by axial forces driving the talus into the lower leg, they are often called “pilon” (hammer) fractures. In this section, the term “plafond fracture” will be used. There are no quality trials for operative fixation of tibial plafond fractures. A low-quality trial compared ORIF of tibial plafond fractures with external fixation with and without limited internal fixation and found no radiographic difference at 15 weeks follow-up, but did demonstrate higher wound complication rate in the ORIF group leading to 3 amputation surgeries.(651) These fractures are noted to have high complication rates from surgical reduction and fixation.(652) Numerous internal and external fixation techniques are described, but there is no recommendation for one method over others.(138, 726-732)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fforefoot-and-midfoot-fractures%2Ftreatment-recommendations%2Fmetatarsal-shaft-fractures , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fforefoot-and-midfoot-fractures%2Ftreatment-recommendations%2Ftarsal-metatarsal-joint-injury , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fforefoot-and-midfoot-fractures%2Ftreatment-recommendations%2Fphalangeal-fractures , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fforefoot-and-midfoot-fractures%2Ftreatment-recommendations%2Fstress-fractures , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhindfoot-fractures%2Ftreatment-recommendations%2Fcalcaneus-injuries , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhindfoot-fractures%2Ftreatment-recommendations%2Ftalus-fractures , https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-and-foot-fractures%2Ftreatment-recommendations%2Fsurgical-considerations",
    "odg-link": ""
  }
}
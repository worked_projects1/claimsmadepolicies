{
  "treatment": {
    "index-name": "glucocorticosteroid-injections",
    "label-name": "Glucocorticosteroid Injections",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/21/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-achilles-tendinopathy",
        "label-name": "Acute Achilles Tendinopathy"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Low-dose glucocorticosteroid injections are not recommended for treatment of acute, subacute, or post-operative Achilles tendinopathy.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: One moderate-quality placebo-controlled RCT evaluating up to 3 triamcinolone injections under ultrasound guidance for treatment of Achilles tendinopathy,(68) found evidence of short-term benefit. It is unclear if ultrasound guidance is necessary as the tissue is palpable. A second study found lack of efficacy.(69) Glucocorticosteroid injections are invasive, have a low adverse effect profile as a single low-dose injection, and are moderately costly. They are recommended as a treatment for select patients, after more conservative treatments have been attempted and found insufficient."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-achilles-tendinopathy",
        "label-name": "Subacute Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is the patient in in the late subacute stage, has moderate to severe achilles tendinopathy, and has failed other treatments including exercise and NSAIDs?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "Not Recommended. Low-dose glucocorticosteroid injections are not recommended for treatment of acute, subacute, or post-operative Achilles tendinopathy.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: One moderate-quality placebo-controlled RCT evaluating up to 3 triamcinolone injections under ultrasound guidance for treatment of Achilles tendinopathy,(68) found evidence of short-term benefit. It is unclear if ultrasound guidance is necessary as the tissue is palpable. A second study found lack of efficacy.(69) Glucocorticosteroid injections are invasive, have a low adverse effect profile as a single low-dose injection, and are moderately costly. They are recommended as a treatment for select patients, after more conservative treatments have been attempted and found insufficient.\n\nThere may be cases in the late subacute stage in which these injections may be appropriate if other treatments have failed; thus, there is overall no recommendation for patients in the subacute stage."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-achilles-tendinopathy-and-associated-paratendon-bursitis",
        "label-name": "Chronic Achilles Tendinopathy and Associated Paratendon Bursitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate or severe chronic achilles tendinopathy?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Have treatment with other interventions such as NSAIDs and exercises been attempted previously and either failed or results were unsatisfactory?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Low-dose glucocorticosteroid injections are recommended as an alternative therapy for treatment of chronic Achilles tendinopathy and associated paratendon bursitis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Moderate or severe chronic Achilles tendinopathy. Treatment with other interventions such as NSAIDs and exercises should have been attempted previously and either failed or results were unsatisfactory. There may be cases in the late subacute stage in which these injections may be appropriate if other treatments have failed; thus, there is overall no recommendation for patients in the subacute stage.\n\nFrequency/Dose/Duration: Up to 3 injections of triamcinolone 20mg over 3 weeks,(68) with 2nd and 3rd injections performed if the 1st does not yield complete relief, the problem continues to be incapacitating, conservative treatment options have been exhausted, and the patient understands and accepts that Achilles tendon rupture is possible and may necessitate surgery. Other glucocorticosteroids may be effective; however, one trial showed no effect of 1 methyl prednisolone injection(69) and quality trials with other glucocorticosteroids have not been reported.\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits.\n\nRationale: One moderate-quality placebo-controlled RCT evaluating up to 3 triamcinolone injections under ultrasound guidance for treatment of Achilles tendinopathy,(68) found evidence of short-term benefit. It is unclear if ultrasound guidance is necessary as the tissue is palpable. A second study found lack of efficacy.(69) Glucocorticosteroid injections are invasive, have a low adverse effect profile as a single low-dose injection, and are moderately costly. They are recommended as a treatment for select patients, after more conservative treatments have been attempted and found insufficient."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "morton’s-neuroma",
        "label-name": "Morton’s Neuroma"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Is pain and/or debility significant?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has changing shoe wear and/or orthotics failed to sufficiently control symptoms?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Glucocorticosteroid injections are recommended for Morton’s Neuroma.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications: select cases where pain and/or debility are significant and changing shoe wear, and/or orthotics fail to sufficiently control symptoms.\n\nRationale: There is one moderate-quality trial suggesting efficacy of a glucocorticoid injection at 3 months compared with placebo. Long-term results are unclear, and may well not be present. Still, up to 3 injections to attempt to reduce symptoms is a reasonable intervention to try before surgery. Ongoing injections are not recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-sprain",
        "label-name": "Acute Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of glucocorticosteroid injection for routine treatment of acute, subacute, or chronic ankle sprain.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality trials regarding glucocorticosteroid injection of ankle sprain. A low-quality trial found reduced skin temperature after local steroid injection compared to non-injected group, although there were no clinically significant differences.(411) Recommendations for local injection of glucocorticosteroid in musculoskeletal conditions (i.e. epicondylalgia, plantar fasciitis, and shoulder impingement) are limited to focal pathology. There is no description found for injection of ankle ligaments. Injections are minimally invasive, are of moderate cost, with no evidence of efficacy, and have a potential risk for further ligament weakening. Therefore, there is no recommendation for or against the use of steroid injection for the treatment of ankle sprain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-sprain",
        "label-name": "Subacute Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of glucocorticosteroid injection for routine treatment of acute, subacute, or chronic ankle sprain.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality trials regarding glucocorticosteroid injection of ankle sprain. A low-quality trial found reduced skin temperature after local steroid injection compared to non-injected group, although there were no clinically significant differences.(411) Recommendations for local injection of glucocorticosteroid in musculoskeletal conditions (i.e. epicondylalgia, plantar fasciitis, and shoulder impingement) are limited to focal pathology. There is no description found for injection of ankle ligaments. Injections are minimally invasive, are of moderate cost, with no evidence of efficacy, and have a potential risk for further ligament weakening. Therefore, there is no recommendation for or against the use of steroid injection for the treatment of ankle sprain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-sprain",
        "label-name": "Chronic Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of glucocorticosteroid injection for routine treatment of acute, subacute, or chronic ankle sprain.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality trials regarding glucocorticosteroid injection of ankle sprain. A low-quality trial found reduced skin temperature after local steroid injection compared to non-injected group, although there were no clinically significant differences.(411) Recommendations for local injection of glucocorticosteroid in musculoskeletal conditions (i.e. epicondylalgia, plantar fasciitis, and shoulder impingement) are limited to focal pathology. There is no description found for injection of ankle ligaments. Injections are minimally invasive, are of moderate cost, with no evidence of efficacy, and have a potential risk for further ligament weakening. Therefore, there is no recommendation for or against the use of steroid injection for the treatment of ankle sprain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-plantar-fasciitis",
        "label-name": "Acute Plantar Fasciitis"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Glucocorticosteroid injections are not recommended for treatment of acute or subacute plantar fasciitis.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-plantar-fasciitis",
        "label-name": "Subacute Plantar Fasciitis"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Glucocorticosteroid injections are not recommended for treatment of acute or subacute plantar fasciitis.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-plantar-fasciitis",
        "label-name": "Chronic Plantar Fasciitis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have  moderate or severe plantar fasciitis?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient failed satisfactory management with NSAIDs, stretching, and other exercise?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Glucocorticosteroid injections are recommended for short-term relief of chronic plantar fasciitis.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Moderate\n\nIndications: Moderate or severe plantar fasciitis, failed satisfactory management with NSAIDs, stretching, and other exercise.             \n\nFrequency/Dose/Duration: Quality trials have utilized hydrocortisone 25mg, triamcinolone 20mg, betamethasone 5.7mg, and prednisolone acetate 25mg.(256, 279, 301-303) (Blockey 56; Kalaci 09; Crawford 99; Kriss 03; Porter 05) The tenderest point is generally included in the injection. A 2nd injection may be performed if prior results unsatisfactory, the problem is incapacitating, other options have been exhausted, and the patient understands and accepts that rupture is a possible complication and will likely necessitate surgery.\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "tarsal-tunnel-syndrome-(tts)",
        "label-name": "Tarsal Tunnel Syndrome (TTS)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Glucocorticosteroid injections are recommended as part of a conservative management strategy for treatment of TTS.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nRationale: There are no quality studies evaluating the effects of corticosteroid injections in treating TTS. Injections are commonly reported as part of conservative therapy and as an additional mode for confirmation of suspected diagnosis of TSS. Corticosteroid injections are useful in other entrapment syndromes, particularly CTS (see Hand, Wrist, and Forearm Disorders guideline), and have been reported helpful for TTS. There are no data on recurrence or long-term benefits. This option is invasive, but low cost and has few side effects. Thus, if a more conservative treatment strategy fails to improve the condition, glucocorticosteroid injections may be useful. There is no quality information on the frequency or number of injections (see CTS in Hand, Wrist, and Forearm Disorders guideline)."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-tendinosis",
        "label-name": "Acute Ankle Tendinosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have ankle symptoms of pain over a compartment?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient had generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Strength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Ankle symptoms of pain over a compartment. Generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment. It is reasonable to treat cases with an initial injection although there is no quality evidence to support that approach. Failure or suboptimal results with an initial injection result in a need for additional injection(s) in a minority of patients which is (are) usually successful. (Anderson 91; Sakai 02; Peters-Veluthamaningal 09b)\n\nFrequency/Dose/Duration: Optimal dose is unknown. Studies in the wrist have utilized methylprednisolone acetate 40mg, (Anderson 91; Goldfarb 07; Witt 91) and triamcinolone acetonide 10mg. (Sakai 02; Peters-Veluthamaningal 09b) An adjuvant injectable anesthetic is typically used. (Anderson 91; Sakai 02; Jirarattanaphochai 04) It is recommended that a single injection be scheduled and the results evaluated to document improvement. (Peters-Veluthamaningal 09b) Failure of a response within 1-2 weeks should result in reanalysis of the diagnosis and consideration of repeat injection. (Peters-Veluthamaningal 09b) Recurrence of symptoms months later should result in consideration of re-injection. (Anderson 91; Lapidus 72) While there is no evidence-based maximum number of injections to treat an episode or over a lifetime, more than 3 injections in a year should be avoided due to tendon weakening and risk of rupture. Recurring injections on a year after year basis should also be similarly avoided.\n\nIndications for Discontinuation: If a partial response, consideration should be given to repeating the injection, typically at a modestly higher dose.\n\nRationale: There are no quality studies that address glucocorticosteroid injections for ankle tendinosis. By analogy, there is one moderate-quality study comparing glucocorticosteroid injections with placebo for treatment of de Quervain’s stenosing tenosynovitis. (Peters-Veluthamaningal 09b) The trial showed considerable benefits from active treatment that persisted for 12 months and allows for an evidence-based recommendation. Another high-quality trial found no additive benefit of NSAID in addition to injection to prevent recurrence but did not assess reductions in pain immediately after injection thus appears to have no bearing on use of NSAIDs for those purposes. (Jirarattanaphochai 04) (A low-quality trial found glucocorticosteroid injection superior to splinting in pregnant and lactating females. (Avci 02)) These injections are minimally invasive, have low adverse effects and are low to moderate cost. Thus, they are recommended to treat ankle tendinosis."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-tendinosis",
        "label-name": "Subacute Ankle Tendinosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have ankle symptoms of pain over a compartment?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient had generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Strength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Ankle symptoms of pain over a compartment. Generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment. It is reasonable to treat cases with an initial injection although there is no quality evidence to support that approach. Failure or suboptimal results with an initial injection result in a need for additional injection(s) in a minority of patients which is (are) usually successful. (Anderson 91; Sakai 02; Peters-Veluthamaningal 09b)\n\nFrequency/Dose/Duration: Optimal dose is unknown. Studies in the wrist have utilized methylprednisolone acetate 40mg, (Anderson 91; Goldfarb 07; Witt 91) and triamcinolone acetonide 10mg. (Sakai 02; Peters-Veluthamaningal 09b) An adjuvant injectable anesthetic is typically used. (Anderson 91; Sakai 02; Jirarattanaphochai 04) It is recommended that a single injection be scheduled and the results evaluated to document improvement. (Peters-Veluthamaningal 09b) Failure of a response within 1-2 weeks should result in reanalysis of the diagnosis and consideration of repeat injection. (Peters-Veluthamaningal 09b) Recurrence of symptoms months later should result in consideration of re-injection. (Anderson 91; Lapidus 72) While there is no evidence-based maximum number of injections to treat an episode or over a lifetime, more than 3 injections in a year should be avoided due to tendon weakening and risk of rupture. Recurring injections on a year after year basis should also be similarly avoided.\n\nIndications for Discontinuation: If a partial response, consideration should be given to repeating the injection, typically at a modestly higher dose.\n\nRationale: There are no quality studies that address glucocorticosteroid injections for ankle tendinosis. By analogy, there is one moderate-quality study comparing glucocorticosteroid injections with placebo for treatment of de Quervain’s stenosing tenosynovitis. (Peters-Veluthamaningal 09b) The trial showed considerable benefits from active treatment that persisted for 12 months and allows for an evidence-based recommendation. Another high-quality trial found no additive benefit of NSAID in addition to injection to prevent recurrence but did not assess reductions in pain immediately after injection thus appears to have no bearing on use of NSAIDs for those purposes. (Jirarattanaphochai 04) (A low-quality trial found glucocorticosteroid injection superior to splinting in pregnant and lactating females. (Avci 02)) These injections are minimally invasive, have low adverse effects and are low to moderate cost. Thus, they are recommended to treat ankle tendinosis."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-tendinosis",
        "label-name": "Chronic Ankle Tendinosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have ankle symptoms of pain over a compartment?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the patient had generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Strength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Ankle symptoms of pain over a compartment. Generally at least 1 week of non-invasive treatment to determine if condition will resolve without invasive treatment. It is reasonable to treat cases with an initial injection although there is no quality evidence to support that approach. Failure or suboptimal results with an initial injection result in a need for additional injection(s) in a minority of patients which is (are) usually successful. (Anderson 91; Sakai 02; Peters-Veluthamaningal 09b)\n\nFrequency/Dose/Duration: Optimal dose is unknown. Studies in the wrist have utilized methylprednisolone acetate 40mg, (Anderson 91; Goldfarb 07; Witt 91) and triamcinolone acetonide 10mg. (Sakai 02; Peters-Veluthamaningal 09b) An adjuvant injectable anesthetic is typically used. (Anderson 91; Sakai 02; Jirarattanaphochai 04) It is recommended that a single injection be scheduled and the results evaluated to document improvement. (Peters-Veluthamaningal 09b) Failure of a response within 1-2 weeks should result in reanalysis of the diagnosis and consideration of repeat injection. (Peters-Veluthamaningal 09b) Recurrence of symptoms months later should result in consideration of re-injection. (Anderson 91; Lapidus 72) While there is no evidence-based maximum number of injections to treat an episode or over a lifetime, more than 3 injections in a year should be avoided due to tendon weakening and risk of rupture. Recurring injections on a year after year basis should also be similarly avoided.\n\nIndications for Discontinuation: If a partial response, consideration should be given to repeating the injection, typically at a modestly higher dose.\n\nRationale: There are no quality studies that address glucocorticosteroid injections for ankle tendinosis. By analogy, there is one moderate-quality study comparing glucocorticosteroid injections with placebo for treatment of de Quervain’s stenosing tenosynovitis. (Peters-Veluthamaningal 09b) The trial showed considerable benefits from active treatment that persisted for 12 months and allows for an evidence-based recommendation. Another high-quality trial found no additive benefit of NSAID in addition to injection to prevent recurrence but did not assess reductions in pain immediately after injection thus appears to have no bearing on use of NSAIDs for those purposes. (Jirarattanaphochai 04) (A low-quality trial found glucocorticosteroid injection superior to splinting in pregnant and lactating females. (Avci 02)) These injections are minimally invasive, have low adverse effects and are low to moderate cost. Thus, they are recommended to treat ankle tendinosis."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendinopathy%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ffoot-neuroma%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-sprain%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ftts%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ftenosynovitis%2Finjections",
    "odg-link": ""
  }
}
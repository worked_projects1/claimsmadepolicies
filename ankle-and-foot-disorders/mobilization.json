{
  "treatment": {
    "index-name": "mobilization",
    "label-name": "Mobilization",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/22/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-compartment-tenosynovitis",
        "label-name": "Acute Ankle Compartment Tenosynovitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation.\n\nAllied Health Interventions\n\nThere is no recommendation for or against the use of other non-operative interventions (i.e., manipulation and mobilization, massage, deep friction massage, or acupuncture) for the treatment of acute, subacute, or chronic ankle tenosynovitis as other interventions have proven efficacy and are preferentially indicated for initial and subsequent treatment options.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies evaluating other non-operative interventions for ankle tenosynovitis. Other treatments have evidence of efficacy for treatment of the wrist and thus they are recommended by analogy."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-compartment-tenosynovitis",
        "label-name": "Subacute Ankle Compartment Tenosynovitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation.\n\nAllied Health Interventions\n\nThere is no recommendation for or against the use of other non-operative interventions (i.e., manipulation and mobilization, massage, deep friction massage, or acupuncture) for the treatment of acute, subacute, or chronic ankle tenosynovitis as other interventions have proven efficacy and are preferentially indicated for initial and subsequent treatment options.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies evaluating other non-operative interventions for ankle tenosynovitis. Other treatments have evidence of efficacy for treatment of the wrist and thus they are recommended by analogy."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-compartment-tenosynovitis",
        "label-name": "Chronic Ankle Compartment Tenosynovitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation.\n\nAllied Health Interventions\n\nThere is no recommendation for or against the use of other non-operative interventions (i.e., manipulation and mobilization, massage, deep friction massage, or acupuncture) for the treatment of acute, subacute, or chronic ankle tenosynovitis as other interventions have proven efficacy and are preferentially indicated for initial and subsequent treatment options.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies evaluating other non-operative interventions for ankle tenosynovitis. Other treatments have evidence of efficacy for treatment of the wrist and thus they are recommended by analogy."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ankle-sprain",
        "label-name": "Acute Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of manipulation or mobilization for the treatment of acute or subacute ankle sprain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ankle-sprain",
        "label-name": "Subacute Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of manipulation or mobilization for the treatment of acute or subacute ankle sprain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ankle-sprain",
        "label-name": "Chronic Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of manipulation or mobilization for the treatment of chronic recurrent ankle sprain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There is one moderate-quality trial that compared the addition of passive talocrural joint mobilization to a RICE protocol for acute ankle sprain.(516) The mobilization group demonstrated improvement in dorsiflexion range of motion. However, there is no correlation of improvement to other outcomes such as lost workdays, return to work, or return to sports or normal walking measures, making this finding of uncertain clinical significance. Another moderate-quality trial comparing a single session of mobilization plus movement with “no treatment” for subacute ankle sprain demonstrated immediate improvement of talocrural dorsiflexion.(545) There were no other long-term or clinical outcomes reported, making the clinical significance of improved dorsiflexion and the intervention of unknown benefit. A high-quality cross-over trial comparing mobilization plus movement with “no treatment with or no weight bearing” also demonstrated improved talocrural movement, but conclusions of clinical utility are again limited.(550) An experimental trial of single intervention found limited evidence of potential efficacy, but no intermediate or long-term results. (Yeo 11) A moderate-quality trial found minimal additive benefit of myofascial techniques to manipulation/mobilization. (Truyols-Dominguez 13) Manipulation is not invasive, is moderately costly, but may have adverse effects. There is no recommendation for or against manipulation of the ankle and foot joints for acute, subacute, or chronic ankle sprain as there is an absence of quality evidence."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "hallux-valgus-(bunions)",
        "label-name": "Hallux Valgus (Bunions)"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of manipulation or mobilization for treatment of hallux valgus.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no sham-controlled studies. A pragmatic comparative trial found no difference between manual manipulative treatment and a night splint at 1 week, although better outcomes were reported at 1 month and sustainability was not reported. (du Plessis 11) Thus, there is no recommendation."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Ftenosynovitis%2Fallied-health-interventions,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-sprain%2Ftreatment-recommendations%2Fallied-health-interventions,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fbunions%2Ftreatment-recommendations%2Fallied-health-interventions",
    "odg-link": ""
  }
}
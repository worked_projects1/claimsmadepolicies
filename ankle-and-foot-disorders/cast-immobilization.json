{
  "treatment": {
    "index-name": "cast-immobilization",
    "label-name": "Cast Immobilization",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/20/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-mild-to-moderate-ankle-sprain",
        "label-name": "Acute Mild to Moderate Ankle Sprain"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Immobilization by cast is not recommended for patients with acute mild to moderate ankle sprain as splints should be sufficient.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "cast-immobilization-for-severe-ankle-sprain",
        "label-name": "Cast Immobilization for Severe Ankle Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of immobilization by cast.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nFrequency/Dose/Duration: Application of a sugar-tong splint for 10 days to 3 weeks after a 48-hour period of elevation and non-weight bearing.\n\n\nRationale: There are five quality trials that compared early mobilization with cast immobilization (see Table in Immobilization).(386, 488-494) There is evidence early mobilization provides short-term benefit in functional improvement, pain, and return to work. There are no trials demonstrating a negative treatment effect long-term for acute, moderate, or severe sprain injuries. Therefore, early mobilization is recommended over immobilization for most patients (see Immobilization for additional discussion).\n\n\n\nThere are no quality trials for casting of mild ankle sprains. Mild acute sprains are generally self-limited and respond well to early mobilization and other therapies; therefore, casting is not recommended. There are six quality trials that compared casting with early mobilization for moderate and severe acute ankle sprains. The moderate-quality CAST trial,(386, 488, 489) demonstrated below-the-knee casting of moderate and severe sprains for 10 days provided a statistical, but clinically indeterminate, short-term benefit compared with tubular bandage (control) in subjective ratings of pain, function, and symptoms in the first 12 weeks. There were no differences between casting and bracing however, and by 9 months there were no differences between groups. A moderate-quality study demonstrated short-term improvement in pain, swelling, and range of motion in the early mobilization group compared to casting for moderate and severe sprains, but did not demonstrate any long-term benefit of one over the other.(490, 491) Another moderate-quality trial demonstrated casting was less beneficial for moderate (Grade II) sprains, but was equivocal for severe sprains compared with bracing.(492) A moderate-quality trial limited to severe ankle sprains demonstrated early mobilization resulted in short-term benefit in swelling, pain, stiffness and early return to sport compared with 3-week casting.(493) There were no long-term differences measured at 12 months. Another moderate-quality trial demonstrated early mobilization after a 48-hour non-weight-bearing period provided subjective improvement in pain perception at 3 weeks, but no differences in improvement of swelling, residual pain, and function compared with casting.(494) However, early mobilization resulted in much faster return to full duty. No long-term differences were demonstrated in any of the quality trials.\n\n\n\nCasting is non-invasive, but is restrictive of activity, including return to work, impairs driving performance more than bracing,(509) (Tremblay 09) and is associated with risk for deep venous thrombosis.(488) (Lamb 09) Total direct and indirect costs of casting based on the U.K. health care system are similar to the use of Aircast brace and more cost-effective than compression wrap.(386) (Cooke 09) Cast immobilization is therefore not recommended as an alternative to splinting and early mobilization treatment for severe and moderate ankle sprain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "ankle-fracture",
        "label-name": "Ankle Fracture"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Moderately Recommended. Cast immobilization is moderately recommended for the management of ankle fractures.\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nLevel of Confidence – High\n\nIndications: All ankle fractures.\n\n \n\nFrequency/Dose/Duration: Immobilization generally for 6 to 8 weeks.(754-762) \n\n \n\nRationale: There are 9 moderate-quality trials that compared cast immobilization to other methods, including orthosis, removable cast, and splint with and without early motion and early weight-bearing that demonstrated equivocal results.(754-762) Cast immobilization is recommended for all patients and the use is dependent on physician and patient preference."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "tibial-shaft-fractures-(closed,-diaphyseal)",
        "label-name": "Tibial Shaft Fractures (Closed, Diaphyseal)"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against non-operative management of tibial shaft fractures.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There is one moderate-quality trial comparing management of tibial shaft fracture using operative fixation using intramedullary nail to plaster cast. The plaster cast group was separated into those whose fractures were not displaced and a cast alone would suffice, and those whose fracture was displaced and underwent internal fixation using cerclage or screw in addition to the plaster cast.(720) (Karladani 00) The study demonstrated significantly reduced time to fracture union time and weight bearing for the group without the cast. The cast-only and cast-with-minimal internal-fixation groups both demonstrated 50% failure requiring ORIF for non-union. As the intramedullary nail appears to result in quicker healing time and return to activity, it is recommended. This study indicates that patients should be counseled on the likelihood of knee pain long-term (44% of subjects). Casting may be an alternative for some patients, but with counseling that nearly half may need surgical intervention for delayed union.\n\n\n\nThere are no quality trials of one type of surgical fixation compared with another. A low-quality trial demonstrated plates to provide faster healing time compared with intramedullary nail.(721) (Fernandes 06) Described techniques include use of an intramedullary nail, external fixators, or plates and screws, and there is no recommendation of one over another, with use based on surgeon preference.(720-722) There appears to be no advantage to minimal fixation with screw or cerclage compared with casting.(720)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "distal-tibial-extra-articular-fractures",
        "label-name": "Distal Tibial Extra-articular Fractures"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a closed simple fracture with initial shortening <15mm, angular deformity after initial manipulation <5° in any plane?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Non-operative management is recommended in select circumstances for distal extra-articular tibial fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Moderate\n\nIndications: Closed simple fractures with initial shortening <15mm, angular deformity after initial manipulation <5° in any plane.(723) (Zelle 06)\n\nRationale: There are no quality trials comparing operative to non-operative treatment for distal extra-articular tibia fracture. A systematic review of 1,125 fractures demonstrated a low non-union rate for immobilization of 1.3 vs. 5.5% for intramedullary nailing, although there were more open fractures in the nailing group.(723) (Zelle 06) There is one moderate-quality trial comparing the use of intramedullary nail with percutaneous locking compression plates,(724) (Guo 10) and another moderate-quality trial comparing intramedullary nail with fracture plate and screws.(725) (Im 05) There were no significant differences in functional outcomes between these interventions. Intramedullary nail was demonstrated to have few superficial infections and less angulation than plates and screws,(725) (Im 05) and shorter operating time and radiation exposure than percutaneous compression plate.(724) (Guo 10) However, there is no quality evidence that one technique is superior to the other and no recommendation is made regarding technique."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "calcaneus-fracture",
        "label-name": "Calcaneus Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a non-displaced fracture, displaced extra-articular fracture, or displaced intra-articular fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Non-operative cast immobilization is recommended for select calcaneus fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Non-displaced fracture, displaced extra-articular, displaced intra-articular."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-sprain%2Ftreatment-recommendations%2Fdevices,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-and-foot-fractures%2Ftreatment-recommendations%2Fnonoperative-management,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhindfoot-fractures%2Ftreatment-recommendations%2Fcalcaneus-injuries",
    "odg-link": ""
  }
}
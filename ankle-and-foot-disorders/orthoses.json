{
  "treatment": {
    "index-name": "orthoses",
    "label-name": "Orthoses",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/20/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "charcot-joint-(neurogenic-arthropathy)",
        "label-name": "Charcot Joint (Neurogenic Arthropathy)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Splints, walking braces, orthoses and casts (deSouza 08) should be tailored to the specific cause-condition and are Recommended, Insufficient Evidence (I), Level of Confidence – Low."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "prevention-of-plantar-fasciitis",
        "label-name": "Prevention of Plantar Fasciitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of orthotic devices for the prevention of plantar fasciitis or lower extremity disorders.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There is one high-quality trial comparing custom and pre-fabricated orthoses with sham orthoses for treatment efficacy of plantar fasciitis.(216) (Landorf 06) In 136 patients with clinical plantar fasciitis, modest functional improvement at 3 months was demonstrated in both orthoses groups compared to sham, but the comparative improvement at 3 months was within the range of the clinical measuring method (Foot Health Status Questionnaire) ability to detect differences (intraclass correlation coefficients range, 0.74 to 0.92, and Cronbach α, 0.85 to 0.88), and the statistically significant effect disappeared at 12 months. There were no differences in symptom relief at 3 or 12 months. Thus, there is limited evidence for short-term functional benefit from the use of orthoses and no evidence of long-term benefit. (A low-quality crossover trial of orthoses for symptom relief of metatarsalgia related to rheumatoid arthritis demonstrated pain relief but no improvement in function.(217))\n\nThere is one high-quality and three moderate-quality studies that compared custom-made orthoses to other prefabricated orthoses.(216, 218-220) (Landorf 06, Baldassin 09, Pfeffer 99, Kelly 98) Despite advantages in pressure redistribution achieved with custom orthoses,(220) (Kelly 98) there were no advantages demonstrated in clinical outcomes including symptom relief at 8 weeks from the use of custom orthoses over prefabricated orthoses,(218, 219) (Baldassin 09, Pfeffer 99) or at 3 and 12 months.(216) (Landorf 06) However, patients with unusual foot anatomy may require custom-made orthoses.\n\nOne issue with some of the comparison trials is that custom-made and prefabricated orthoses may use different materials.(216, 220) (Landorf 06, Kelly 98) Thus, the comparison is made of both production method and material of the orthotic. In one trial, both custom-made and prefabricated orthoses were made of the same material and showed similar effectiveness.(218) (Baldassin 09) Material characteristics such elasticity (ratio of force/unit area to fractional change in height) and thicknesses of the orthotics were usually not specified. In comparison with other treatments, orthoses were demonstrated to be equivalent in efficacy to night splints,(212) (Roos 06) supportive shoes,(221) (Chalmers 00) Achilles and plantar stretching exercises,(219) (Pfeffer 99) electrical stimulation(222) (Stratton 09) and in a low-quality study, the airheel device.(223) (Kavros 05)\n\nThere is one moderate-quality trial for orthotics and prevention of lower extremity disorders, which did not demonstrate benefit from using orthotics in a military population.(224) (Esterman 05) However, the study had multiple weaknesses, including low compliance making inference difficult to the general population. A low-quality randomized trial found demonstrated benefit in reducing acute leg and foot pain in referees during a tournament from the use of heel cups.(225) (Fauno 93)\n\nThus, the use of orthotic devices may provide some short-term benefit, but is not likely to result in dramatic improvements over natural healing. These devices are non-invasive, have few adverse effects, and are generally low cost for devices that are not custom-made; therefore, they are recommended. Custom orthoses also appear to have modest efficacy; however, there is no demonstrable improvement compared to other, commercially available orthoses, yet costs are higher. Thus, there is no recommendation for or against custom orthoses. There is insufficient evidence for orthotics for prevention and therefore, there is no recommendation for or against their use."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "prevention-of-lower-extremity-disorders",
        "label-name": "Prevention of Lower Extremity Disorders"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of orthotic devices for the prevention of plantar fasciitis or lower extremity disorders.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There is one high-quality trial comparing custom and pre-fabricated orthoses with sham orthoses for treatment efficacy of plantar fasciitis.(216) (Landorf 06) In 136 patients with clinical plantar fasciitis, modest functional improvement at 3 months was demonstrated in both orthoses groups compared to sham, but the comparative improvement at 3 months was within the range of the clinical measuring method (Foot Health Status Questionnaire) ability to detect differences (intraclass correlation coefficients range, 0.74 to 0.92, and Cronbach α, 0.85 to 0.88), and the statistically significant effect disappeared at 12 months. There were no differences in symptom relief at 3 or 12 months. Thus, there is limited evidence for short-term functional benefit from the use of orthoses and no evidence of long-term benefit. (A low-quality crossover trial of orthoses for symptom relief of metatarsalgia related to rheumatoid arthritis demonstrated pain relief but no improvement in function.(217))\n\nThere is one high-quality and three moderate-quality studies that compared custom-made orthoses to other prefabricated orthoses.(216, 218-220) (Landorf 06, Baldassin 09, Pfeffer 99, Kelly 98) Despite advantages in pressure redistribution achieved with custom orthoses,(220) (Kelly 98) there were no advantages demonstrated in clinical outcomes including symptom relief at 8 weeks from the use of custom orthoses over prefabricated orthoses,(218, 219) (Baldassin 09, Pfeffer 99) or at 3 and 12 months.(216) (Landorf 06) However, patients with unusual foot anatomy may require custom-made orthoses.\n\nOne issue with some of the comparison trials is that custom-made and prefabricated orthoses may use different materials.(216, 220) (Landorf 06, Kelly 98) Thus, the comparison is made of both production method and material of the orthotic. In one trial, both custom-made and prefabricated orthoses were made of the same material and showed similar effectiveness.(218) (Baldassin 09) Material characteristics such elasticity (ratio of force/unit area to fractional change in height) and thicknesses of the orthotics were usually not specified. In comparison with other treatments, orthoses were demonstrated to be equivalent in efficacy to night splints,(212) (Roos 06) supportive shoes,(221) (Chalmers 00) Achilles and plantar stretching exercises,(219) (Pfeffer 99) electrical stimulation(222) (Stratton 09) and in a low-quality study, the airheel device.(223) (Kavros 05)\n\nThere is one moderate-quality trial for orthotics and prevention of lower extremity disorders, which did not demonstrate benefit from using orthotics in a military population.(224) (Esterman 05) However, the study had multiple weaknesses, including low compliance making inference difficult to the general population. A low-quality randomized trial found demonstrated benefit in reducing acute leg and foot pain in referees during a tournament from the use of heel cups.(225) (Fauno 93)\n\nThus, the use of orthotic devices may provide some short-term benefit, but is not likely to result in dramatic improvements over natural healing. These devices are non-invasive, have few adverse effects, and are generally low cost for devices that are not custom-made; therefore, they are recommended. Custom orthoses also appear to have modest efficacy; however, there is no demonstrable improvement compared to other, commercially available orthoses, yet costs are higher. Thus, there is no recommendation for or against custom orthoses. There is insufficient evidence for orthotics for prevention and therefore, there is no recommendation for or against their use."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcharcot-joint,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Fdevices",
    "odg-link": ""
  }
}
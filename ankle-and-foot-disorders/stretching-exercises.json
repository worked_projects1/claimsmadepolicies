{
  "treatment": {
    "index-name": "stretching-exercises",
    "label-name": "Stretching Exercises",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/21/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-achilles-tendinopathy",
        "label-name": "Acute Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe acute achilles tendinosis?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Stretching and loading exercises, particularly eccentric exercises, are recommended for the treatment of acute, subacute, or post-operative Achilles tendinopathy.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Mild, moderate, or severe acute, subacute and post-operative Achilles tendinosis. \n\nFrequency/Dose/Duration: One or 2 sets of exercises per day until symptom resolution and generally 1 or 2 appointments for exercise instruction (an additional 1 or 2 appointments for reinforcement is often needed in more chronic cases). Data suggest more intense exercise regimens result in superior outcomes.(46) Post-operative patients may require additional instruction during the recovery period.\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits.\n\nRationale: Two moderate-quality studies compared more intense to less intense exercise(46) or exercise to “active rest”(48) for treatment of chronic Achilles tendinopathy. There was no difference between the effects of more intense and less intense exercise.(46) Eccentric exercises were found superior to concentric exercises.(25) (A low-quality study found eccentric exercises to have a better outcome over concentric exercises.)(49) There is one high-quality study comparing eccentric exercise with non-intervention and with shockwave therapy (Rompe 07) that found exercise and shockwave therapy both superior to observation.(47) However, the equivalence of exercise to shock wave therapy was not reproducible,(50) challenging the reproducibility and integrity of the study findings. Additionally, in these studies, the uncertainty due to the instruments used to measure outcome(51) was not addressed, with the differences in findings based primarily on statistics and without fully considering the variability introduced by the clinical measurement.\n\nThere are no quality studies of exercise for treatment of acute, subacute, or post-operative Achilles pain. There are many additional studies that included exercise as part of the treatment, but did not have adequate controls to demonstrate the effects of exercise. Studies comparing exercise to other interventions generally used eccentric exercises. Stretching exercises and graded activity does not appear to differ in effect(48), suggesting that allowing patients to engaging in activities according to their comfort level does not worsen outcome.\n\nExercise is non-invasive, has few adverse effects, may benefit the individual’s overall health compared to inactivity, and is not costly when self-administered. Exercise may be taught quickly by providers or therapists and is moderately recommended. For acute pain, there is a lack of evidence for effectiveness, but it is reasonable to infer that this intervention may be beneficial. Post-operative patients may benefit from a few additional supervised visits to help guide exercise and activity levels."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-achilles-tendinopathy",
        "label-name": "Subacute Achilles Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have mild, moderate, or severe subacute achilles tendinosis?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Stretching and loading exercises, particularly eccentric exercises, are recommended for the treatment of acute, subacute, or post-operative Achilles tendinopathy.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications: Mild, moderate, or severe acute, subacute and post-operative Achilles tendinosis. \n\nFrequency/Dose/Duration: One or 2 sets of exercises per day until symptom resolution and generally 1 or 2 appointments for exercise instruction (an additional 1 or 2 appointments for reinforcement is often needed in more chronic cases). Data suggest more intense exercise regimens result in superior outcomes.(46) Post-operative patients may require additional instruction during the recovery period.\n\nIndications for Discontinuation: Resolution, intolerance, adverse effects, or lack of benefits.\n\nRationale: Two moderate-quality studies compared more intense to less intense exercise(46) or exercise to “active rest”(48) for treatment of chronic Achilles tendinopathy. There was no difference between the effects of more intense and less intense exercise.(46) Eccentric exercises were found superior to concentric exercises.(25) (A low-quality study found eccentric exercises to have a better outcome over concentric exercises.)(49) There is one high-quality study comparing eccentric exercise with non-intervention and with shockwave therapy (Rompe 07) that found exercise and shockwave therapy both superior to observation.(47) However, the equivalence of exercise to shock wave therapy was not reproducible,(50) challenging the reproducibility and integrity of the study findings. Additionally, in these studies, the uncertainty due to the instruments used to measure outcome(51) was not addressed, with the differences in findings based primarily on statistics and without fully considering the variability introduced by the clinical measurement.\n\nThere are no quality studies of exercise for treatment of acute, subacute, or post-operative Achilles pain. There are many additional studies that included exercise as part of the treatment, but did not have adequate controls to demonstrate the effects of exercise. Studies comparing exercise to other interventions generally used eccentric exercises. Stretching exercises and graded activity does not appear to differ in effect(48), suggesting that allowing patients to engaging in activities according to their comfort level does not worsen outcome.\n\nExercise is non-invasive, has few adverse effects, may benefit the individual’s overall health compared to inactivity, and is not costly when self-administered. Exercise may be taught quickly by providers or therapists and is moderately recommended. For acute pain, there is a lack of evidence for effectiveness, but it is reasonable to infer that this intervention may be beneficial. Post-operative patients may benefit from a few additional supervised visits to help guide exercise and activity levels."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-plantar-fasciitis",
        "label-name": "Acute Plantar Fasciitis"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Stretching exercises of the plantar fascia and Achilles tendon are recommended for treatment of plantar fasciitis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, or chronic plantar fasciitis.\n\nFrequency/Dose/Duration: Ten-minute stretches 3 times a day; no limit identified for duration.\n\nIndications for Discontinuation: Resolution, adverse effects, intolerance, non-compliance.\n\nRationale: There is one moderate-quality trial with a 2-year follow-up report comparing plantar fascia stretching with Achilles stretching exercises.(230, 231) (DiGiovanni 03; DiGiovanni 06) Heel pain in patients with chronic plantar pain who failed other conservative measures improved significantly with plantar fascia stretching exercises after 8 weeks of treatment compared with the Achilles stretching group. Stretching improved the subjects’ reported pain but did not improve reported function to a statistically-significant level. Those in the Achilles group were crossed over to plantar stretching, and improved significantly over a 2-year period, similar to the first group. There was not a “no treatment” group to compare natural healing. Another moderate-quality trial comparing stretching to calcaneal taping, sham taping, and no treatment over a 1-week period found no benefit from gastrocnemius and plantar fascia stretching.(232) (Hyland 06) However, this study was limited to 1 week of treatment and follow-up. One moderate-quality study used stretching as a treatment arm to compare efficacy of orthotic interventions.(219) (Pfeffer 99) The stretching arm was as beneficial as a felt insert and custom orthosis. Another trial showed no statistically significant improvements between intervention (Achilles tendon-calf muscle stretching and sham ultrasound) and control (sham ultrasound only) groups after its 2-week period.(233) (Radford 07) Three trials offered a comparison between stretching and no stretching(219, 232, 233) (Hyland 06, Radford 07, Pfeffer 99) without comparative benefit of stretching to the alternative treatment demonstrated in any of the trials. Two of the trials had participants who stretched stretch the plantar fascia(219, 232) (Hyland 06, Pfeffer 99) and one did not.(233) (Radford 07) Stretching is non-invasive, has no adverse effects, is self-administered and is of low cost, but has minimal evidence of efficacy. Given its low risk and cost, stretching is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-plantar-fasciitis",
        "label-name": "Subacute Plantar Fasciitis"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Stretching exercises of the plantar fascia and Achilles tendon are recommended for treatment of plantar fasciitis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, or chronic plantar fasciitis.\n\nFrequency/Dose/Duration: Ten-minute stretches 3 times a day; no limit identified for duration.\n\nIndications for Discontinuation: Resolution, adverse effects, intolerance, non-compliance.\n\nRationale: There is one moderate-quality trial with a 2-year follow-up report comparing plantar fascia stretching with Achilles stretching exercises.(230, 231) (DiGiovanni 03; DiGiovanni 06) Heel pain in patients with chronic plantar pain who failed other conservative measures improved significantly with plantar fascia stretching exercises after 8 weeks of treatment compared with the Achilles stretching group. Stretching improved the subjects’ reported pain but did not improve reported function to a statistically-significant level. Those in the Achilles group were crossed over to plantar stretching, and improved significantly over a 2-year period, similar to the first group. There was not a “no treatment” group to compare natural healing. Another moderate-quality trial comparing stretching to calcaneal taping, sham taping, and no treatment over a 1-week period found no benefit from gastrocnemius and plantar fascia stretching.(232) (Hyland 06) However, this study was limited to 1 week of treatment and follow-up. One moderate-quality study used stretching as a treatment arm to compare efficacy of orthotic interventions.(219) (Pfeffer 99) The stretching arm was as beneficial as a felt insert and custom orthosis. Another trial showed no statistically significant improvements between intervention (Achilles tendon-calf muscle stretching and sham ultrasound) and control (sham ultrasound only) groups after its 2-week period.(233) (Radford 07) Three trials offered a comparison between stretching and no stretching(219, 232, 233) (Hyland 06, Radford 07, Pfeffer 99) without comparative benefit of stretching to the alternative treatment demonstrated in any of the trials. Two of the trials had participants who stretched stretch the plantar fascia(219, 232) (Hyland 06, Pfeffer 99) and one did not.(233) (Radford 07) Stretching is non-invasive, has no adverse effects, is self-administered and is of low cost, but has minimal evidence of efficacy. Given its low risk and cost, stretching is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-plantar-fasciitis",
        "label-name": "Chronic Plantar Fasciitis"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Stretching exercises of the plantar fascia and Achilles tendon are recommended for treatment of plantar fasciitis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Acute, subacute, or chronic plantar fasciitis.\n\nFrequency/Dose/Duration: Ten-minute stretches 3 times a day; no limit identified for duration.\n\nIndications for Discontinuation: Resolution, adverse effects, intolerance, non-compliance.\n\nRationale: There is one moderate-quality trial with a 2-year follow-up report comparing plantar fascia stretching with Achilles stretching exercises.(230, 231) (DiGiovanni 03; DiGiovanni 06) Heel pain in patients with chronic plantar pain who failed other conservative measures improved significantly with plantar fascia stretching exercises after 8 weeks of treatment compared with the Achilles stretching group. Stretching improved the subjects’ reported pain but did not improve reported function to a statistically-significant level. Those in the Achilles group were crossed over to plantar stretching, and improved significantly over a 2-year period, similar to the first group. There was not a “no treatment” group to compare natural healing. Another moderate-quality trial comparing stretching to calcaneal taping, sham taping, and no treatment over a 1-week period found no benefit from gastrocnemius and plantar fascia stretching.(232) (Hyland 06) However, this study was limited to 1 week of treatment and follow-up. One moderate-quality study used stretching as a treatment arm to compare efficacy of orthotic interventions.(219) (Pfeffer 99) The stretching arm was as beneficial as a felt insert and custom orthosis. Another trial showed no statistically significant improvements between intervention (Achilles tendon-calf muscle stretching and sham ultrasound) and control (sham ultrasound only) groups after its 2-week period.(233) (Radford 07) Three trials offered a comparison between stretching and no stretching(219, 232, 233) (Hyland 06, Radford 07, Pfeffer 99) without comparative benefit of stretching to the alternative treatment demonstrated in any of the trials. Two of the trials had participants who stretched stretch the plantar fascia(219, 232) (Hyland 06, Pfeffer 99) and one did not.(233) (Radford 07) Stretching is non-invasive, has no adverse effects, is self-administered and is of low cost, but has minimal evidence of efficacy. Given its low risk and cost, stretching is recommended."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendinopathy%2Ftreatment-recommendations%2Factivity-modification-and-exercise,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Factivity-modification-and-exercise",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "orthotic-devices",
    "label-name": "Orthotic Devices",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/27/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-plantar-heel-pain",
        "label-name": "Acute Plantar Heel Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Orthotic devices are recommended for treatment of acute, subacute, or chronic plantar heel pain.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications: All patients with plantar fasciitis.\n\nFrequency/Dose/Duration: Daily use for 2 to 3 months.\n\nIndications for Discontinuation: Resolution, adverse effects, non-compliance."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-plantar-heel-pain",
        "label-name": "Subacute Plantar Heel Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Orthotic devices are recommended for treatment of acute, subacute, or chronic plantar heel pain.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications: All patients with plantar fasciitis.\n\nFrequency/Dose/Duration: Daily use for 2 to 3 months.\n\nIndications for Discontinuation: Resolution, adverse effects, non-compliance."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-plantar-heel-pain",
        "label-name": "Chronic Plantar Heel Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Orthotic devices are recommended for treatment of acute, subacute, or chronic plantar heel pain.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nLevel of Confidence – Low\n\nIndications: All patients with plantar fasciitis.\n\nFrequency/Dose/Duration: Daily use for 2 to 3 months.\n\nIndications for Discontinuation: Resolution, adverse effects, non-compliance."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-achilles-tendinopathy",
        "label-name": "Acute Achilles Tendinopathy"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of orthotic devices such as, heel lifts, heel pads, or heel braces for treatment of acute, subacute, or chronic Achilles tendinopathy.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies comparing orthotics with non-interventional or control groups. A low-quality study comparing groups that used heel pads, molefoam pads, or no device found no difference in the use of these devices.(58) There is one moderate-quality study of one specific device; however, the study did not include a non-intervention group so improvement with intervention could not be differentiated from the natural course of the condition, failed to demonstrate superiority of splints to exercises, and splints provided no additive benefits when combined with exercises.(59) Capillary blood flow in Achilles paratenons(45) and tendons(60) of patients with Achilles tendinopathy who wore an AirCast AirHeel ankle splint was investigated with mixed results between the two studies in microcirculatory effects, but no clinical changes demonstrated in those who wore splints. These devices are usually non-invasive and low cost if not custom-made. Although they are often prescribed, there is insufficient evidence to support a recommendation for or against their use."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-achilles-tendinopathy",
        "label-name": "Subacute Achilles Tendinopathy"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of orthotic devices such as, heel lifts, heel pads, or heel braces for treatment of acute, subacute, or chronic Achilles tendinopathy.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies comparing orthotics with non-interventional or control groups. A low-quality study comparing groups that used heel pads, molefoam pads, or no device found no difference in the use of these devices.(58) There is one moderate-quality study of one specific device; however, the study did not include a non-intervention group so improvement with intervention could not be differentiated from the natural course of the condition, failed to demonstrate superiority of splints to exercises, and splints provided no additive benefits when combined with exercises.(59) Capillary blood flow in Achilles paratenons(45) and tendons(60) of patients with Achilles tendinopathy who wore an AirCast AirHeel ankle splint was investigated with mixed results between the two studies in microcirculatory effects, but no clinical changes demonstrated in those who wore splints. These devices are usually non-invasive and low cost if not custom-made. Although they are often prescribed, there is insufficient evidence to support a recommendation for or against their use."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-achilles-tendinopathy",
        "label-name": "Chronic Achilles Tendinopathy"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of orthotic devices such as, heel lifts, heel pads, or heel braces for treatment of acute, subacute, or chronic Achilles tendinopathy.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale: There are no quality studies comparing orthotics with non-interventional or control groups. A low-quality study comparing groups that used heel pads, molefoam pads, or no device found no difference in the use of these devices.(58) There is one moderate-quality study of one specific device; however, the study did not include a non-intervention group so improvement with intervention could not be differentiated from the natural course of the condition, failed to demonstrate superiority of splints to exercises, and splints provided no additive benefits when combined with exercises.(59) Capillary blood flow in Achilles paratenons(45) and tendons(60) of patients with Achilles tendinopathy who wore an AirCast AirHeel ankle splint was investigated with mixed results between the two studies in microcirculatory effects, but no clinical changes demonstrated in those who wore splints. These devices are usually non-invasive and low cost if not custom-made. Although they are often prescribed, there is insufficient evidence to support a recommendation for or against their use."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fplantar-heel-pain%2Ftreatment-recommendations%2Fdevices,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fachilles-tendinopathy%2Ftreatment-recommendations%2Fdevices",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "immobilization",
    "label-name": "Immobilization",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "ankle-and-foot-disorders",
    "label-name": "Ankle and Foot Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/21/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "distal-phalanx-fracture",
        "label-name": "Distal Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a closed, non-displaced or stable fracture after reduction, involving less than 25% of articular surface?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Immobilization is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Closed, non-displaced or stable after reduction, involves less than 25% of articular surface.(827) No established guides to the degree of acceptability of displacement, angulation, or rotation.\n\nComments: Closed reduction after digital or hematoma block; obtain post-reduction film, repeat at 1 and 6 weeks; splint toe with buddy tape to adjacent toe until non-tender (3 to 4 weeks). Additional immobilization with a post-operative shoe or cast-boot should be considered."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "middle-phalanx-fracture",
        "label-name": "Middle Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a closed, non-displaced or stable fracture after reduction, involving less than 25% of articular surface?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Immobilization is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Closed, non-displaced or stable after reduction, involves less than 25% of articular surface.(827) No established guides to the degree of acceptability of displacement, angulation, or rotation.\n\nComments: Closed reduction after digital or hematoma block; obtain post-reduction film, repeat at 1 and 6 weeks; splint toe with buddy tape to adjacent toe until non-tender (3 to 4 weeks). Additional immobilization with a post-operative shoe or cast-boot should be considered."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "proximal-phalanx-fracture",
        "label-name": "Proximal Phalanx Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a closed, non-displaced or stable fracture after reduction, involving less than 25% of articular surface?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Immobilization is recommended for treatment of select patients with distal, middle, or proximal phalanx fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Closed, non-displaced or stable after reduction, involves less than 25% of articular surface.(827) No established guides to the degree of acceptability of displacement, angulation, or rotation.\n\nComments: Closed reduction after digital or hematoma block; obtain post-reduction film, repeat at 1 and 6 weeks; splint toe with buddy tape to adjacent toe until non-tender (3 to 4 weeks). Additional immobilization with a post-operative shoe or cast-boot should be considered."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "mid-tarsus-sprain",
        "label-name": "Mid-Tarsus Sprain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a mild to moderate case without diastasis?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "For mild to moderate cases without diastasis, immobilization in a short-leg walking boot or cast for 4-6 weeks is Recommended, Insufficient Evidence (I), Level of Confidence – Moderate, (Nunley 02; Burroughs AFP 98; Granata 10; Myerson 08) with repeat x-rays and evaluation for stability at 2 weeks. (Myerson 08)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "non-displaced-ankle-fracture",
        "label-name": "Non-displaced Ankle Fracture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have a non-displaced distal fibula, lateral malleolar, and/or Pilon fracture? Or, does the patient have a select non-displaced medial malleolar fracture?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Non-operative management is recommended for the treatment of non-displaced and reduced stable ankle fractures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – High\n\nIndications: Non-displaced distal fibula, lateral malleolar, or Pilon fractures; select non-displaced medial malleolar fracture.\n\nComments: Cast or rigid orthotics.(709, 714)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fforefoot-and-midfoot-fractures%2Ftreatment-recommendations%2Fphalangeal-fractures,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmid-tarsus-pain,https://app.mdguidelines.com/acoem-section/acoem%2Fdisorders%2Fankle-and-foot-disorders%2Fdiagnostic-and-treatment-recommendations%2Fankle-and-foot-fractures%2Ftreatment-recommendations%2Fnonoperative-management",
    "odg-link": ""
  }
}
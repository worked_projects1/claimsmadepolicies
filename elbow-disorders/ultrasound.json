{
  "treatment": {
    "index-name": "ultrasound",
    "label-name": "Ultrasound",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "elbow-disorders",
    "label-name": "Elbow Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/31/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-lateral-epicondylalgia",
        "label-name": "Acute Lateral Epicondylalgia"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Can the patient not tolerate oral NSAIDs and exercise. Or, has the patient failed other treatments (e.g., insufficient pain relief with elbow straps and activity modification)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic lateral epicondylalgia.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: For acute, subacute, or chronic epicondylalgia patients; patients who cannot tolerate oral NSAIDs and exercise; or patients who fail other treatments (e.g., insufficient pain relief with elbow straps and activity modification) may be ideal candidates. Generally moderately to severely affected patients are thought to be better candidates. Overall effect of ultrasound appears modest, thus other interventions are recommended first, particularly exercise.(226)\n\nFrequency/Dose/Duration: Various regimens have been utilized in the quality studies. The two trials showing the most benefit utilized 10 to 12 treatments (1.0MHz, 1-2W/cm2 for 5 to 10 minutes per session) over 4 to 6 weeks.(112, 247) There are no comparative trials for different regimens.\n\nIndications for Discontinuation: Resolution of pain, intolerance, lack of efficacy or non-compliance.\n\nRationale: There are two high- and two moderate-quality sham-controlled trials that address ultrasound. The two high-quality trials(246, 248) both found ultrasound ineffective while the two moderate-quality trials found it effective.(112, 247) However, the two moderate-quality trials both had larger sample sizes. (However, these are both older trials. Thus, the score may understate the true quality of the trials.) There is quality evidence that exercise is superior to ultrasound.(226) There also is evidence ultrasound is superior to chiropractic care.(235) Four moderate-quality trials included ultrasound as a co-intervention, thus utility of ultrasound is unable to be assessed from these studies.(12, 195, 251, 252) Thus, there is overall evidence of a modest benefit from ultrasound. Ultrasound is not invasive, has few adverse effects, but is moderately costly. As the overall evidence is for a modest benefit, it is recommended particularly for patients who fail other interventions."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-lateral-epicondylalgia",
        "label-name": "Subacute Lateral Epicondylalgia"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Can the patient not tolerate oral NSAIDs and exercise. Or, has the patient failed other treatments (e.g., insufficient pain relief with elbow straps and activity modification)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic lateral epicondylalgia.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: For acute, subacute, or chronic epicondylalgia patients; patients who cannot tolerate oral NSAIDs and exercise; or patients who fail other treatments (e.g., insufficient pain relief with elbow straps and activity modification) may be ideal candidates. Generally moderately to severely affected patients are thought to be better candidates. Overall effect of ultrasound appears modest, thus other interventions are recommended first, particularly exercise.(226)\n\nFrequency/Dose/Duration: Various regimens have been utilized in the quality studies. The two trials showing the most benefit utilized 10 to 12 treatments (1.0MHz, 1-2W/cm2 for 5 to 10 minutes per session) over 4 to 6 weeks.(112, 247) There are no comparative trials for different regimens.\n\nIndications for Discontinuation: Resolution of pain, intolerance, lack of efficacy or non-compliance.\n\nRationale: There are two high- and two moderate-quality sham-controlled trials that address ultrasound. The two high-quality trials(246, 248) both found ultrasound ineffective while the two moderate-quality trials found it effective.(112, 247) However, the two moderate-quality trials both had larger sample sizes. (However, these are both older trials. Thus, the score may understate the true quality of the trials.) There is quality evidence that exercise is superior to ultrasound.(226) There also is evidence ultrasound is superior to chiropractic care.(235) Four moderate-quality trials included ultrasound as a co-intervention, thus utility of ultrasound is unable to be assessed from these studies.(12, 195, 251, 252) Thus, there is overall evidence of a modest benefit from ultrasound. Ultrasound is not invasive, has few adverse effects, but is moderately costly. As the overall evidence is for a modest benefit, it is recommended particularly for patients who fail other interventions."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-lateral-epicondylalgia",
        "label-name": "Chronic Lateral Epicondylalgia"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Can the patient not tolerate oral NSAIDs and exercise. Or, has the patient failed other treatments (e.g., insufficient pain relief with elbow straps and activity modification)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic lateral epicondylalgia.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications: For acute, subacute, or chronic epicondylalgia patients; patients who cannot tolerate oral NSAIDs and exercise; or patients who fail other treatments (e.g., insufficient pain relief with elbow straps and activity modification) may be ideal candidates. Generally moderately to severely affected patients are thought to be better candidates. Overall effect of ultrasound appears modest, thus other interventions are recommended first, particularly exercise.(226)\n\nFrequency/Dose/Duration: Various regimens have been utilized in the quality studies. The two trials showing the most benefit utilized 10 to 12 treatments (1.0MHz, 1-2W/cm2 for 5 to 10 minutes per session) over 4 to 6 weeks.(112, 247) There are no comparative trials for different regimens.\n\nIndications for Discontinuation: Resolution of pain, intolerance, lack of efficacy or non-compliance.\n\nRationale: There are two high- and two moderate-quality sham-controlled trials that address ultrasound. The two high-quality trials(246, 248) both found ultrasound ineffective while the two moderate-quality trials found it effective.(112, 247) However, the two moderate-quality trials both had larger sample sizes. (However, these are both older trials. Thus, the score may understate the true quality of the trials.) There is quality evidence that exercise is superior to ultrasound.(226) There also is evidence ultrasound is superior to chiropractic care.(235) Four moderate-quality trials included ultrasound as a co-intervention, thus utility of ultrasound is unable to be assessed from these studies.(12, 195, 251, 252) Thus, there is overall evidence of a modest benefit from ultrasound. Ultrasound is not invasive, has few adverse effects, but is moderately costly. As the overall evidence is for a modest benefit, it is recommended particularly for patients who fail other interventions."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-pronator-syndrome",
        "label-name": "Acute Pronator Syndrome"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-pronator-syndrome",
        "label-name": "Subacute Pronator Syndrome"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-pronator-syndrome",
        "label-name": "Chronic Pronator Syndrome"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-radial-nerve-entrapment-(including-radial-tunnel-syndrome)",
        "label-name": "Acute Radial Nerve Entrapment (Including Radial Tunnel Syndrome)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-radial-nerve-entrapment-(including-radial-tunnel-syndrome)",
        "label-name": "Subacute Radial Nerve Entrapment (Including Radial Tunnel Syndrome)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-radial-nerve-entrapment-(including-radial-tunnel-syndrome)",
        "label-name": "Chronic Radial Nerve Entrapment (Including Radial Tunnel Syndrome)"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-ulnar-neuropathies",
        "label-name": "Acute Ulnar Neuropathies"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient been given nocturnal splints and/ or other treatments and had an inadequate response?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic ulnar neuropathies.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Ulnar neuropathies that are sufficiently symptomatic to warrant treatment. Patients should generally be given nocturnal splints and had an inadequate response.     \n\nFrequency/Dose/Duration: The regimen in the highest quality study of CTS patients consisted of daily 15-minute sessions, 5 a week for 2 weeks, then twice a week for 5 more weeks; 1MHz with intensity 1.0W/cm2, pulsed mode duty cycle of 1:4 and transducer area of 5cm2.(484) Another successful regimen consisted of 15-minute sessions, 5 times a week for 3 weeks.(480)\n\nIndications for Discontinuation: Resolution, failure to objectively improve or intolerance.\n\nRationale: There are no quality trials for treatment of patients with ulnar neuropathies. However, there are trials for treatment of CTS that suggest modest benefit(480, 483-486) (see Hand, Wrist, and Forearm Disorders chapter). Thus, by analogy, ultrasound is recommended for select patients who have failed treatment with a nocturnal brace/splint or obtained insufficient benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-ulnar-neuropathies",
        "label-name": "Subacute Ulnar Neuropathies"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient been given nocturnal splints and/ or other treatments and had an inadequate response?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic ulnar neuropathies.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Ulnar neuropathies that are sufficiently symptomatic to warrant treatment. Patients should generally be given nocturnal splints and had an inadequate response.     \n\nFrequency/Dose/Duration: The regimen in the highest quality study of CTS patients consisted of daily 15-minute sessions, 5 a week for 2 weeks, then twice a week for 5 more weeks; 1MHz with intensity 1.0W/cm2, pulsed mode duty cycle of 1:4 and transducer area of 5cm2.(484) Another successful regimen consisted of 15-minute sessions, 5 times a week for 3 weeks.(480)\n\nIndications for Discontinuation: Resolution, failure to objectively improve or intolerance.\n\nRationale: There are no quality trials for treatment of patients with ulnar neuropathies. However, there are trials for treatment of CTS that suggest modest benefit(480, 483-486) (see Hand, Wrist, and Forearm Disorders chapter). Thus, by analogy, ultrasound is recommended for select patients who have failed treatment with a nocturnal brace/splint or obtained insufficient benefits."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-ulnar-neuropathies",
        "label-name": "Chronic Ulnar Neuropathies"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the patient been given nocturnal splints and/ or other treatments and had an inadequate response?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Ultrasound is recommended for the treatment of acute, subacute, or chronic ulnar neuropathies.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Ulnar neuropathies that are sufficiently symptomatic to warrant treatment. Patients should generally be given nocturnal splints and had an inadequate response.     \n\nFrequency/Dose/Duration: The regimen in the highest quality study of CTS patients consisted of daily 15-minute sessions, 5 a week for 2 weeks, then twice a week for 5 more weeks; 1MHz with intensity 1.0W/cm2, pulsed mode duty cycle of 1:4 and transducer area of 5cm2.(484) Another successful regimen consisted of 15-minute sessions, 5 times a week for 3 weeks.(480)\n\nIndications for Discontinuation: Resolution, failure to objectively improve or intolerance.\n\nRationale: There are no quality trials for treatment of patients with ulnar neuropathies. However, there are trials for treatment of CTS that suggest modest benefit(480, 483-486) (see Hand, Wrist, and Forearm Disorders chapter). Thus, by analogy, ultrasound is recommended for select patients who have failed treatment with a nocturnal brace/splint or obtained insufficient benefits."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Flateral-epicondylalgia%2Ftreatment-recommendations , https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Fpronator-syndrome%2Ftreatment-recommendations , https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Fradial-nerve-entrapment%2Ftreatment-recommendations , https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Fulnar-neuropathy-at-the-elbow%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
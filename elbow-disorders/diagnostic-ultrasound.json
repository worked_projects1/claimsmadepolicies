{
  "treatment": {
    "index-name": "diagnostic-ultrasound",
    "label-name": "Diagnostic Ultrasound",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "elbow-disorders",
    "label-name": "Elbow Disorders"
  },
  "version": {
    "mtus": "04/18/19",
    "odg": "",
    "policy-doc": "01/31/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "biceps-tendinosis",
        "label-name": "Biceps Tendinosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate to severe biceps tendinosis or rupture with the need for surgery being uncertain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Diagnostic ultrasound is recommended for the evaluation and diagnosis of biceps tendinosis or ruptures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with moderate to severe biceps tendinosis or ruptures, particularly those for whom the need for surgery is uncertain. Patients with complete ruptures generally do not require diagnostic ultrasound as it usually does not alter the need for surgery. Patients with mild tears generally do not require ultrasound as the test does not alter the treatment plan and the good prognosis. Ultrasound should generally not be performed in addition to MRI as it usually does not add additional information of benefit.\n\nRationale: After MRI, diagnostic ultrasound is likely the second most common imaging study to evaluate the degree of biceps tendinosis or rupture. Ultrasound may assist in evaluating the need for surgery particularly in those patients with moderately severe tears in whom the degree of rupture may help identify whether surgery is likely to be beneficial. Ultrasound is not invasive, has low adverse effects, and is moderate cost. Therefore, it is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "biceps-rupture",
        "label-name": "Biceps Rupture"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the patient have moderate to severe biceps tendinosis or rupture with the need for surgery being uncertain?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Diagnostic ultrasound is recommended for the evaluation and diagnosis of biceps tendinosis or ruptures.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications: Patients with moderate to severe biceps tendinosis or ruptures, particularly those for whom the need for surgery is uncertain. Patients with complete ruptures generally do not require diagnostic ultrasound as it usually does not alter the need for surgery. Patients with mild tears generally do not require ultrasound as the test does not alter the treatment plan and the good prognosis. Ultrasound should generally not be performed in addition to MRI as it usually does not add additional information of benefit.\n\nRationale: After MRI, diagnostic ultrasound is likely the second most common imaging study to evaluate the degree of biceps tendinosis or rupture. Ultrasound may assist in evaluating the need for surgery particularly in those patients with moderately severe tears in whom the degree of rupture may help identify whether surgery is likely to be beneficial. Ultrasound is not invasive, has low adverse effects, and is moderate cost. Therefore, it is recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluation-and-diagnosis-of-ulnar-neuropathies-at-the-elbow",
        "label-name": "Evaluation and Diagnosis of Ulnar Neuropathies at the Elbow"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound and MRI for the evaluation and diagnosis of ulnar neuropathies at the elbow.\n\nStrength of Evidence - No Recommendation, Insufficient Evidence (I)\n\nRationale for Recommendation: There are no quality studies available demonstrating superiority of ultrasound or MRI over other available tests to evaluate and diagnose. Therefore, there is no recommendation for or against the use of ultrasound and MRI."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "elbow-osteonecrosis",
        "label-name": "Elbow Osteonecrosis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound for the evaluation and diagnosis of other elbow disorders, including osteonecrosis, osteoarthrosis, dysplasia, and fractures.\n\n Strength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: Ultrasound has been found to be helpful evaluating tendinopathies, including tendon ruptures. There is no clear indication for use of ultrasound for evaluation of osteoarthrosis and other disorders. Ultrasound is not invasive, has no adverse effects and is moderately costly. It is recommended for disorders with soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "elbow-osteoarthrosis",
        "label-name": "Elbow Osteoarthrosis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound for the evaluation and diagnosis of other elbow disorders, including osteonecrosis, osteoarthrosis, dysplasia, and fractures.\n\n Strength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: Ultrasound has been found to be helpful evaluating tendinopathies, including tendon ruptures. There is no clear indication for use of ultrasound for evaluation of osteoarthrosis and other disorders. Ultrasound is not invasive, has no adverse effects and is moderately costly. It is recommended for disorders with soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "elbow-dysplasia",
        "label-name": "Elbow Dysplasia"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound for the evaluation and diagnosis of other elbow disorders, including osteonecrosis, osteoarthrosis, dysplasia, and fractures.\n\n Strength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: Ultrasound has been found to be helpful evaluating tendinopathies, including tendon ruptures. There is no clear indication for use of ultrasound for evaluation of osteoarthrosis and other disorders. Ultrasound is not invasive, has no adverse effects and is moderately costly. It is recommended for disorders with soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "elbow-fracture",
        "label-name": "Elbow Fracture"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound for the evaluation and diagnosis of other elbow disorders, including osteonecrosis, osteoarthrosis, dysplasia, and fractures.\n\n Strength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: Ultrasound has been found to be helpful evaluating tendinopathies, including tendon ruptures. There is no clear indication for use of ultrasound for evaluation of osteoarthrosis and other disorders. Ultrasound is not invasive, has no adverse effects and is moderately costly. It is recommended for disorders with soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "other-elbow-disorders",
        "label-name": "Other Elbow Disorders"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound for the evaluation and diagnosis of other elbow disorders, including osteonecrosis, osteoarthrosis, dysplasia, and fractures.\n\n Strength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale: Ultrasound has been found to be helpful evaluating tendinopathies, including tendon ruptures. There is no clear indication for use of ultrasound for evaluation of osteoarthrosis and other disorders. Ultrasound is not invasive, has no adverse effects and is moderately costly. It is recommended for disorders with soft tissue pathology."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Fbiceps-tendinosis%2Fdiagnostic-recommendations , https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Fulnar-neuropathy-at-the-elbow%2Fdiagnostic-recommendations , https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Felbow-disorders%2Fdiagnosis-and-treatment-recommendations%2Felbow-fracture%2Fdiagnostic-recommendations",
    "odg-link": ""
  }
}
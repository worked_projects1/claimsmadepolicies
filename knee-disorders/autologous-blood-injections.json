{
  "treatment": {
    "index-name": "autologous-blood-injections",
    "label-name": "Autologous Blood Injections",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "01/10/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-osteoarthorosis",
        "label-name": "Knee Osteoarthorosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have moderate to severe knee osteoarthorosis?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the injured worker had insufficient response to NSAID/acetaminophen, and exercise?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        },
        {
          "#": "3",
          "question": "Has the patient had PRP injections?",
          "option-list": [
            {
              "option": "yes",
              "action": "4"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        },
        {
          "#": "4",
          "question": "Has the injured worker had glucocorticosteroid injections? (Generally they should also be trialed first as there is moderate quality evidence suggesting comparable efficacy).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "no_recommendation"
            }
          ]
        }
      ],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of autologous blood injections for moderate to severe knee osteoarthrosis\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nIndications – Re. PRP injections, moderately to severe knee osteoarthrosis with insufficient responses to NSAID/acetaminophen, and exercise.  Generally, should also have attempted weight loss.  Glucocorticosteroid should generally also be trialed first as there is moderate quality evidence suggesting comparable efficacy (2421, 2422).\n\nBenefits – Improved pain and function, with somewhat conflicting evidence on duration of the benefit being 6- vs. 12+ months (2423,2424).\n\nHarms – Hypertension, proteinuria, diarrhea, constipation, rare infections (2425)\n\nFrequency/Dose/Duration – One injection should be evaluated to ascertain whether it is effective. Although a series of injections has been trialed, there is no clear superiority of a series of injections compared with a single injection (2423, 2424, 2426).\n\nRationale – There is one quality trial of injections of PRP vs. Hyaluronic acid vs. NSAID and used MRI imaging, and while the symptoms reportedly improved, there was not evidence of efficacy based on MRI results at one year (2427). One trial without placebo-control suggested MRI improvements in the PRP group compared with the HA group, with 48% vs. 8% improving at least one OA grade over 12 months (2418).  Several moderate-quality placebo-controlled trials suggest modest benefits of PRP compared with placebo (2423-2424, 2427, 2429-2432); e.g.,  one trial suggested comparable benefits with either one or two injections at 6 months but superiority compared with placebo yet benefits waned after that point for either 1- or 2-injections (1349); another trial suggested durable benefits at 12 months of a series of three (3) PRP injections compared to placebo (2424); and one trial suggested PRP improved pain and disability (2433). Two moderate-quality trials suggested comparable efficacy to glucocorticoid injection (2421, 2422), with one of those trials suggesting longer duration of the PRP (2422). \n\nThere are many moderate- to high-quality trials (1346-1348, 1353, 2434), mostly comparisons against viscosupplementation.  Thus, the body of evidence tends to suggest PRP injections tend to be superior to viscosupplementation injections, which appear superior to glucocorticosteroids (see below).  With limited and somewhat conflicting placebo-controlled trials, the evidence was considered too limited by the panel for evidence-based recommendations. \n\nPRP injections have also been trialed for preventing post-operative blood loss and have been suggested to improve function in knee arthroplasty patients (2435, 2436). The duration of the benefit was gone at 6 months in one trial (2435), but lasted 12 months in the other trial (2436). PRP has also been trialed with stem cells in a small-sized RCT (2437).\n\nPRP injections are invasive and have a low risk of adverse effects but are high cost. A majority of the Evidence-based Practice Knee concluded that there should be no recommendation for platelet rich plasma injections for moderate to severe knee osteoarthrosis based on the relative lack of, and conflicts among the quality placebo-controlled trials. In addition, the Evidence-base Practice Knee Panel concluded there is insufficient evidence to conclude either for or against a recommendation (57% agreed with no recommendation, 15% thought it should be not recommended and 29% felt it should be recommended) for moderate to severe knee osteoarthrosis based on the lack of quality trials regarding the overall efficacy of these injections. Indications are provided above as a potential appeals process."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acl-tear",
        "label-name": "ACL Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no quality trials specifically addressing patients with ACL and PCL tears. Work limitations are usually necessary, especially in the acute phase, although required job demands must be incorporated. Those performing high physical demand tasks or those who cannot avoid repeating physically demanding job tasks similar to those that resulted in the condition are especially recommended to have work limitations. In other cases, particularly where the worker has the ability to modulate work tasks, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for most patients. Nonsteroidal anti-inflammatory medications and ice/heat are recommended. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma-rich platelet injections, glucocorticosteroid injections, or hyaluronic acid injections for treatment of ACL tears."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-sprain",
        "label-name": "Knee Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation, mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections for knee sprains.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patellofemoral-joint-pain",
        "label-name": "Patellofemoral Joint Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, iontophoresis, low-level laser therapy, phonophoresis, autologous blood injections, or hyaluronic acid injections for treatment of patellofemoral joint pain.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale – Work limitations may be necessary depending on the severity of the condition and the required job demands. Those performing physically demanding tasks or those who have no ability to avoid repeating physically demanding job tasks that have resulted in the condition are recommended to have work limitations. In other cases, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for some patients, particularly those more severely affected. NSAIDs, ice, heat, Ace wraps, supports, and sleeves are recommended. Those with persisting pain thought to not be clearly surgical are recommended to have a course of rehabilitation therapy. There is no recommendation for or against therapeutic ultrasound, diathermy, iontophoresis, low-level laser therapy, phonophoresis, autologous blood injections, or hyaluronic acid injections for treatment of patellofemoral joint pain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patellar-tendinopathy",
        "label-name": "Patellar Tendinopathy"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Platelet-rich plasma and autologous blood injections are not recommended for the treatment of patellar tendinopathy.\n\nStrength of Evidence – Not Recommended, Evidence (C)\n\nLevel of Confidence – Low \n\nRationale – One small, placebo-controlled trial for patellar tendinopathy suggested a lack of efficacy (2463).  Another trial found no differences between 1 and 2 injections (2464) while another found 2 injections better than one (2465).  There is one moderate-quality study suggesting efficacy of PRP over dry-needling.(2372) There are two moderate-quality trials suggesting PRP is superior to extracorporeal shockwave therapy, but ESWT appears ineffective (see ESWT).(2373, 2374) PRP injections are invasive, have adverse effects, are costly, and the sole placebo-controlled trial suggests lack of efficacy; thus, they are not recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "meniscal-tear",
        "label-name": "Meniscal Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "There is no recommendation for or against the use of platelet-rich plasma or autologous blood injections.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale – One research group has reported a placebo-controlled trial, suggesting that PRP was associated with improved healing of chronic meniscal tears and limited the need for future surgical repairs significantly (8% vs 28%), while the failure rate (non-union of the meniscal tear) was 48% in PRP group vs 70% in controls (2460). As there is only one research group reporting favorable results, there is no recommendation until reproducible and durable efficacy has been demonstrated. "
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fosteoarthrosis%2Ftreatment-recommendations%2Finjections,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fknee-pain%2Ftreatment-recommendations%2Finjections,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fpatellar-tendinopathy%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Ftreatment-recommendations%2Finjection-therapies",
    "odg-link": ""
  }
}
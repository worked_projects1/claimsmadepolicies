{
  "treatment": {
    "index-name": "acupuncture",
    "label-name": "Acupuncture",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "12/16/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-knee-pain",
        "label-name": "Acute Knee Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of acupuncture for the treatment of acute or subacute knee pain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I) \n\nRationale – There are several high- and moderate-quality studies that evaluated acupuncture for the treatment of knee osteoarthrosis.(1190, 1192-1204) Trials of auricular acupuncture suggests efficacy in reducing analgesia requirements peri-operative,(1205) intra-operative,(1206) and post-operative.(1174) Some have concluded that the evidence suggests that there is no effect of acupuncture on pain.(1012) Some trials have combined acupuncture with electrical currents, others have applied electrical currents to acupuncture sites,(1201, 1207, 1208) and one involved periosteal stimulation.(1209) There are no quality studies to show clear benefit of electroacupuncture over needling. There continue to be some questions about efficacy of acupuncture,(1210, 1211) with concerns about biases, e.g., attention and expectation bias in these study designs as well as the adequacy of placebo acupuncture treatments.(1212, 1213) One trial demonstrated acupuncturist behaviors to set positive expectations had a significant impact on outcomes from acupuncture.(1214)\n\nStudies reporting results after the cessation of acupuncture have nearly all found lasting benefits,(1187, 1192, 1215) although there are no long-term follow-up reports. Although not all studies have been positive,(1216) acupuncture has been found to be superior to no acupuncture ,(1192, 1217) superior to more of the same medication,(1202) superior to usual care,(1218-1221) and also an additive benefit to an NSAID.(1198) Results of three trials involving shams have indicated the sham was approximately equivalent to acupuncture,(1189, 1190, 1222) but acupuncture(1196) and electroacupuncture(1207) were superior to sham in two other trials. High-quality studies with sizable populations and long follow-up periods are needed for all of these potential indications. Acupuncture when performed by experienced professionals is minimally invasive, has minimal adverse effects, and is moderately costly. Despite significant reservations regarding its true mechanism of action, a limited course of acupuncture may be recommended for treatment of knee osteoarthrosis as an adjunct to a conditioning and weight loss program. Acupuncture is recommended to assist in increasing functional activity levels more rapidly. Primary attention should remain on the conditioning program. Acupuncture is not recommended for those not involved in a conditioning program or who are non-compliant with graded increases in activity levels."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-knee-pain",
        "label-name": "Subacute Knee Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of acupuncture for the treatment of acute or subacute knee pain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I) \n\nRationale – There are several high- and moderate-quality studies that evaluated acupuncture for the treatment of knee osteoarthrosis.(1190, 1192-1204) Trials of auricular acupuncture suggests efficacy in reducing analgesia requirements peri-operative,(1205) intra-operative,(1206) and post-operative.(1174) Some have concluded that the evidence suggests that there is no effect of acupuncture on pain.(1012) Some trials have combined acupuncture with electrical currents, others have applied electrical currents to acupuncture sites,(1201, 1207, 1208) and one involved periosteal stimulation.(1209) There are no quality studies to show clear benefit of electroacupuncture over needling. There continue to be some questions about efficacy of acupuncture,(1210, 1211) with concerns about biases, e.g., attention and expectation bias in these study designs as well as the adequacy of placebo acupuncture treatments.(1212, 1213) One trial demonstrated acupuncturist behaviors to set positive expectations had a significant impact on outcomes from acupuncture.(1214)\n\nStudies reporting results after the cessation of acupuncture have nearly all found lasting benefits,(1187, 1192, 1215) although there are no long-term follow-up reports. Although not all studies have been positive,(1216) acupuncture has been found to be superior to no acupuncture ,(1192, 1217) superior to more of the same medication,(1202) superior to usual care,(1218-1221) and also an additive benefit to an NSAID.(1198) Results of three trials involving shams have indicated the sham was approximately equivalent to acupuncture,(1189, 1190, 1222) but acupuncture(1196) and electroacupuncture(1207) were superior to sham in two other trials. High-quality studies with sizable populations and long follow-up periods are needed for all of these potential indications. Acupuncture when performed by experienced professionals is minimally invasive, has minimal adverse effects, and is moderately costly. Despite significant reservations regarding its true mechanism of action, a limited course of acupuncture may be recommended for treatment of knee osteoarthrosis as an adjunct to a conditioning and weight loss program. Acupuncture is recommended to assist in increasing functional activity levels more rapidly. Primary attention should remain on the conditioning program. Acupuncture is not recommended for those not involved in a conditioning program or who are non-compliant with graded increases in activity levels."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "anterior-knee-pain",
        "label-name": "Anterior Knee Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of acupuncture for anterior knee pain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\n               \n\nRationale for Recommendation\n\nThere are two moderate-quality trials with somewhat conflicting results. One trial compared electroacupuncture with minimal superficial acupuncture and failed to find evidence of efficacy,(1208) while the other suggested slight benefits compared with no treatment controls.(2358) Thus, there is no recommendation for or against the use of acupuncture to treat anterior knee pain."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-osteoarthrosis",
        "label-name": "Chronic Osteoarthrosis"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have moderate to severe chronic osteoarthrosis of the knee?",
          "option-list": [
            {
              "option": "yes",
              "action": "2"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "2",
          "question": "Has the injured worker had prior treatments, which should include NSAIDs, weight loss, and exercise, including a graded walking program and strengthening exercises?",
          "option-list": [
            {
              "option": "yes",
              "action": "3"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        },
        {
          "#": "3",
          "question": "Will this be considered as an adjunct to a conditioning program that has resulted in insufficient clinical response?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Moderately Recommended. Acupuncture is moderately recommended for select use for treatment of chronic osteoarthrosis of the knee as an adjunct to more efficacious treatments.\n\nStrength of Evidence – Moderately Recommended, Evidence (B)\n\nIndications – Moderate to severe chronic osteoarthrosis of the knee. Prior treatments should include NSAIDs, weight loss, and exercise, including a graded walking program and strengthening exercises. Should be considered as an adjunct to a conditioning program that has resulted in insufficient clinical response.\n\nFrequency/Dose/Duration – A limited course of 6 appointments(1187) with clear objective and functional goals to be achieved. Additional appointments would require documented functional benefits, lack of plateau in measures and probability of obtaining further benefits. There is quality evidence suggesting traditional acupuncture needle placement may be unnecessary(1188) and that superficial needling is as successful as deep needling.(1189, 1190) There is evidence suggesting it is not necessary to perform bilateral needling,(1191) although that result has not been replicated.\n\nIndications for Discontinuation – Resolution, intolerance, and non-compliance, including non-compliance with aerobic and strengthening exercises.\n\nRationale – There are several high- and moderate-quality studies that evaluated acupuncture for the treatment of knee osteoarthrosis.(1190, 1192-1204) Trials of auricular acupuncture suggests efficacy in reducing analgesia requirements peri-operative,(1205) intra-operative,(1206) and post-operative.(1174) Some have concluded that the evidence suggests that there is no effect of acupuncture on pain.(1012) Some trials have combined acupuncture with electrical currents, others have applied electrical currents to acupuncture sites,(1201, 1207, 1208) and one involved periosteal stimulation.(1209) There are no quality studies to show clear benefit of electroacupuncture over needling. There continue to be some questions about efficacy of acupuncture,(1210, 1211) with concerns about biases, e.g., attention and expectation bias in these study designs as well as the adequacy of placebo acupuncture treatments.(1212, 1213) One trial demonstrated acupuncturist behaviors to set positive expectations had a significant impact on outcomes from acupuncture.(1214)\n\nStudies reporting results after the cessation of acupuncture have nearly all found lasting benefits,(1187, 1192, 1215) although there are no long-term follow-up reports. Although not all studies have been positive,(1216) acupuncture has been found to be superior to no acupuncture ,(1192, 1217) superior to more of the same medication,(1202) superior to usual care,(1218-1221) and also an additive benefit to an NSAID.(1198) Results of three trials involving shams have indicated the sham was approximately equivalent to acupuncture,(1189, 1190, 1222) but acupuncture(1196) and electroacupuncture(1207) were superior to sham in two other trials. High-quality studies with sizable populations and long follow-up periods are needed for all of these potential indications. Acupuncture when performed by experienced professionals is minimally invasive, has minimal adverse effects, and is moderately costly. Despite significant reservations regarding its true mechanism of action, a limited course of acupuncture may be recommended for treatment of knee osteoarthrosis as an adjunct to a conditioning and weight loss program. Acupuncture is recommended to assist in increasing functional activity levels more rapidly. Primary attention should remain on the conditioning program. Acupuncture is not recommended for those not involved in a conditioning program or who are non-compliant with graded increases in activity levels."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acl-tear",
        "label-name": "ACL Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no quality trials specifically addressing patients with ACL and PCL tears. Work limitations are usually necessary, especially in the acute phase, although required job demands must be incorporated. Those performing high physical demand tasks or those who cannot avoid repeating physically demanding job tasks similar to those that resulted in the condition are especially recommended to have work limitations. In other cases, particularly where the worker has the ability to modulate work tasks, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for most patients. Nonsteroidal anti-inflammatory medications and ice/heat are recommended. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma-rich platelet injections, glucocorticosteroid injections, or hyaluronic acid injections for treatment of ACL tears."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "meniscal-tear",
        "label-name": "Meniscal Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, phonophoresis, acupuncture, autologous blood injections, plasma rich platelet injections, and hyaluronic acid injections for meniscal tears.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – Work limitations may be necessary depending on the severity of the condition and the required job demands. Those performing high physical demand tasks or those who have no ability to avoid repeating physically demanding job tasks that may have resulted in the condition are recommended to have work limitations. In other cases, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for some patients, particularly those more severely affected. Nonsteroidal anti-inflammatory medications, ice, heat, Ace wraps, supports or sleeves are recommended. Those with persisting pain thought to not be clearly surgical are recommended to have a course of rehabilitation therapy. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, phonophoresis, acupuncture, manual therapy, autologous blood injections, plasma rich platelet injections, and hyaluronic acid injections. Hyaluronic acid injections have been used to treat knee osteoarthritis,(1424) and have been reported to have additive benefit for arthroscopy patients found to have arthrosis at the time of meniscal surgery."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-sprain",
        "label-name": "Knee Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation, mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections for knee sprains.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fknee-pain%2Ftreatment-recommendations%2Fallied-health-interventions,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fosteoarthrosis%2Ftreatment-recommendations%2Fallied-health-interventions,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "glucocorticosteroid-injections",
    "label-name": "Glucocorticosteroid Injections",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "01/10/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "iliotibial-(it)-band-syndrome",
        "label-name": "Iliotibial (IT) Band Syndrome"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Has the injured worker had insufficient results from all of the following: activity modification, relative rest, NSAIDs, and local applications of ice or heat?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Glucocorticosteroid injections are recommended for the treatment of iliotibial band syndrome in a subset of patients with insufficient results from other treatments.\n\nStrength of Evidence – Recommended, Evidence (C)\n\nIndications – Iliotibial band syndrome patients with insufficient results from activity modification, relative rest, NSAIDs, and local applications of ice or heat.\n\nFrequency/Dose/Duration – One quality trial used methylprednisolone acetate 40mg mixed with 1% lidocaine, injected between the IT band and lateral femoral condyle.(1998) If there is insufficient response, consideration may be given to a second injection, often with a modestly higher dose.\n\nIndications for Discontinuation – A second glucocorticosteroid injection is not recommended if the first has resulted in significant reduction or resolution of symptoms. If there has not been any response to a first injection, there is also less indication for a second. If the interventionalist believes the medication was not well placed and/or if the underlying condition is so severe that one steroid bolus could not be expected to adequately treat the condition, a second injection may be indicated. In patients who respond with several weeks of pharmacologically appropriate, temporary, partial relief of pain, but then have worsening pain and function and are not (yet) interested in surgical intervention, a repeat steroid injection is an option. There is unlikely to be benefit with greater than about 3 injections per year.\n\nRationale – There is one moderate-quality placebo-controlled trial that suggested benefits of injection with glucocorticoid compared with placebo anesthetic for treatment of iliotibial band syndrome.(1998) Although the trial was small, the results were statistically significant, thus meeting minimum criteria for an evidence-based recommendation. These injections are mildly invasive, have adverse effects, are moderately costly, and appear effective and are therefore recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-bursitis",
        "label-name": "Knee Bursitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation.  There is no recommendation for or against the use of glucocorticosteroid injections for the treatment of knee bursitis. This may be a reasonable option for patients who are failing to resolve prior to consideration of surgery.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no quality studies evaluating the use of glucocorticosteroid injections to treat knee bursitis. These injections sometimes appear to help speed resolution in cases not trending towards resorption. However, these injections potentially introduce bacteria, thus the one drawback is the potential to create a septic bursitis, which then often requires surgical drainage. If attempted, these injections appear to be reserved for patients thought to not be infected and/or who are not resolving with activity modifications and observation. If attempted, generally only 1 aspiration/injection is performed followed by careful observation. Some physicians aspirate and then inject, while others only inject the steroid. If the bursitis is not satisfactorily resolved, a second aspiration/injection is often attempted, although usually not sooner than 3 to 4 weeks later. Doses of steroid are approximately, e.g., methylprednisolone 20 to 40mg or equivalent. Aspirated fluid should be sent at least once for studies including crystals (light polarizing microscopy), Gram stain, culture, and sensitivity and complete cell count. Glucocorticosteroid injection is invasive, has relatively low adverse effects, although it can introduce an infection, and is moderately costly; thus, it is recommended in those cases not trending towards resolution."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "meniscal-tear",
        "label-name": "Meniscal Tear"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Glucocorticosteroid injections are not recommended for the treatment of meniscal tears.\n\nStrength of Evidence – Not Recommended, Insufficient Evidence (I)\n\nLevel of Confidence – Low\n\nRationale – There is no quality evidence of efficacy of these injections for meniscal tears and thus they are not recommended.  There are other indications for steroid injections.  The medical expert panel differed on this recommendation with 43% supporting not recommended, 29% no recommendation and 29% recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patellar-tendinopathy",
        "label-name": "Patellar Tendinopathy"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have chronic patellar tendinopathy that is unresponsive to other treatments including NSAID(s), activity modification and exercises?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. Glucocorticosteroid injections are recommended for select patents to treat patellar tendinopathy.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Chronic patellar tendinopathy that is unresponsive to other treatments including NSAID(s), activity modification and exercises.(1326, 2362)\n\nRationale for Recommendation\n \nThere is one moderate-quality placebo-controlled trial that evaluated the use of glucocorticosteroid injections for the treatment of patellar tendinopathy and found some evidence of efficacy, although somewhat less than with aprotinin.(2362) There is also one moderate-quality trial comparing glucocorticosteroid injections with two different exercise regimens that suggested that the steroid injections are inferior to heavy slow-resistance training exercises.(1326) These injections are mildly invasive, have adverse effects, are moderately costly, and have some evidence of efficacy, thus they are recommended for those select patients who fail a quality exercise program."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acl-tear",
        "label-name": "ACL Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no quality trials specifically addressing patients with ACL and PCL tears. Work limitations are usually necessary, especially in the acute phase, although required job demands must be incorporated. Those performing high physical demand tasks or those who cannot avoid repeating physically demanding job tasks similar to those that resulted in the condition are especially recommended to have work limitations. In other cases, particularly where the worker has the ability to modulate work tasks, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for most patients. Nonsteroidal anti-inflammatory medications and ice/heat are recommended. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma-rich platelet injections, glucocorticosteroid injections, or hyaluronic acid injections for treatment of ACL tears."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-sprain",
        "label-name": "Knee Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation, mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections for knee sprains.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Filiotibial-band-syndrome%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fbursitis%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fpatellar-tendinopathy%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Ftreatment-recommendations",
    "odg-link": ""
  }
}
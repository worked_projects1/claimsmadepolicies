{
  "treatment": {
    "index-name": "magnetic-resonance-imaging-(mri)",
    "label-name": "Magnetic Resonance Imaging (MRI)",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "01/10/23",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-osteonecrosis-(avn)",
        "label-name": "Diagnosing Osteonecrosis (AVN)"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain thought to be related to osteonecrosis (AVN)? (Particularly recommended if the diagnosis is unclear or if additional diagnostic evaluation and staging is needed).",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for diagnosing osteonecrosis.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain thought to be related to osteonecrosis (AVN), particularly if the diagnosis is unclear or if additional diagnostic evaluation and staging is needed.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluation-of-knee-sprain",
        "label-name": "Evaluation of Knee Sprain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. X-ray and/or MRI are recommended for the evaluation of knee sprains, particularly to rule out fracture.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluation-of-meniscal-tears",
        "label-name": "Evaluation of Meniscal Tears"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. X-ray and MRI are recommended in more severe cases of meniscal tears, including cases involving significant trauma, particularly to rule out fracture. MRI is also helpful for defining other injuries that may accompany tears such as cruciate and other ligament tears.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-joint-pathology",
        "label-name": "Knee Joint Pathology"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-meniscal-tears",
        "label-name": "Diagnosing Meniscal Tears"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-cruciate-ligament-tears",
        "label-name": "Diagnosing Cruciate Ligament Tears"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-hamstring-and-other-muscular-tears",
        "label-name": "Diagnosing Hamstring and other Muscular Tears"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "post-arthroplasty-chronic-pain",
        "label-name": "Post-arthroplasty Chronic Pain"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "diagnosing-periarticular-masses",
        "label-name": "Diagnosing Periarticular Masses"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci)?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. MRI is recommended for select patients with subacute or chronic knee symptoms in which mechanically disruptive internal derangement or similar soft tissue pathology is a concern. It is generally not indicated for patients with acute knee pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Subacute or chronic knee pain in which imaging of surrounding or intraarticular soft tissues is needed (including menisci); evaluation of moderately severe and severe cruciate ligament sprains and tears to evaluate the extent of the injury and help determine whether surgery is indicated.\n\nRationale – MRI has not been evaluated in quality studies for knee joint pathology, although studies have reported accuracy estimates ranging from 82 to 96% for cruciate ligament and meniscal tears.(84, 121, 348, 356, 357, 367, 430-434) False-negative MRI interpretations are particularly likely in posterior horn meniscal tears.(368) There is concern that MRI is overutilized, particularly in cases where clinical examination is sufficient.(84, 102, 116, 435) However, most physicians believe that MRI should be performed prior to arthroscopy for meniscal or ACL tears(436) or in patients with non-specific knee pain.(437)\n\nMRI may play a role in staging osteoarthrosis,(438) although there is no quality evidence that this practice affects prognosis or treatment. MRI can detect osteophytes(439) and is better than x-ray for identifying cartilage loss and subchondral cysts, but it is relatively poor at detecting early subchondral sclerosis.(439, 440) There are no quality studies evaluating the use of MRI for osteonecrosis of the knee joint. There is low-quality evidence that MRI may be less sensitive for detection of subchondral fractures than helical CT or plain x-rays in patients with osteonecrosis.(396) MRI is not invasive, has no adverse effects, although there may be issues related to claustrophobia or complications of concomitantly administered medications, but it is costly. MRI is not recommended for routine knee imaging, but it is recommended for selected knee joint pathology, particularly suspected soft tissue pathology."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluating-patellofemoral-joint-pain",
        "label-name": "Evaluating Patellofemoral Joint Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of diagnostic ultrasound or MRI to evaluate patellofemoral joint pain.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "severe-quadriceps-strain",
        "label-name": "Severe Quadriceps Strain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. In the more severe cases of quadriceps, gastrocnemius, and soleus strains, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nRationale for Recommendation\n\nThe examination findings for these types of strains are tenderness, usually at either the muscle origin or insertion (e.g., high vs. low hamstring strains), with swelling or large ecchymoses in more severe cases. Some cases involve complete ruptures and require surgical repair. Clinical tests are generally not necessary, although in the more severe cases, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear, as severe cases may require surgery."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "severe-gastrocnemius-strain",
        "label-name": "Severe Gastrocnemius Strain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. In the more severe cases of quadriceps, gastrocnemius, and soleus strains, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\n \n\nRationale for Recommendation\n\n\nThe examination findings for these types of strains are tenderness, usually at either the muscle origin or insertion (e.g., high vs. low hamstring strains), with swelling or large ecchymoses in more severe cases. Some cases involve complete ruptures and require surgical repair. Clinical tests are generally not necessary, although in the more severe cases, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear, as severe cases may require surgery."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "severe-soleus-strain",
        "label-name": "Severe Soleus Strain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. In the more severe cases of quadriceps, gastrocnemius, and soleus strains, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\n \n\nRationale for Recommendation\n\n\nThe examination findings for these types of strains are tenderness, usually at either the muscle origin or insertion (e.g., high vs. low hamstring strains), with swelling or large ecchymoses in more severe cases. Some cases involve complete ruptures and require surgical repair. Clinical tests are generally not necessary, although in the more severe cases, evaluation with x-ray and/or MRI are recommended for evaluation of the underlying bony structure as well as the degree of muscle tear, as severe cases may require surgery."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "evaluating-anterior-cruciate-ligament-(acl)-tear",
        "label-name": "Evaluating Anterior Cruciate Ligament (ACL) Tear"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. MRI is recommended for ACL tears, particularly if there are concerns for other soft tissue damage including meniscal tears and other sprains. However, some cases also may be managed clinically without MRI.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fosteonecrosis%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fknee-pain%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhamstring-quadriceps-calf-strains%2Fdiagnostic-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Fdiagnostic-recommendations",
    "odg-link": ""
  }
}
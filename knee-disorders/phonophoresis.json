{
  "treatment": {
    "index-name": "phonophoresis",
    "label-name": "Phonophoresis",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "12/20/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "iliotibial-(it)-band-syndrome",
        "label-name": "Iliotibial (IT) Band Syndrome"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of phonophoresis for the treatment of iliotibial band syndrome.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no placebo-controlled trials that evaluate phonophoresis for treatment of IT band syndrome. There are also no quality trials comparing phonophoresis with an intervention with known efficacy. There is one moderate-quality trial comparing phonophoresis with knee immobilization that found phonophoresis superior.(1997) However, the study was likely biased in favor of phonophoresis. Therefore, there is no recommendation for or against the use of phonophoresis."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-osteoarthrosis",
        "label-name": "Knee Osteoarthrosis"
      },
      "recommendation": "deny",
      "questions": [],
      "mtus-text": "Not Recommended. Phonophoresis is not recommended for knee osteoarthrosis.\n\nStrength of Evidence – Not Recommended, Evidence (C)\n\nRationale – There is one moderate-quality study evaluating phonophoresis with ibuprofen compared to ultrasound and found no difference between the two therapies. The authors reported that both groups were improved over the 2 weeks of therapy.(1167) Thus, as there is not evidence of efficacy, phonophoresis is not recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acl-tear",
        "label-name": "ACL Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There are no quality trials specifically addressing patients with ACL and PCL tears. Work limitations are usually necessary, especially in the acute phase, although required job demands must be incorporated. Those performing high physical demand tasks or those who cannot avoid repeating physically demanding job tasks similar to those that resulted in the condition are especially recommended to have work limitations. In other cases, particularly where the worker has the ability to modulate work tasks, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for most patients. Nonsteroidal anti-inflammatory medications and ice/heat are recommended. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation and mobilization or manual therapy, autologous blood injections, plasma-rich platelet injections, glucocorticosteroid injections, or hyaluronic acid injections for treatment of ACL tears."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "meniscal-tear",
        "label-name": "Meniscal Tear"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, phonophoresis, acupuncture, autologous blood injections, plasma rich platelet injections, and hyaluronic acid injections for meniscal tears.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – Work limitations may be necessary depending on the severity of the condition and the required job demands. Those performing high physical demand tasks or those who have no ability to avoid repeating physically demanding job tasks that may have resulted in the condition are recommended to have work limitations. In other cases, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for some patients, particularly those more severely affected. Nonsteroidal anti-inflammatory medications, ice, heat, Ace wraps, supports or sleeves are recommended. Those with persisting pain thought to not be clearly surgical are recommended to have a course of rehabilitation therapy. There is no recommendation for or against therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, phonophoresis, acupuncture, manual therapy, autologous blood injections, plasma rich platelet injections, and hyaluronic acid injections. Hyaluronic acid injections have been used to treat knee osteoarthritis,(1424) and have been reported to have additive benefit for arthroscopy patients found to have arthrosis at the time of meniscal surgery."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-sprain",
        "label-name": "Knee Sprain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, electrical stimulation, iontophoresis, low-level laser therapy, phonophoresis, acupuncture, manipulation, mobilization or manual therapy, autologous blood injections, plasma rich platelet injections, glucocorticosteroid injections, and hyaluronic acid injections for knee sprains.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patellofemoral-joint-pain",
        "label-name": "Patellofemoral Joint Pain"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of therapeutic ultrasound, diathermy, iontophoresis, low-level laser therapy, phonophoresis, autologous blood injections, or hyaluronic acid injections for treatment of patellofemoral joint pain.\n\nStrength of Evidence – No Recommendation, Insuffcient Evidence (I)\n\nRationale – Work limitations may be necessary depending on the severity of the condition and the required job demands. Those performing physically demanding tasks or those who have no ability to avoid repeating physically demanding job tasks that have resulted in the condition are recommended to have work limitations. In other cases, there is no recommendation for or against work limitations. Bed rest and knee immobilization are not recommended due to risks of venous thromboembolisms and other adverse effects of bed rest, although relative rest may be required for some patients, particularly those more severely affected. NSAIDs, ice, heat, Ace wraps, supports, and sleeves are recommended. Those with persisting pain thought to not be clearly surgical are recommended to have a course of rehabilitation therapy. There is no recommendation for or against therapeutic ultrasound, diathermy, iontophoresis, low-level laser therapy, phonophoresis, autologous blood injections, or hyaluronic acid injections for treatment of patellofemoral joint pain."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Filiotibial-band-syndrome%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fosteoarthrosis%2Ftreatment-recommendations%2Fallied-health-interventions,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Ftreatment-recommendations%2Finjection-therapies,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fknee-pain%2Ftreatment-recommendations%2Finjections",
    "odg-link": ""
  }
}
{
  "treatment": {
    "index-name": "nsaids",
    "label-name": "NSAIDs",
    "treatment-type": "other"
  },
  "body-system": {
    "index-name": "knee-disorders",
    "label-name": "Knee Disorders"
  },
  "version": {
    "mtus": "09/21/20",
    "odg": "",
    "policy-doc": "12/16/22",
    "created-by": "Cade",
    "comments": ""
  },
  "policies": [
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-knee-pain",
        "label-name": "Acute Knee Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Strongly Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative knee pain. There is no consistent quality evidence that one NSAID is superior to another, thus there is No Recommendation, Insufficient Evidence (I), nor is there consistent quality evidence for superiority of one dosage form(626) or enteric-coated or sustained release preparations.(627-630) Due to their inhibitory effects on platelet function, non-selective COX inhibitors should be used with caution, or avoided altogether, in the post-operative period if patients are also receiving pharmacoprophylaxis (e.g., warfarin, low molecular weight heparins) to prevent venous thromboembolic disease. Concomitant use of non-selective COX inhibitors and anti-coagulation regimens may increase the risk of hemorrhage. There is also concern that COX inhibitors, particularly COX-2 inhibitors, may inhibit bone healing. Therefore, these agents should be used with caution, or avoided altogether, in the acute post-operative period in situations where bone healing is required, such as in fracture repair or in knee replacements where cementless components are utilized.\n\nAcetaminophen (or the analog, paracetamol) may be a reasonable alternative for treatment of acute, subacute, chronic or post-operative knee pain,(631, 632) although quality evidence suggests that acetaminophen is less efficacious than NSAIDs.(633-639) At least two quality trials of acetaminophen compared to placebo have been negative, including one with a large sample size of 779 patients.(637, 640) Of note, a recent FDA advisory committee recommended reduction of the maximum dose of acetaminophen to 650mg, which is less than the 1gm dose used in most quality trials. Consequently, the degree of successful treatment of osteoarthrosis with lower doses of acetaminophen is somewhat unclear. There is evidence that NSAIDs are as effective for pain relief as tramadol(641, 642) and dextropropoxyphene, although slightly less efficacious than codeine.(643, 644)\n\nStrength of Evidence – Strongly Recommended, Evidence (A) – Chronic knee pain(231, 631, 637, 648-660)\n\nRecommended, Evidence (C) – Acute flares(648, 661, 662)\n\nRecommended, Insufficient Evidence (I) – Acute, subacute, post-operative knee pain(663)\n\nIndications – Acute, subacute, chronic, or post-operative knee pain. OTC agents may suffice and be tried first.\n \nFrequency/Dose/Duration – Per manufacturer’s recommendations; essentially all NSAIDs have proven efficacious for this indication. As-needed use may be reasonable for many patients. However, nearly all trials used scheduled doses.(645) There is evidence that nocturnal dosing is superior if patient primarily has morning or nocturnal pain,(646) although this may only apply to agents with shorter half-lives, including indomethacin.(647)\n\nIndications for Discontinuation – Resolution of knee pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "subacute-knee-pain",
        "label-name": "Subacute Knee Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Strongly Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative knee pain. There is no consistent quality evidence that one NSAID is superior to another, thus there is No Recommendation, Insufficient Evidence (I), nor is there consistent quality evidence for superiority of one dosage form(626) or enteric-coated or sustained release preparations.(627-630) Due to their inhibitory effects on platelet function, non-selective COX inhibitors should be used with caution, or avoided altogether, in the post-operative period if patients are also receiving pharmacoprophylaxis (e.g., warfarin, low molecular weight heparins) to prevent venous thromboembolic disease. Concomitant use of non-selective COX inhibitors and anti-coagulation regimens may increase the risk of hemorrhage. There is also concern that COX inhibitors, particularly COX-2 inhibitors, may inhibit bone healing. Therefore, these agents should be used with caution, or avoided altogether, in the acute post-operative period in situations where bone healing is required, such as in fracture repair or in knee replacements where cementless components are utilized.\n\nAcetaminophen (or the analog, paracetamol) may be a reasonable alternative for treatment of acute, subacute, chronic or post-operative knee pain,(631, 632) although quality evidence suggests that acetaminophen is less efficacious than NSAIDs.(633-639) At least two quality trials of acetaminophen compared to placebo have been negative, including one with a large sample size of 779 patients.(637, 640) Of note, a recent FDA advisory committee recommended reduction of the maximum dose of acetaminophen to 650mg, which is less than the 1gm dose used in most quality trials. Consequently, the degree of successful treatment of osteoarthrosis with lower doses of acetaminophen is somewhat unclear. There is evidence that NSAIDs are as effective for pain relief as tramadol(641, 642) and dextropropoxyphene, although slightly less efficacious than codeine.(643, 644)\n\nStrength of Evidence – Strongly Recommended, Evidence (A) – Chronic knee pain(231, 631, 637, 648-660)\n\nRecommended, Evidence (C) – Acute flares(648, 661, 662)\n\nRecommended, Insufficient Evidence (I) – Acute, subacute, post-operative knee pain(663)\n\nIndications – Acute, subacute, chronic, or post-operative knee pain. OTC agents may suffice and be tried first.\n \nFrequency/Dose/Duration – Per manufacturer’s recommendations; essentially all NSAIDs have proven efficacious for this indication. As-needed use may be reasonable for many patients. However, nearly all trials used scheduled doses.(645) There is evidence that nocturnal dosing is superior if patient primarily has morning or nocturnal pain,(646) although this may only apply to agents with shorter half-lives, including indomethacin.(647)\n\nIndications for Discontinuation – Resolution of knee pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "chronic-knee-pain",
        "label-name": "Chronic Knee Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Strongly Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative knee pain. There is no consistent quality evidence that one NSAID is superior to another, thus there is No Recommendation, Insufficient Evidence (I), nor is there consistent quality evidence for superiority of one dosage form(626) or enteric-coated or sustained release preparations.(627-630) Due to their inhibitory effects on platelet function, non-selective COX inhibitors should be used with caution, or avoided altogether, in the post-operative period if patients are also receiving pharmacoprophylaxis (e.g., warfarin, low molecular weight heparins) to prevent venous thromboembolic disease. Concomitant use of non-selective COX inhibitors and anti-coagulation regimens may increase the risk of hemorrhage. There is also concern that COX inhibitors, particularly COX-2 inhibitors, may inhibit bone healing. Therefore, these agents should be used with caution, or avoided altogether, in the acute post-operative period in situations where bone healing is required, such as in fracture repair or in knee replacements where cementless components are utilized.\n\nAcetaminophen (or the analog, paracetamol) may be a reasonable alternative for treatment of acute, subacute, chronic or post-operative knee pain,(631, 632) although quality evidence suggests that acetaminophen is less efficacious than NSAIDs.(633-639) At least two quality trials of acetaminophen compared to placebo have been negative, including one with a large sample size of 779 patients.(637, 640) Of note, a recent FDA advisory committee recommended reduction of the maximum dose of acetaminophen to 650mg, which is less than the 1gm dose used in most quality trials. Consequently, the degree of successful treatment of osteoarthrosis with lower doses of acetaminophen is somewhat unclear. There is evidence that NSAIDs are as effective for pain relief as tramadol(641, 642) and dextropropoxyphene, although slightly less efficacious than codeine.(643, 644)\n\nStrength of Evidence – Strongly Recommended, Evidence (A) – Chronic knee pain(231, 631, 637, 648-660)\n\nRecommended, Evidence (C) – Acute flares(648, 661, 662)\n\nRecommended, Insufficient Evidence (I) – Acute, subacute, post-operative knee pain(663)\n\nIndications – Acute, subacute, chronic, or post-operative knee pain. OTC agents may suffice and be tried first.\n \nFrequency/Dose/Duration – Per manufacturer’s recommendations; essentially all NSAIDs have proven efficacious for this indication. As-needed use may be reasonable for many patients. However, nearly all trials used scheduled doses.(645) There is evidence that nocturnal dosing is superior if patient primarily has morning or nocturnal pain,(646) although this may only apply to agents with shorter half-lives, including indomethacin.(647)\n\nIndications for Discontinuation – Resolution of knee pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "anterior-cruciate-ligament-(acl)-tear",
        "label-name": "Anterior Cruciate Ligament (ACL) Tear"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for ACL tears. (See NSAID section for dose, frequency, discontinuation information.)\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "iliotibial-(it)-band-syndrome",
        "label-name": "Iliotibial (IT) Band Syndrome"
      },
      "recommendation": "1",
      "questions": [
        {
          "#": "1",
          "question": "Does the injured worker have sufficient symptoms to require treatment?",
          "option-list": [
            {
              "option": "yes",
              "action": "approve"
            },
            {
              "option": "no",
              "action": "deny"
            }
          ]
        }
      ],
      "mtus-text": "Recommended. NSAIDs are recommended for the treatment of iliotibial band syndrome.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nIndications – Iliotibial band syndrome patients with sufficient symptoms to require treatment.\n\nFrequency/Dose/Duration – Per manufacturers’ recommendations.\n\nIndications for Discontinuation – Sufficient clinical results (NSAIDS no longer required), resolution of symptoms, intolerance, adverse effects. A trial with a different class of NSAID is reasonable for treatment failures.\n\nRationale – There is one moderate-quality placebo-controlled trial; however, it did not document improvements compared to placebo.(1980) That trial included patients with acute symptoms and baseline differences that may have impacted the results. It also involved very short follow-up of 1 week with continued treadmill exercise in athletes resulting in difficulty extrapolating to working populations. The use of acute patients may have resulted in underpowering due to favorable prognoses in all treatment groups. NSAIDs are thought to be helpful, are not invasive, have few adverse, effects especially in young patients, are of low cost, and are thus recommended."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-bursitis",
        "label-name": "Knee Bursitis"
      },
      "recommendation": "no_recommendation",
      "questions": [],
      "mtus-text": "No Recommendation. There is no recommendation for or against the use of NSAIDs for the treatment of knee bursitis.\n\nStrength of Evidence – No Recommendation, Insufficient Evidence (I)\n\nRationale – There is no quality evidence that NSAIDs alter the clinical course, thus there is no recommendation for or against their use for knee bursitis. The threshold for a trial of these medications should generally be low."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "knee-sprain",
        "label-name": "Knee Sprain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended.  Nonsteroidal anti-inflammatory medications are recommended for treatment of knee sprains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "meniscal-tear",
        "label-name": "Meniscal Tear"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for meniscal tears. (See NSAIDs section for dose, frequency, discontinuation information).\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nRationale – Nonsteroidal anti-inflammatory medications, ice, heat, Ace wraps, supports or sleeves are recommended. Those with persisting pain thought to not be clearly surgical are recommended to have a course of rehabilitation therapy."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "patellofemoral-joint-pain",
        "label-name": "Patellofemoral Joint Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for treatment of patellofemoral joint pain.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)"
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "quadriceps-strai",
        "label-name": "Quadriceps Strai"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for quadriceps, gastrocnemius, and soleus strains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nFrequency/Dose/Duration – See NSAID section for dose, frequency, discontinuation information."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "gastrocnemius-strain",
        "label-name": "Gastrocnemius Strain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for quadriceps, gastrocnemius, and soleus strains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nFrequency/Dose/Duration – See NSAID section for dose, frequency, discontinuation information."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "soleus-strain",
        "label-name": "Soleus Strain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Recommended. Nonsteroidal anti-inflammatory medications are recommended for quadriceps, gastrocnemius, and soleus strains.\n\nStrength of Evidence – Recommended, Insufficient Evidence (I)\n\nFrequency/Dose/Duration – See NSAID section for dose, frequency, discontinuation information."
    },
    {
      "source": "mtus",
      "diagnosis": {
        "index-name": "acute-flares-of-knee-pain",
        "label-name": "Acute Flares of Knee Pain"
      },
      "recommendation": "approve",
      "questions": [],
      "mtus-text": "Strongly Recommended. NSAIDs are recommended for treatment of acute, subacute, chronic, or post-operative knee pain. There is no consistent quality evidence that one NSAID is superior to another, thus there is No Recommendation, Insufficient Evidence (I), nor is there consistent quality evidence for superiority of one dosage form(626) or enteric-coated or sustained release preparations.(627-630) Due to their inhibitory effects on platelet function, non-selective COX inhibitors should be used with caution, or avoided altogether, in the post-operative period if patients are also receiving pharmacoprophylaxis (e.g., warfarin, low molecular weight heparins) to prevent venous thromboembolic disease. Concomitant use of non-selective COX inhibitors and anti-coagulation regimens may increase the risk of hemorrhage. There is also concern that COX inhibitors, particularly COX-2 inhibitors, may inhibit bone healing. Therefore, these agents should be used with caution, or avoided altogether, in the acute post-operative period in situations where bone healing is required, such as in fracture repair or in knee replacements where cementless components are utilized.\n\nAcetaminophen (or the analog, paracetamol) may be a reasonable alternative for treatment of acute, subacute, chronic or post-operative knee pain,(631, 632) although quality evidence suggests that acetaminophen is less efficacious than NSAIDs.(633-639) At least two quality trials of acetaminophen compared to placebo have been negative, including one with a large sample size of 779 patients.(637, 640) Of note, a recent FDA advisory committee recommended reduction of the maximum dose of acetaminophen to 650mg, which is less than the 1gm dose used in most quality trials. Consequently, the degree of successful treatment of osteoarthrosis with lower doses of acetaminophen is somewhat unclear. There is evidence that NSAIDs are as effective for pain relief as tramadol(641, 642) and dextropropoxyphene, although slightly less efficacious than codeine.(643, 644)\n\nStrength of Evidence – Strongly Recommended, Evidence (A) – Chronic knee pain(231, 631, 637, 648-660)\n\nRecommended, Evidence (C) – Acute flares(648, 661, 662)\n\nRecommended, Insufficient Evidence (I) – Acute, subacute, post-operative knee pain(663)\n\nIndications – Acute, subacute, chronic, or post-operative knee pain. OTC agents may suffice and be tried first.\n\nFrequency/Dose/Duration – Per manufacturer’s recommendations; essentially all NSAIDs have proven efficacious for this indication. As-needed use may be reasonable for many patients. However, nearly all trials used scheduled doses.(645) There is evidence that nocturnal dosing is superior if patient primarily has morning or nocturnal pain,(646) although this may only apply to agents with shorter half-lives, including indomethacin.(647)\n\nIndications for Discontinuation – Resolution of knee pain, lack of efficacy, or development of adverse effects that necessitate discontinuation."
    }
  ],
  "references": {
    "mtus-link": "https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fknee-pain%2Ftreatment-recommendations%2Fmedications%2Fnsaids,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fcruciate-ligament-tears%2Ftreatment-recommendations%2Fmedications,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Filiotibial-band-syndrome%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fbursitis%2Ftreatment-recommendations%2Fmedications,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmcl-and-lcl%2Ftreatment-recommendations,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fmeniscal-tears%2Ftreatment-recommendations%2Fmedications,https://app.mdguidelines.com/acoem-section/state-guidelines%2Fca-mtus%2Fknee-disorders%2Fdiagnostic-and-treatment-recommendations%2Fhamstring-quadriceps-calf-strains%2Ftreatment-recommendations",
    "odg-link": ""
  }
}